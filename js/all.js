$(function(){
	$('.quest_tab').eq(0).show().addClass('active');

	var total = $('.quest_tab').length;
	$('.pr').text('1/'+total).width(Math.round($('.progress').width()/total));


	$('.next').click(function(e){
		e.preventDefault();
		index_tab = $('.quest_tab.active').index();
		if($('.quest_tab').eq(index_tab).find('input:checked').val()){flag = true}else{flag=false}
		if(flag){
			$('.quest_tab.active').removeClass('active').hide();
				index_tab++;
				if(index_tab<=(total-1)){
					$('.quest_tab').eq(index_tab).show().addClass('active');
					$('.pr').text(++index_tab+'/'+total).css('margin-left',--index_tab*$('.pr').width());
				}else{
					$('.quest_tab').hide();
					$('.content-footer').hide();
					$('.form').show();
				}
		}
		
	});

	$('#submit').click(function(){
		str='';
		$('.quest_tab').each(function(){
			str +='<input type="hidden" name="answer['+$(this).find('form').attr('data-id')+']" value="'+$(this).find('input:checked').val()+'">';
		});
		$('#result-form').append(str);
		return true;
	});

});