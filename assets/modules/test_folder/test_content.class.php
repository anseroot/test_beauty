<?php

class test_content{
    var $modx = null;

    function test_content(&$modx) {
        $this->modx = $modx;
    }


    function creat_view(){
        if(isset($_POST['type'])){
            switch ($_POST['type']) {
                case 'add_new_quest':
                    $this->add_new_quest($_POST);
                    break;
                case 'save_quest':
                    $this->save_quest($_POST);
                    break;

                
                default:
                    # code...
                    break;
            }
            
        }

        if(isset($_POST['id_test']) && $_POST['id_test']!=''){
                $quest_view = $this->make_quest_arr($_POST['id_test']);
                $add_quest_html = $this->modx->getChunk('add_quest_html');
                $add_quest_html_ = str_replace(array("\r\n", "\r", "\n"), " ", $add_quest_html);
                
                
                $out = $this->modx->parseChunk('quest_view',array('quest_list'=>$quest_view,'id_test'=>$_POST['id_test'],'add_quest_html'=>$add_quest_html_),'[+','+]');
            echo $out;
        }else{
            $result = $this->modx->db->query('SELECT `id`,`pagetitle` FROM `modx_site_content` WHERE `parent`=4');
            $arr_result = $this->modx->db->makeArray($result);
            $test_link = '';
            foreach ($arr_result as $key => $value) {
                $test_link .='<a href="'.$value['id'].'" class="test_link">'.$value['pagetitle'].'</a>';
            }
            $out = $this->modx->parseChunk('tpl_main',array('test_link'=>$test_link),'[+','+]');
            echo $out;
        };
    }

    function make_quest_arr($test_id){
        $out = '';
        $result = $this->modx->db->query('SELECT * FROM `test_quest` WHERE `id_test`='.$test_id.' ORDER BY quest_sort ASC');
        $quest_arr = $this->modx->db->makeArray($result);
        
        if(count($quest_arr)>0){
            for($ii = 0,$max_i=count($quest_arr);$ii<$max_i;$ii++) {
                //answer_list
                $value = array(
                    'answer_list'=>'',
                    'id'=>$quest_arr[$ii]['id'],
                    'id_test'=>$quest_arr[$ii]['id_test'],
                    'quest_title'=>$quest_arr[$ii]['quest_title'],
                    'quest_sort'=>$quest_arr[$ii]['quest_sort'],
                    'quest_correct_answer'=>$quest_arr[$ii]['quest_correct_answer'],
                    'img'=>$quest_arr[$ii]['img']
                    );
                $result_answer = $this->modx->db->query('SELECT * FROM `test_answer` WHERE `id_quest` = '.$value['id']);
                $answer_arr = $this->modx->db->makeArray($result_answer);

                if(count($answer_arr)>0){

                    for($i=0,$max=count($answer_arr);$i<$max;$i++){
                        if($answer_arr[$i]['status']=='1'){
                            $value['answer_list'] .= $this->modx->parseChunk('add_answer_line_act',$answer_arr[$i],'[+','+]'); 
                        }else{
                           $value['answer_list'] .= $this->modx->parseChunk('add_answer_line',$answer_arr[$i],'[+','+]'); 
                        }
                    }
                }else{
                    $value['answer_list'] = '<i>Нет вариантов ответа на вопрос</i>';
                }

                $out .= $this->modx->parseChunk('test_quest_line',$value,'[+','+]');
                
            }
        }else{
            $out = '<i>У данного квеста нет вопросов</i>';
        }


        return $out;
    }

    function add_new_quest($data){
            unset($data['quest_content_flag']);
            unset($data['type']);
            $result = $this->modx->db->insert($data,'test_quest');
    }

    function save_quest($data){
        $quest_field = array(
            'quest_title'=>$data['quest_title'],
            'quest_sort'=>$data['quest_sort'],
            'quest_correct_answer'=>$data['quest_correct_answer'],
            'img'=>$data['img']
            );
        $result = $this->modx->db->update($quest_field,'test_quest','id = '.$data['id']);
        
        if(isset($_POST['answer'])){
            
           foreach ($_POST['answer'] as $key => $value) {
               $pos = strpos($key, 'new_');
               $field = array(
                    'status'=>$value['flag'],
                    'answer_title'=>$value['title']
                );
               if( $pos === false){
                    if($field['status']!='del'){
                        $this->modx->db->update($field,'test_answer','id = '.$key);
                    }else{
                        $this->modx->db->delete('test_answer','id = '.$key);
                    }
               }else{
                    $field['id_quest'] = intval($data['id']);
                    $this->modx->db->insert($field,'test_answer');
               }
           }
        }
    }

    /* front view */

    function make_quest($id){
        $result = $this->modx->db->query('SELECT `id`,`quest_title`,`img` FROM `test_quest` WHERE `id_test`='.$id.' ORDER BY `quest_sort` ASC');
        $arResult = $this->modx->db->makeArray($result);
        $out = '';
        if(count($arResult)>0){
            foreach ($arResult as $key => $value) {
                $result_answer = $this->modx->db->query('SELECT `id`,`answer_title` FROM `test_answer` WHERE `id_quest`='.$value['id']);

                $arResult_answer = $this->modx->db->makeArray($result_answer);
                $value['answer_list'] = '';

                if(count($arResult_answer)>0){
                    foreach ($arResult_answer as $key_a => $value_a) {
                        $value_a['id_q'] = $value['id'];
                        $value['answer_list'] .= $this->modx->parseChunk('answer_list',$value_a,'[+','+]');
                    }
                }else{
                    exit('Сайт временно не доступен');
                }
                
                $out .= $this->modx->parseChunk('quest_tab',$value,'[+','+]');
            }
        }else{
            exit('Сайт временно не доступен');
        }
        return $out;
    }

    function chek_form(){
        $field = array(
            'name'=>$this->check_input($_POST['name']),
            'mail'=>$this->check_input($_POST['email']),
            'test_id'=>intval($this->check_input($_POST['test_id'])),
            'hash_user'=>md5(time()),
            'page'=>'http://'.$_SERVER['SERVER_NAME'].''.$this->modx->makeURL(intval($this->check_input($_POST['pid'])))
            );
        $this->modx->db->insert($field,'test_user');
        $user_id = $this->modx->db->getInsertId();
        foreach ($_POST['answer'] as $key => $value) {
            $answer_filed = array(
                'id_answer'=>intval($this->check_input($key)),
                'answer_value'=>intval($this->check_input($value)),
                'user_id'=>$user_id
            );
            $this->modx->db->insert($answer_filed,'test_user_answer');
        }
        //echo('тут должна быть страница "тест пройден на вашу почту пришло письмо"');
        //echo($field['page'].'?uid='.$field['hash_user']);

        //$m_name = $this->check_input($_POST['name']);
        //$m_mail = $this->check_input($_POST['email']);
        //$m_url = $field['page'].'?uid='.$field['hash_user'];
        $m_pid = $this->check_input($_POST['pid']);
        $m_hash_user = $field['hash_user'];
        
        $result_page = $this->modx->config['site_url']."result_form?hash_user=".$m_hash_user."&pid=".$m_pid;
        $this->modx->sendRedirect($result_page);
        
        //$this->result_form($m_mail, $m_name, $m_url);
        //header ("Location: $result_page");
        
        //$this->storeAddress($m_mail,$m_name,$m_url);
        // $this->storeAddress('anseroot@gmail.com','Formname','specialview.ru');

    }
    
    function result_form(){
        
        $hash_user = $this->check_input($_REQUEST['hash_user']);
        $pid = $this->check_input($_REQUEST['pid']);
        $link = 'http://'.$_SERVER['SERVER_NAME'].''.$this->modx->makeURL(intval($this->check_input($pid))).'?uid='.$hash_user;
        
        if($hash_user == "" && $pid == "") {
            $out = "Идентификаторы не переданы!"; 
        }
        else {
            
            $result_user = $this->modx->db->query('SELECT `id`,`test_id`,`mail`,`name`,`hash_user`,`page`,`viewed` FROM `test_user` WHERE `hash_user` = \''.$hash_user.'\'');
            $user_data = $this->modx->db->makeArray($result_user);
            
            if(count($user_data)>0) {

                $m_id = $user_data[0]['id'];
                $m_mail = $user_data[0]['mail'];
                $m_name = $user_data[0]['name'];
                $m_url = $user_data[0]['page'].'?uid='.$user_data[0]['hash_user'];
                $m_viewed = $user_data[0]['viewed'];
                
                if($m_viewed == 0) {
                    $v_field = array(
                        "viewed" => 1
                    );
                    $this->modx->db->update($v_field,'test_user','id = '.$m_id);
                    $this->storeAddress($m_mail, $m_name, $m_url);
                }
                
            /*
                $email = $user_data[0]['mail'];
                $name = $user_data[0]['name'];
                
                $site_name = $this->modx->config['site_name'];
                $emailsender = $this->modx->config['emailsender'];
                $emailsubject = 'Ответы на тест beauty-matrix';
                $message = 'Добрый день! <br/><br/> Чтобы узнать свои ответы на тест beauty-matrix - перейдите по ссылке ниже. <br/><br/> <a href="'.$link.'" target="_blank">Посмотреть результаты!</a>';
                
                $param = array();
                $param['from']    = "{$site_name}<{$emailsender}>";
                $param['subject'] = $emailsubject;
                $param['body']    = $message;
                $param['to']      = $email;
                $rs = $this->modx->sendmail($param);
                //var_dump($user_data);
                */
                
                $out = "Результат отправлен вам на email!";
            }
            else {
                $out = "Пользователь не найден!";
            }
            
        }
        
        return $out;

    }

    function check_input($data){
        $out = $this->modx->db->escape($data);
        return $out;
    }


    function storeAddress($email,$name,$link){
    
        // Validation
        //if(!$_GET['email']){ return "No email address provided"; } 

        require_once($_SERVER['DOCUMENT_ROOT'].'/mail/inc/MCAPI.class.php');
        // grab an API Key from http://admin.mailchimp.com/account/api/
        $api = new MCAPI('0d758e22de0ea38bb57d57deda5b5b5c-us10');
        
        // grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
        // Click the "settings" link for the list - the Unique Id is at the bottom of that page. 
        $list_id = "1008f735ea";

        $merge_vars = array('t_name'=>$name,'t_link'=>$link);
        
        $api->listSubscribe($list_id, $email, $merge_vars);
        
        /*
        if($api->listSubscribe($list_id, $email, $merge_vars) === true) {
            // It worked!
            echo 'Success! Check your email to confirm sign up.';
        }else{
            // An error ocurred, return error message   
            echo 'Error: ' . $api->errorMessage;
        }
        */
    
    }

    function load_user_answer($get_data){
       $uid = $this->check_input($get_data['uid']);
       $result_user = $this->modx->db->query('SELECT `id`,`test_id`,`name`,`mail`,`hash_user`,`page`,`viewed` FROM `test_user` WHERE `hash_user` = \''.$uid.'\'');
       $user_data = $this->modx->db->makeArray($result_user);
       $result_answer_user = $this->modx->db->query('SELECT * FROM `test_user_answer` WHERE `user_id` = '.intval($user_data[0]['id']));
       //$arr_answer_user = $this->modx->db->makeArray($result_answer_user);
       while($row = mysql_fetch_array($result_answer_user)){
            $arr_answer_user[$row['id_answer']] = $row['answer_value'];
       }

        if(count($user_data)>0){
            $result_quest = $this->modx->db->query('SELECT * FROM `test_quest` WHERE `id_test`='.intval($user_data[0]['test_id']));
            $arr_quest = $this->modx->db->makeArray($result_quest);
            $out = '';
            $count_step = 0;
            foreach ($arr_quest as $key => $value) {
                $value['iter'] = ++$count_step;
                $value['answer_list'] ='';
                $result_answer = $this->modx->db->query('SELECT * FROM `test_answer` WHERE `id_quest`='.intval($value['id']));
                $arr_answer = $this->modx->db->makeArray($result_answer);
                foreach ($arr_answer as $key_a => $value_a) {

                    $value_a['cheked'] = ($arr_answer_user[$value['id']]==$value_a['id'])?'checked="checked"':'';
                    if($value_a['status']=='1'){
                         $value_a['true_answer'] = '<span class="correct">Верно</span>';
                         $value_a['class'] = 'green';
                    }

                    if($value_a['status']=='0' && $arr_answer_user[$value['id']]==$value_a['id']){
                        $value_a['false_answer'] = '<span class="incorrect">Неверно</span>';
                        $value_a['class'] = 'red';
                    }
                   
                    $value['answer_list'] .= $this->modx->parseChunk('answer_list_user',$value_a,'[+','+]');
                }
                $out .= $this->modx->parseChunk('quest_full',$value,'[+','+]');
            }
            
        }else{
            $result_for_redirect = $this->modx->db->query('SELECT `parent` FROM `modx_site_content` WHERE `id`='.$this->modx->documentIdentifier);
            $arr_document= $this->modx->db->makeArray($result_for_redirect);
            $url_redirect = $this->modx->makeUrl(intval($arr_document[0]['parent']));
            $this->modx->sendRedirect($url_redirect);
        }

        return $out;
    }

}

?>
