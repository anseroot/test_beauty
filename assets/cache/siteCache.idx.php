<?php
$c=&$this->config;
$c['manager_theme'] = "MODxRE";
$c['settings_version'] = "1.0.15";
$c['show_meta'] = "0";
$c['server_offset_time'] = "0";
$c['server_protocol'] = "http";
$c['manager_language'] = "russian-UTF8";
$c['modx_charset'] = "UTF-8";
$c['site_name'] = "My MODX Site";
$c['site_start'] = "1";
$c['error_page'] = "1";
$c['unauthorized_page'] = "1";
$c['site_status'] = "1";
$c['site_unavailable_message'] = "The site is currently unavailable";
$c['track_visitors'] = "0";
$c['top_howmany'] = "10";
$c['auto_template_logic'] = "parent";
$c['default_template'] = "3";
$c['old_template'] = "3";
$c['publish_default'] = "0";
$c['cache_default'] = "1";
$c['search_default'] = "1";
$c['friendly_urls'] = "1";
$c['friendly_url_prefix'] = "";
$c['friendly_url_suffix'] = "";
$c['friendly_alias_urls'] = "1";
$c['use_alias_path'] = "0";
$c['use_udperms'] = "1";
$c['udperms_allowroot'] = "0";
$c['failed_login_attempts'] = "3";
$c['blocked_minutes'] = "60";
$c['use_captcha'] = "0";
$c['captcha_words'] = "MODX,Access,Better,BitCode,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Tattoo,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";
$c['emailsender'] = "anseroot@gmail.com";
$c['email_method'] = "mail";
$c['smtp_auth'] = "0";
$c['smtp_host'] = "";
$c['smtp_port'] = "25";
$c['smtp_username'] = "";
$c['emailsubject'] = "Your login details";
$c['number_of_logs'] = "100";
$c['number_of_messages'] = "30";
$c['number_of_results'] = "20";
$c['use_editor'] = "1";
$c['use_browser'] = "1";
$c['rb_base_dir'] = "/var/www/test_beauty/www/assets/";
$c['rb_base_url'] = "assets/";
$c['which_editor'] = "TinyMCE";
$c['fe_editor_lang'] = "russian-UTF8";
$c['fck_editor_toolbar'] = "standard";
$c['fck_editor_autolang'] = "0";
$c['editor_css_path'] = "";
$c['editor_css_selectors'] = "";
$c['strip_image_paths'] = "1";
$c['upload_images'] = "bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff";
$c['upload_media'] = "au,avi,mp3,mp4,mpeg,mpg,wav,wmv";
$c['upload_flash'] = "fla,flv,swf";
$c['upload_files'] = "php,otf,eot,ttf,woff,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip";
$c['upload_maxsize'] = "1048576";
$c['new_file_permissions'] = "0644";
$c['new_folder_permissions'] = "0755";
$c['filemanager_path'] = "/var/www/test_beauty/www/";
$c['theme_refresher'] = "";
$c['manager_layout'] = "4";
$c['custom_contenttype'] = "application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json";
$c['auto_menuindex'] = "1";
$c['session.cookie.lifetime'] = "604800";
$c['mail_check_timeperiod'] = "60";
$c['manager_direction'] = "ltr";
$c['tinymce_editor_theme'] = "editor";
$c['tinymce_custom_plugins'] = "style,advimage,advlink,searchreplace,print,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras,visualchars,media";
$c['tinymce_custom_buttons1'] = "undo,redo,selectall,separator,pastetext,pasteword,separator,search,replace,separator,nonbreaking,hr,charmap,separator,image,link,unlink,anchor,media,separator,cleanup,removeformat,separator,fullscreen,print,code,help";
$c['tinymce_custom_buttons2'] = "bold,italic,underline,strikethrough,sub,sup,separator,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,separator,styleprops";
$c['tree_show_protected'] = "0";
$c['rss_url_news'] = "http://feeds.feedburner.com/modx-announce";
$c['rss_url_security'] = "http://feeds.feedburner.com/modxsecurity";
$c['validate_referer'] = "1";
$c['datepicker_offset'] = "-10";
$c['xhtml_urls'] = "1";
$c['allow_duplicate_alias'] = "0";
$c['automatic_alias'] = "1";
$c['datetime_format'] = "dd-mm-YYYY";
$c['warning_visibility'] = "1";
$c['remember_last_tab'] = "0";
$c['enable_bindings'] = "1";
$c['seostrict'] = "1";
$c['cache_type'] = "1";
$c['maxImageWidth'] = "1600";
$c['maxImageHeight'] = "1200";
$c['thumbWidth'] = "150";
$c['thumbHeight'] = "150";
$c['thumbsDir'] = ".thumbs";
$c['jpegQuality'] = "90";
$c['denyZipDownload'] = "0";
$c['denyExtensionRename'] = "0";
$c['showHiddenFiles'] = "0";
$c['docid_incrmnt_method'] = "0";
$c['make_folders'] = "0";
$c['site_id'] = "568fe5a337b4a";
$c['site_unavailable_page'] = "";
$c['reload_site_unavailable'] = "";
$c['siteunavailable_message_default'] = "В настоящее время сайт недоступен.";
$c['check_files_onlogin'] = "index.php\r\n.htaccess\r\nmanager/index.php\r\nmanager/includes/config.inc.php";
$c['error_reporting'] = "1";
$c['send_errormail'] = "0";
$c['pwd_hash_algo'] = "UNCRYPT";
$c['reload_captcha_words'] = "";
$c['captcha_words_default'] = "MODX,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";
$c['smtp_secure'] = "none";
$c['reload_emailsubject'] = "";
$c['emailsubject_default'] = "Данные для авторизации";
$c['reload_signupemail_message'] = "";
$c['signupemail_message'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['system_email_signup_default'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['reload_websignupemail_message'] = "";
$c['websignupemail_message'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['system_email_websignup_default'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['reload_system_email_webreminder_message'] = "";
$c['webpwdreminder_message'] = "Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация";
$c['system_email_webreminder_default'] = "Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация";
$c['tree_page_click'] = "27";
$c['resource_tree_node_name'] = "pagetitle";
$c['mce_editor_skin'] = "default";
$c['mce_template_docs'] = "";
$c['mce_template_chunks'] = "";
$c['mce_entermode'] = "p";
$c['mce_element_format'] = "xhtml";
$c['mce_schema'] = "html5";
$c['tinymce_custom_buttons3'] = "";
$c['tinymce_custom_buttons4'] = "";
$c['tinymce_css_selectors'] = "left=justifyleft;right=justifyright";
$c['rb_webuser'] = "0";
$c['clean_uploaded_filename'] = "0";
$c['sys_files_checksum'] = "a:4:{s:34:\"/var/www/test_beauty/www/index.php\";s:32:\"c6f73908b7b0a58acfe95b0f844d134e\";s:34:\"/var/www/test_beauty/www/.htaccess\";s:32:\"61a3be34923238e6cfe23b963fb82015\";s:42:\"/var/www/test_beauty/www/manager/index.php\";s:32:\"30df65e2d71987b65a4258e318c21aaf\";s:56:\"/var/www/test_beauty/www/manager/includes/config.inc.php\";s:32:\"090831e50bbfe2322dacba24e34e539a\";}";
$this->aliasListing = array();
$a = &$this->aliasListing;
$d = &$this->documentListing;
$m = &$this->documentMap;
$d['minimal-base'] = 1;
$a[1] = array('id' => 1, 'alias' => 'minimal-base', 'path' => '', 'parent' => 0, 'isfolder' => 0);
$m[] = array('0' => '1');
$d['test_folder'] = 4;
$a[4] = array('id' => 4, 'alias' => 'test_folder', 'path' => '', 'parent' => 0, 'isfolder' => 1);
$m[] = array('0' => '4');
$d['stranica-otvetov'] = 3;
$a[3] = array('id' => 3, 'alias' => 'stranica-otvetov', 'path' => '', 'parent' => 2, 'isfolder' => 0);
$m[] = array('2' => '3');
$d['chek_form'] = 5;
$a[5] = array('id' => 5, 'alias' => 'chek_form', 'path' => '', 'parent' => 2, 'isfolder' => 0);
$m[] = array('2' => '5');
$d['result_form'] = 6;
$a[6] = array('id' => 6, 'alias' => 'result_form', 'path' => '', 'parent' => 2, 'isfolder' => 0);
$m[] = array('2' => '6');
$d['test_1'] = 2;
$a[2] = array('id' => 2, 'alias' => 'test_1', 'path' => '', 'parent' => 4, 'isfolder' => 1);
$m[] = array('4' => '2');
$c = &$this->contentTypes;
$c = &$this->chunkCache;
$c['mm_rules'] = '// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php
// example of how PHP is allowed - check that a TV named documentTags exists before creating rule

if ($modx->db->getValue($modx->db->select(\'count(id)\', $modx->getFullTableName(\'site_tmplvars\'), "name=\'documentTags\'"))) {
	mm_widget_tags(\'documentTags\', \' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV
}
mm_widget_showimagetvs(); // Always give a preview of Image TVs
';
$c['WebLoginSideBar'] = '<!-- #declare:separator <hr> -->
<!-- login form section-->
<form method="post" name="loginfrm" action="[+action+]">
    <input type="hidden" value="[+rememberme+]" name="rememberme" />
    <fieldset>
        <h3>Your Login Details</h3>
        <label for="username">User: <input type="text" name="username" id="username" tabindex="1" onkeypress="return webLoginEnter(document.loginfrm.password);" value="[+username+]" /></label>
    	<label for="password">Password: <input type="password" name="password" id="password" tabindex="2" onkeypress="return webLoginEnter(document.loginfrm.cmdweblogin);" value="" /></label>
    	<input type="checkbox" id="checkbox_1" name="checkbox_1" tabindex="3" size="1" value="" [+checkbox+] onclick="webLoginCheckRemember()" /><label for="checkbox_1" class="checkbox">Remember me</label>
    	<input type="submit" value="[+logintext+]" name="cmdweblogin" class="button" />
	<a href="#" onclick="webLoginShowForm(2);return false;" id="forgotpsswd">Forget Your Password?</a>
	</fieldset>
</form>
<hr>
<!-- log out hyperlink section -->
<h4>You\'re already logged in</h4>
Do you wish to <a href="[+action+]" class="button">[+logouttext+]</a>?
<hr>
<!-- Password reminder form section -->
<form name="loginreminder" method="post" action="[+action+]">
    <fieldset>
        <h3>It happens to everyone...</h3>
        <input type="hidden" name="txtpwdrem" value="0" />
        <label for="txtwebemail">Enter the email address of your account to reset your password: <input type="text" name="txtwebemail" id="txtwebemail" size="24" /></label>
        <label>To return to the login form, press the cancel button.</label>
    	<input type="submit" value="Submit" name="cmdweblogin" class="button" /> <input type="reset" value="Cancel" name="cmdcancel" onclick="webLoginShowForm(1);" class="button" style="clear:none;display:inline" />
    </fieldset>
</form>

';
$c['tpl_main'] = '<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../assets/modules/test_folder/admin_css.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>
		var j = jQuery.noConflict();
		j(function(){
			j(\'.test_link\').click(function(e){
				e.preventDefault();
				document.test_id.id_test.value=j(this).attr(\'href\');
				document.test_id.submit();
			});
		});
	</script>
</head>
<body>
	<p>Выберите тест</p>
	<p>[+test_link+]</p>
	<p><i>Для создания нового теста нужно создать ресурс в test_folder и страницу ответов в новом тесте</i></p>
	<form name="test_id" method="post">
		<input type="hidden" name="id_test" value="[+id_test+]">
	</form>
</body>
</html>';
$c['quest_view'] = '<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../assets/modules/test_folder/admin_css.css">
	<link rel="stylesheet" href="../assets/plugins/tinymce/tiny_mce/themes/advanced/skins/default/ui.css">
	<link rel="stylesheet" href="../assets/plugins/tinymce/tiny_mce/plugins/inlinepopups/skins/clearlooks2/window.css">
	
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	
	<script language="javascript" type="text/javascript" src="../assets/plugins/tinymce/tiny_mce/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript" src="../assets/plugins/tinymce/js/xconfig.js"></script>
	<script>
		var j = jQuery.noConflict(),
			add_html = \'[+add_quest_html+]\';
		
		j(function(){
			j(\'.test_link\').click(function(e){
				e.preventDefault();
				document.test_id.id_test.value=j(this).attr(\'href\');
				document.test_id.submit();
			});
		});
	</script>
	<script src="../assets/modules/test_folder/admin_js.js"></script>
</head>
<body>
	<button id="back">Назад</button>
	<button id="add_quest">Добавить вопрос</button>
	<p>Список вопросов</p>
	<div>[+quest_list+]</div>
	
	<form name="test_id" method="post">
		<input type="hidden" name="id_test" class="id_test" value="[+id_test+]">
	</form>
</body>
</html>';
$c['add_quest_html'] = '<form name="add_quest_" method="POST" id="add_quest_">
	<input type="hidden" name="type" value="add_new_quest">
	<input name="quest_title" type="text" required placeholder="Текст вопроса">
	<input name="quest_sort" type="text" required placeholder="Индекс сортировки" >
	
	<textarea name="quest_correct_answer" class="new_textarea"></textarea>
	<input name="quest_content_flag" type="text" required style="height:0;opacity:0;">
	
	<button id="save_add_quest">Сохранить</button>
</form>';
$c['test_quest_line'] = '<div class="quest_line">
	<form name="quest_form_[+id+]" method="POST">
		<input type="hidden" name="id" value="[+id+]">
		<input type="hidden" name="id_test" value="[+id_test+]">
		<div class="left_zone">
			<p>Вопрос:  <input type="text" name="quest_title"  value="[+quest_title+]" required></p>
			<p>Индекс сортировки: <input type="text" name="quest_sort" value="[+quest_sort+]" required></p>
			<div class="answer_list">
				<ul>[+answer_list+]</ul>
				<button class="add_answer">добавить ответ</button>
			</div>
		</div><div class="content_true_answer">
			<p>Контент ответа правильного вопроса</p>
			<textarea class="content_answer" id="content_answe_[+id+]">[+quest_correct_answer+]</textarea>
			<input type="text" class="hid_input" name="quest_correct_answer" required value=\'[+quest_correct_answer+]\'>
			<input type="text" name="img" value="[+img+]" required id="img_[+id+]">
			<button class="add_img" data-id="[+id+]">Добавить картинку</button>
		</div>
		
		
		
		<input type="hidden" name="type" value="">
		<button class="save_quest">Сохранить</button>
		<button class="del_quest">Удалить</button>
	</form>
</div>';
$c['add_answer_line'] = '<li class="answer">
	<input type="text" name="answer[ [+id+] ][title]" value="[+answer_title+]">
	<input type="hidden" class="status" name="answer[ [+id+] ][flag]" value="[+status+]">
	<input type="radio" name="true_answer" required>
	<button class="del_answer">Удалить ответ</button>
</li>';
$c['add_answer_line_act'] = '<li class="answer">
	<input type="text" name="answer[ [+id+] ][title]" value="[+answer_title+]">
	<input type="hidden" class="status" name="answer[ [+id+] ][flag]" value="[+status+]">
	<input type="radio" name="true_answer" required checked="checked">
	<button class="del_answer">Удалить ответ</button>
</li>';
$c['quest_tab'] = '<div class="quest_tab">
	<section class="content-header">
		<img src="[+img+]" alt="picture-top">
	</section>
	<section class="test">	
		<h2>[+quest_title+]</h2>
		<form id="test-answer-[+id+]" action="#" method="post" data-id="[+id+]">
			[+answer_list+]
		</form>
	</section>
</div>';
$c['quest_full'] = '<section class="test">
            <h2>[+iter+]. [+quest_title+]</h2>
	<form id="test-answer-1" action="#" method="post" style="pointer-events:none;">
                [+answer_list+]
            </form>
        </section><!--test-->
        <section class="quote">
            <div class="picture-user">
                <img src="images/img-1.png" alt="img-1">
                <b>Анна Лысенко</b>
                <p>Основатель программы Beauty Matrix</p>
            </div>
            <div class="balloon">
                [+quest_correct_answer+]
            </div>
            <br>
        </section>';
$c['answer_list'] = '<p><input id="an[+id_q+]-[+id+]" type="radio" name="answer" value="[+id+]"><label for="an[+id_q+]-[+id+]">
<span class="radio-button"></span>[+answer_title+]</label></p>';
$c['answer_list_user'] = '<p><input id="an1-2" type="radio" name="answer" value="a2" [+cheked+]><label for="an1-2" class="[+class+]">
                    <span class="radio-button"></span>[+answer_title+] [+true_answer+][+false_answer+]</label></p>';
$s = &$this->snippetCache;
$s['Personalize'] = 'return require MODX_BASE_PATH.\'assets/snippets/personalize/snippet.personalize.php\';';
$s['FirstChildRedirect'] = 'return require MODX_BASE_PATH.\'assets/snippets/firstchildredirect/snippet.firstchildredirect.php\';';
$s['Reflect'] = '/*
 * Author: 
 *      Mark Kaplan for MODX CMF
 * 
 * Note: 
 *      If Reflect is not retrieving its own documents, make sure that the
 *          Ditto call feeding it has all of the fields in it that you plan on
 *       calling in your Reflect template. Furthermore, Reflect will ONLY
 *          show what is currently in the Ditto result set.
 *       Thus, if pagination is on it will ONLY show that page\'s items.
*/
 

// ---------------------------------------------------
//  Includes
// ---------------------------------------------------

$reflect_base = isset($reflect_base) ? $modx->config[\'base_path\'].$reflect_base : $modx->config[\'base_path\']."assets/snippets/reflect/";
/*
    Param: ditto_base
    
    Purpose:
    Location of Ditto files

    Options:
    Any valid folder location containing the Ditto source code with a trailing slash

    Default:
    [(base_path)]assets/snippets/ditto/
*/

$config = (isset($config)) ? $config : "default";
/*
    Param: config

    Purpose:
    Load a custom configuration

    Options:
    "default" - default blank config file
    CONFIG_NAME - Other configs installed in the configs folder or in any folder within the MODX base path via @FILE

    Default:
    "default"
    
    Related:
    - <extenders>
*/

require($reflect_base."configs/default.config.php");
require($reflect_base."default.templates.php");
if ($config != "default") {
    require((substr($config, 0, 5) != "@FILE") ? $reflect_base."configs/$config.config.php" : $modx->config[\'base_path\'].trim(substr($config, 5)));
}

// ---------------------------------------------------
//  Parameters
// ---------------------------------------------------

$id = isset($id) ? $id."_" : false;
/*
    Param: id

    Purpose:
    Unique ID for this Ditto instance for connection with other scripts (like Reflect) and unique URL parameters

    Options:
    Any valid folder location containing the Ditto source code with a trailing slash

    Default:
    "" - blank
*/
$getDocuments = isset($getDocuments) ? $getDocuments : 0;
/*
    Param: getDocuments

    Purpose:
    Force Reflect to get documents

    Options:
    0 - off
    1 - on
    
    Default:
    0 - off
*/
$showItems = isset($showItems) ? $showItems : 1;
/*
    Param: showItems

    Purpose:
    Show individual items in the archive

    Options:
    0 - off
    1 - on
    
    Default:
    1 - on
*/
$groupByYears = isset($groupByYears)? $groupByYears : 1;
/*
    Param: groupByYears

    Purpose:
    Group the archive by years

    Options:
    0 - off
    1 - on
    
    Default:
    1 - on
*/
$targetID = isset($targetID) ? $targetID : $modx->documentObject[\'id\'];
/*
    Param: targetID

    Purpose:
    ID for archive links to point to

    Options:
    Any MODX document with a Ditto call setup with extenders=`dateFilter`
    
    Default:
    Current MODX Document
*/
$dateSource = isset($dateSource) ? $dateSource : "createdon";
/*
    Param: dateSource

    Purpose:
    Date source to display for archive items

    Options:
    # - Any UNIX timestamp from MODX fields or TVs such as createdon, pub_date, or editedon
    
    Default:
    "createdon"
    
    Related:
    - <dateFormat>
*/
$dateFormat = isset($dateFormat) ? $dateFormat : "%d-%b-%y %H:%M";  
/*
    Param: dateFormat

    Purpose:
    Format the [+date+] placeholder in human readable form

    Options:
    Any PHP valid strftime option

    Default:
    "%d-%b-%y %H:%M"
    
    Related:
    - <dateSource>
*/
$yearSortDir = isset($yearSortDir) ? $yearSortDir : "DESC";
/*
    Param: yearSortDir

    Purpose:
    Direction to sort documents

    Options:
    ASC - ascending
    DESC - descending

    Default:
    "DESC"
    
    Related:
    - <monthSortDir>
*/
$monthSortDir = isset($monthSortDir) ? $monthSortDir : "ASC";
/*
    Param: monthSortDir

    Purpose:
    Direction to sort the months

    Options:
    ASC - ascending
    DESC - descending

    Default:
    "ASC"
    
    Related:
    - <yearSortDir>
*/
$start = isset($start)? intval($start) : 0;
/*
    Param: start

    Purpose:
    Number of documents to skip in the results
    
    Options:
    Any number

    Default:
    0
*/  
$phx = (isset($phx))? $phx : 1;
/*
    Param: phx

    Purpose:
    Use PHx formatting

    Options:
    0 - off
    1 - on
    
    Default:
    1 - on
*/

// ---------------------------------------------------
//  Initialize Ditto
// ---------------------------------------------------
$placeholder = ($id != false && $getDocuments == 0) ? true : false;
if ($placeholder === false) {
    $rID = "reflect_".rand(1,1000);
    $itemTemplate = isset($tplItem) ? $tplItem: "@CODE:".$defaultTemplates[\'item\'];
    $dParams = array(
        "id" => "$rID",
        "save" => "3",  
        "summarize" => "all",
        "tpl" => $itemTemplate,
    );
    
    $source = $dittoSnippetName;
    $params = $dittoSnippetParameters;
        // TODO: Remove after 3.0
        
    if (isset($params)) {
        $givenParams = explode("|",$params);
        foreach ($givenParams as $parameter) {
            $p = explode(":",$parameter);
            $dParams[$p[0]] = $p[1];
        }
    }
    /*
        Param: params

        Purpose:
        Pass parameters to the Ditto instance used to retreive the documents

        Options:
        Any valid ditto parameters in the format name:value 
        with multiple parameters separated by a pipe (|)
        
        Note:
        This parameter is only needed for config, start, and phx as you can
        now simply use the parameter as if Reflect was Ditto

        Default:
        [NULL]
    */
    
    $reflectParameters = array(\'reflect_base\',\'config\',\'id\',\'getDocuments\',\'showItems\',\'groupByYears\',\'targetID\',\'yearSortDir\',\'monthSortDir\',\'start\',\'phx\',\'tplContainer\',\'tplYear\',\'tplMonth\',\'tplMonthInner\',\'tplItem\',\'save\');
    $params =& $modx->event->params;
    if(is_array($params)) {
        foreach ($params as $param=>$value) {
            if (!in_array($param,$reflectParameters) && substr($param,-3) != \'tpl\') {
                $dParams[$param] = $value;
            }
        }
    }

    $source = isset($source) ? $source : "Ditto";
    /*
        Param: source

        Purpose:
        Name of the Ditto snippet to use

        Options:
        Any valid snippet name

        Default:
        "Ditto"
    */
    $snippetOutput = $modx->runSnippet($source,$dParams);
    $ditto = $modx->getPlaceholder($rID."_ditto_object");
    $resource = $modx->getPlaceholder($rID."_ditto_resource");
} else {
    $ditto = $modx->getPlaceholder($id."ditto_object");
    $resource = $modx->getPlaceholder($id."ditto_resource");
}
if (!is_object($ditto) || !isset($ditto) || !isset($resource)) {
    return !empty($snippetOutput) ? $snippetOutput : "The Ditto object is invalid. Please check it.";
}

// ---------------------------------------------------
//  Templates
// ---------------------------------------------------

$templates[\'tpl\'] = isset($tplContainer) ? $ditto->template->fetch($tplContainer): $defaultTemplates[\'tpl\'];
/*
    Param: tplContainer

    Purpose:
    Container template for the archive

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'year\'] = isset($tplYear) ? $ditto->template->fetch($tplYear): $defaultTemplates[\'year\'];
/*
    Param: tplYear

    Purpose:
    Template for the year item

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'year_inner\'] = isset($tplYearInner) ? $ditto->template->fetch($tplYearInner): $defaultTemplates[\'year_inner\'];
/*
    Param: tplYearInner

    Purpose:
    Template for the year item (the ul to hold the year template)

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'month\'] = isset($tplMonth) ? $ditto->template->fetch($tplMonth): $defaultTemplates[\'month\'];
/*
    Param: tplMonth

    Purpose:
    Template for the month item

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'month_inner\'] = isset($tplMonthInner) ? $ditto->template->fetch($tplMonthInner): $defaultTemplates[\'month_inner\'];
/*
    Param: tplMonthInner

    Purpose:
    Template for the month item  (the ul to hold the month template)

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'item\'] = isset($tplItem) ? $ditto->template->fetch($tplItem): $defaultTemplates[\'item\'];
/*
    Param: tplItem

    Purpose:
    Template for the individual item

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/

$ditto->addField("date","display","custom");
    // force add the date field if receiving data from a Ditto instance

// ---------------------------------------------------
//  Reflect
// ---------------------------------------------------

if (function_exists("reflect") === FALSE) {
function reflect($templatesDocumentID, $showItems, $groupByYears, $resource, $templatesDateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir) {
    global $modx;
    $cal = array();
    $output = \'\';
    $ph = array(\'year\'=>\'\',\'month\'=>\'\',\'item\'=>\'\',\'out\'=>\'\');
    $build = array();
    $stop = count($resource);

    // loop and fetch all the results
    for ($i = $start; $i < $stop; $i++) {
        $date = getdate($resource[$i][$templatesDateSource]);
        $year = $date["year"];
        $month = $date["mon"];
        $cal[$year][$month][] = $resource[$i];
    }
    if ($yearSortDir == "DESC") {
        krsort($cal);
    } else {
        ksort($cal);
    }
    foreach ($cal as $year=>$months) {
        if ($monthSortDir == "ASC") {
            ksort($months);
        } else {
            krsort($months);
        }
        $build[$year] = $months;
    }
    
    foreach ($build as $year=>$months) {
        $r_year = \'\';
        $r_month = \'\';
        $r_month_2 = \'\';
        $year_count = 0;
        $items = array();
        
        foreach ($months as $mon=>$month) {
            $month_text = strftime("%B", mktime(10, 10, 10, $mon, 10, $year));
            $month_url = $ditto->buildURL("month=".$mon."&year=".$year."&day=false&start=0",$templatesDocumentID,$id);
            $month_count = count($month);
            $year_count += $month_count;
            $r_month = $ditto->template->replace(array("year"=>$year,"month"=>$month_text,"url"=>$month_url,"count"=>$month_count),$templates[\'month\']);
            if ($showItems) {
                foreach ($month as $item) {
                    $items[$year][$mon][\'items\'][] = $ditto->render($item, $templates[\'item\'], false, $templatesDateSource, $dateFormat, array(),$phx);
                }
                $r_month_2 = $ditto->template->replace(array(\'wrapper\' => implode(\'\',$items[$year][$mon][\'items\'])),$templates[\'month_inner\']);
                $items[$year][$mon] = $ditto->template->replace(array(\'wrapper\' => $r_month_2),$r_month);
            } else {
                $items[$year][$mon] = $r_month;
            }
        }
        if ($groupByYears) {
            $year_url = $ditto->buildURL("year=".$year."&month=false&day=false&start=0",$templatesDocumentID,$id);
            $r_year =  $ditto->template->replace(array("year"=>$year,"url"=>$year_url,"count"=>$year_count),$templates[\'year\']);
            $var = $ditto->template->replace(array(\'wrapper\'=>implode(\'\',$items[$year])),$templates[\'year_inner\']);
            $output .= $ditto->template->replace(array(\'wrapper\'=>$var),$r_year);
        } else {
            $output .= implode(\'\',$items[$year]);
        }
    }

    $output = $ditto->template->replace(array(\'wrapper\'=>$output),$templates[\'tpl\']);
    $modx->setPlaceholder($id.\'reset\',$ditto->buildURL(\'year=false&month=false&day=false\',$templatesDocumentID,$id));

return $output;
    
}
}

return reflect($targetID, $showItems, $groupByYears, $resource, $dateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir);';
$s['WebChangePwd'] = '# Created By Raymond Irving April, 2005
#::::::::::::::::::::::::::::::::::::::::
# Params:	
#
#	&tpl			- (Optional)
#		Chunk name or document id to use as a template
#				  
#	Note: Templats design:
#			section 1: change pwd template
#			section 2: notification template 
#
# Examples:
#
#	[[WebChangePwd? &tpl=`ChangePwd`]] 

# Set Snippet Paths 
$snipPath  = (($modx->isBackend())? "../":"");
$snipPath .= "assets/snippets/";

# check if inside manager
if ($m = $modx->isBackend()) {
	return \'\'; # don\'t go any further when inside manager
}


# Snippet customize settings
$tpl		= isset($tpl)? $tpl:"";

# System settings
$isPostBack		= count($_POST) && isset($_POST[\'cmdwebchngpwd\']);

# Start processing
include_once $snipPath."weblogin/weblogin.common.inc.php";
include_once $snipPath."weblogin/webchangepwd.inc.php";

# Return
return $output;



';
$s['WebSignup'] = '# Created By Raymond Irving April, 2005
#::::::::::::::::::::::::::::::::::::::::
# Usage:     
#    Allows a web user to signup for a new web account from the website
#    This snippet provides a basic set of form fields for the signup form
#    You can customize this snippet to create your own signup form
#
# Params:    
#
#    &tpl        - (Optional) Chunk name or document id to use as a template
#	    		   If custom template AND captcha on AND using WebSignup and 
#                  WebLogin on the same page make sure you have a field named
#                  cmdwebsignup. In the default template it is the submit button 
#                  One can use a hidden field.
#    &groups     - Web users groups to be assigned to users
#    &useCaptcha - (Optional) Determine to use (1) or not to use (0) captcha
#                  on signup form - if not defined, will default to system
#                  setting. GD is required for this feature. If GD is not 
#                  available, useCaptcha will automatically be set to false;
#                  
#    Note: Templats design:
#        section 1: signup template
#        section 2: notification template 
#
# Examples:
#
#    [[WebSignup? &tpl=`SignupForm` &groups=`NewsReaders,WebUsers`]] 

# Set Snippet Paths 
$snipPath = $modx->config[\'base_path\'] . "assets/snippets/";

# check if inside manager
if ($m = $modx->isBackend()) {
    return \'\'; # don\'t go any further when inside manager
}


# Snippet customize settings
$tpl = isset($tpl)? $tpl:"";
$useCaptcha = isset($useCaptcha)? $useCaptcha : $modx->config[\'use_captcha\'] ;
// Override captcha if no GD
if ($useCaptcha && !gd_info()) $useCaptcha = 0;

# setup web groups
$groups = isset($groups) ? array_filter(array_map(\'trim\', explode(\',\', $groups))):array();

# System settings
$isPostBack        = count($_POST) && isset($_POST[\'cmdwebsignup\']);

$output = \'\';

# Start processing
include_once $snipPath."weblogin/weblogin.common.inc.php";
include_once $snipPath."weblogin/websignup.inc.php";

# Return
return $output;';
$s['WebSignupProps'] = '&tpl=Template;string; ';
$s['MemberCheck'] = 'return require MODX_BASE_PATH.\'assets/snippets/membercheck/snippet.membercheck.php\';';
$s['if'] = 'return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';';
$s['ListIndexer'] = 'return require MODX_BASE_PATH.\'assets/snippets/listindexer/snippet.listindexer.php\';';
$s['phpthumb'] = 'return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';
';
$s['Breadcrumbs'] = 'return require MODX_BASE_PATH.\'assets/snippets/breadcrumbs/snippet.breadcrumbs.php\';';
$s['Ditto'] = 'return require MODX_BASE_PATH.\'assets/snippets/ditto/snippet.ditto.php\';';
$s['Jot'] = '/*####
#
# Author: Armand "bS" Pondman (apondman@zerobarrier.nl)
#
# Latest Version: http://modx.com/extras/package/jot
# Jot Demo Site: http://projects.zerobarrier.nl/modx/
# Documentation: http://wiki.modxcms.com/index.php/Jot (wiki)
#
####*/

$jotPath = $modx->config[\'base_path\'] . \'assets/snippets/jot/\';
include_once($jotPath.\'jot.class.inc.php\');

$Jot = new CJot;
$Jot->VersionCheck("1.1.4");
$Jot->Set("path",$jotPath);
$Jot->Set("action", $action);
$Jot->Set("postdelay", $postdelay);
$Jot->Set("docid", $docid);
$Jot->Set("tagid", $tagid);
$Jot->Set("subscribe", $subscribe);
$Jot->Set("moderated", $moderated);
$Jot->Set("captcha", $captcha);
$Jot->Set("badwords", $badwords);
$Jot->Set("bw", $bw);
$Jot->Set("sortby", $sortby);
$Jot->Set("numdir", $numdir);
$Jot->Set("customfields", $customfields);
$Jot->Set("guestname", $guestname);
$Jot->Set("canpost", $canpost);
$Jot->Set("canview", $canview);
$Jot->Set("canedit", $canedit);
$Jot->Set("canmoderate", $canmoderate);
$Jot->Set("trusted", $trusted);
$Jot->Set("pagination", $pagination);
$Jot->Set("placeholders", $placeholders);
$Jot->Set("subjectSubscribe", $subjectSubscribe);
$Jot->Set("subjectModerate", $subjectModerate);
$Jot->Set("subjectAuthor", $subjectAuthor);
$Jot->Set("notify", $notify);
$Jot->Set("notifyAuthor", $notifyAuthor);
$Jot->Set("validate", $validate);
$Jot->Set("title", $title);
$Jot->Set("authorid", $authorid);
$Jot->Set("css", $css);
$Jot->Set("cssFile", $cssFile);
$Jot->Set("cssRowAlt", $cssRowAlt);
$Jot->Set("cssRowMe", $cssRowMe);
$Jot->Set("cssRowAuthor", $cssRowAuthor);
$Jot->Set("tplForm", $tplForm);
$Jot->Set("tplComments", $tplComments);
$Jot->Set("tplModerate", $tplModerate);
$Jot->Set("tplNav", $tplNav);
$Jot->Set("tplNotify", $tplNotify);
$Jot->Set("tplNotifyModerator", $tplNotifyModerator);
$Jot->Set("tplNotifyAuthor", $tplNotifyAuthor);
$Jot->Set("tplSubscribe", $tplSubscribe);
$Jot->Set("debug", $debug);
$Jot->Set("output", $output);
return $Jot->Run();';
$s['eForm'] = 'return require MODX_BASE_PATH.\'assets/snippets/eform/snippet.eform.php\';';
$s['UltimateParent'] = 'return require MODX_BASE_PATH.\'assets/snippets/ultimateparent/snippet.ultimateparent.php\';';
$s['WebLogin'] = '# Created By Raymond Irving 2004
#::::::::::::::::::::::::::::::::::::::::
# Params:	
#
#	&loginhomeid 	- (Optional)
#		redirects the user to first authorized page in the list.
#		If no id was specified then the login home page id or 
#		the current document id will be used
#
#	&logouthomeid 	- (Optional)
#		document id to load when user logs out	
#
#	&pwdreqid 	- (Optional)
#		document id to load after the user has submited
#		a request for a new password
#
#	&pwdactid 	- (Optional)
#		document id to load when the after the user has activated
#		their new password
#
#	&logintext		- (Optional) 
#		Text to be displayed inside login button (for built-in form)
#
#	&logouttext 	- (Optional)
#		Text to be displayed inside logout link (for built-in form)
#	
#	&tpl			- (Optional)
#		Chunk name or document id to as a template
#				  
#	Note: Templats design:
#			section 1: login template
#			section 2: logout template 
#			section 3: password reminder template 
#
#			See weblogin.tpl for more information
#
# Examples:
#
#	[[WebLogin? &loginhomeid=`8` &logouthomeid=`1`]] 
#
#	[[WebLogin? &loginhomeid=`8,18,7,5` &tpl=`Login`]] 

# Set Snippet Paths 
$snipPath = $modx->config[\'base_path\'] . "assets/snippets/";

# check if inside manager
if ($m = $modx->isBackend()) {
	return \'\'; # don\'t go any further when inside manager
}

# deprecated params - only for backward compatibility
if(isset($loginid)) $loginhomeid=$loginid;
if(isset($logoutid)) $logouthomeid = $logoutid;
if(isset($template)) $tpl = $template;

# Snippet customize settings
$liHomeId	= isset($loginhomeid)? array_filter(array_map(\'intval\', explode(\',\', $loginhomeid))):array($modx->config[\'login_home\'],$modx->documentIdentifier);
$loHomeId	= isset($logouthomeid)? $logouthomeid:$modx->documentIdentifier;
$pwdReqId	= isset($pwdreqid)? $pwdreqid:0;
$pwdActId	= isset($pwdactid)? $pwdactid:0;
$loginText	= isset($logintext)? $logintext:\'Login\';
$logoutText	= isset($logouttext)? $logouttext:\'Logout\';
$tpl		= isset($tpl)? $tpl:"";

# System settings
$webLoginMode = isset($_REQUEST[\'webloginmode\'])? $_REQUEST[\'webloginmode\']: \'\';
$isLogOut		= $webLoginMode==\'lo\' ? 1:0;
$isPWDActivate	= $webLoginMode==\'actp\' ? 1:0;
$isPostBack		= count($_POST) && (isset($_POST[\'cmdweblogin\']) || isset($_POST[\'cmdweblogin_x\']));
$txtPwdRem 		= isset($_REQUEST[\'txtpwdrem\'])? $_REQUEST[\'txtpwdrem\']: 0;
$isPWDReminder	= $isPostBack && $txtPwdRem==\'1\' ? 1:0;

$site_id = isset($site_id)? $site_id: \'\';
$cookieKey = substr(md5($site_id."Web-User"),0,15);

# Start processing
include_once $snipPath."weblogin/weblogin.common.inc.php";
include_once ($modx->config[\'site_manager_path\'] . "includes/crypt.class.inc.php");

if ($isPWDActivate || $isPWDReminder || $isLogOut || $isPostBack) {
	# include the logger class
	include_once $modx->config[\'site_manager_path\'] . "includes/log.class.inc.php";
	include_once $snipPath."weblogin/weblogin.processor.inc.php";
}

include_once $snipPath."weblogin/weblogin.inc.php";

# Return
return $output;
';
$s['WebLoginProps'] = '&loginhomeid=Login Home Id;string; &logouthomeid=Logout Home Id;string; &logintext=Login Button Text;string; &logouttext=Logout Button Text;string; &tpl=Template;string; ';
$s['Wayfinder'] = 'return require MODX_BASE_PATH.\'assets/snippets/wayfinder/snippet.wayfinder.php\';
';
$s['load_quest'] = '
include_once (MODX_BASE_PATH.\'assets/modules/test_folder/test_content.class.php\');

$work = new test_content($modx);

echo($work->make_quest(intval($id)));
';
$s['chek_form'] = '
include_once (MODX_BASE_PATH.\'assets/modules/test_folder/test_content.class.php\');

$work = new test_content($modx);

echo($work->chek_form());
';
$s['find_worker'] = '
$result = $modx->db->query(\'SELECT `id` FROM `modx_site_content` WHERE `parent`=\'.$id.\' AND `template` = 7\');
$arResult = $modx->db->makeArray($result);
return $arResult[0][\'id\'];
';
$s['find_result_page'] = '
$result = $modx->db->query(\'SELECT `id` FROM `modx_site_content` WHERE `parent`=\'.$id.\' AND `template` = 6\');
$arResult = $modx->db->makeArray($result);
return $arResult[0][\'id\'];
';
$s['load_user_answer'] = '
include_once (MODX_BASE_PATH.\'assets/modules/test_folder/test_content.class.php\');

$work = new test_content($modx);

echo($work->load_user_answer($_GET));
';
$s['result_form'] = '
include_once (MODX_BASE_PATH.\'assets/modules/test_folder/test_content.class.php\');

$work = new test_content($modx);

echo($work->result_form());
';
$p = &$this->pluginCache;
$p['TinyMCE Rich Text Editor'] = 'require MODX_BASE_PATH.\'assets/plugins/tinymce/plugin.tinymce.php\';
';
$p['TinyMCE Rich Text EditorProps'] = '&customparams=Custom Parameters;textarea;valid_elements : "*[*]", &mce_formats=Block Formats;text;p,h1,h2,h3,h4,h5,h6,div,blockquote,code,pre &entity_encoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &mce_path_options=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &mce_resizing=Advanced Resizing;list;true,false;true &disabledButtons=Disabled Buttons;text; &link_list=Link List;list;enabled,disabled;enabled &webtheme=Web Theme;list;simple,editor,creative,custom;simple &webPlugins=Web Plugins;text;style,advimage,advlink,searchreplace,contextmenu,paste,fullscreen,xhtmlxtras,media &webButtons1=Web Buttons 1;text;undo,redo,selectall,|,pastetext,pasteword,|,search,replace,|,hr,charmap,|,image,link,unlink,anchor,media,|,cleanup,removeformat,|,fullscreen,code,help &webButtons2=Web Buttons 2;text;bold,italic,underline,strikethrough,sub,sup,|,|,blockquote,bullist,numlist,outdent,indent,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,|,styleprops &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;500 ';
$p['CodeMirror'] = '$_CM_BASE = \'assets/plugins/codemirror/\';

$_CM_URL = $modx->config[\'site_url\'] . $_CM_BASE;

require(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');

';
$p['CodeMirrorProps'] = '&theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light; &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;true ';
$p['ManagerManager'] = '// You can put your ManagerManager rules EITHER in a chunk OR in an external file - whichever suits your development style the best

// To use an external file, put your rules in /assets/plugins/managermanager/mm_rules.inc.php 
// (you can rename default.mm_rules.inc.php and use it as an example)
// The chunk SHOULD have php opening tags at the beginning and end

// If you want to put your rules in a chunk (so you can edit them through the Manager),
// create the chunk, and enter its name in the configuration tab.
// The chunk should NOT have php tags at the beginning or end.

// See also user-friendly module for editing ManagerManager configuration file ddMMEditor (http://code.divandesign.biz/modx/ddmmeditor).

// ManagerManager requires jQuery 1.9.1, which is located in /assets/plugins/managermanager/js/ folder.

// You don\'t need to change anything else from here onwards
//-------------------------------------------------------

// Run the main code
include($modx->config[\'base_path\'].\'assets/plugins/managermanager/mm.inc.php\');';
$p['ManagerManagerProps'] = '&remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules ';
$p['FileSource'] = 'require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';';
$p['TransAlias'] = 'require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';';
$p['TransAliasProps'] = '&table_name=Trans table;list;common,russian,dutch,german,czech,utf8,utf8lowercase;russian &char_restrict=Restrict alias to;list;lowercase alphanumeric,alphanumeric,legal characters;legal characters &remove_periods=Remove Periods;list;Yes,No;No &word_separator=Word Separator;list;dash,underscore,none;dash &override_tv=Override TV name;string; ';
$e = &$this->pluginEvent;
$e['OnBeforeDocFormSave'] = array('ManagerManager');
$e['OnBeforePluginFormSave'] = array('FileSource');
$e['OnBeforeSnipFormSave'] = array('FileSource');
$e['OnChunkFormRender'] = array('CodeMirror');
$e['OnDocDuplicate'] = array('ManagerManager');
$e['OnDocFormPrerender'] = array('ManagerManager');
$e['OnDocFormRender'] = array('CodeMirror','ManagerManager');
$e['OnDocFormSave'] = array('ManagerManager');
$e['OnInterfaceSettingsRender'] = array('TinyMCE Rich Text Editor');
$e['OnModFormRender'] = array('CodeMirror');
$e['OnPluginFormPrerender'] = array('FileSource');
$e['OnPluginFormRender'] = array('ManagerManager','CodeMirror','FileSource');
$e['OnRichTextEditorInit'] = array('TinyMCE Rich Text Editor');
$e['OnRichTextEditorRegister'] = array('TinyMCE Rich Text Editor');
$e['OnSnipFormPrerender'] = array('FileSource');
$e['OnSnipFormRender'] = array('CodeMirror','FileSource');
$e['OnStripAlias'] = array('TransAlias');
$e['OnTempFormRender'] = array('CodeMirror');
$e['OnTVFormRender'] = array('ManagerManager');

