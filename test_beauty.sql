-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 18 2016 г., 11:15
-- Версия сервера: 5.1.72-0ubuntu0.10.04.1
-- Версия PHP: 5.3.5-1ubuntu7.2ppa1~lucid1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_beauty`
--

-- --------------------------------------------------------

--
-- Структура таблицы `modx_active_users`
--

CREATE TABLE IF NOT EXISTS `modx_active_users` (
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `id` int(10) DEFAULT NULL,
  `action` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data about active users.';

--
-- Дамп данных таблицы `modx_active_users`
--

INSERT INTO `modx_active_users` (`internalKey`, `username`, `lasthit`, `id`, `action`, `ip`) VALUES
(1, 'anser', 1453104794, NULL, '31', '185.36.62.244'),
(2, 'den', 1452953053, NULL, '8', '90.154.70.169');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_categories`
--

CREATE TABLE IF NOT EXISTS `modx_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Categories to be used snippets,tv,chunks, etc' AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `modx_categories`
--

INSERT INTO `modx_categories` (`id`, `category`) VALUES
(1, 'Demo Content'),
(2, 'Js'),
(3, 'Login'),
(4, 'Manager and Admin'),
(5, 'Search'),
(6, 'Navigation'),
(7, 'Content'),
(8, 'Forms'),
(9, 'test_adminka'),
(10, 'front');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_documentgroup_names`
--

CREATE TABLE IF NOT EXISTS `modx_documentgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `private_memgroup` tinyint(4) DEFAULT '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) DEFAULT '0' COMMENT 'determines whether the document is private to web users',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_document_groups`
--

CREATE TABLE IF NOT EXISTS `modx_document_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document` (`document`),
  KEY `document_group` (`document_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_event_log`
--

CREATE TABLE IF NOT EXISTS `modx_event_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventid` int(11) DEFAULT '0',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1- information, 2 - warning, 3- error',
  `user` int(11) NOT NULL DEFAULT '0' COMMENT 'link to user table',
  `usertype` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - manager, 1 - web',
  `source` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Stores event and error logs' AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `modx_event_log`
--

INSERT INTO `modx_event_log` (`id`, `eventid`, `createdon`, `type`, `user`, `usertype`, `source`, `description`) VALUES
(1, 0, 1452283245, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - Unknown column ''type'' in ''field list'' &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">INSERT INTO test_quest (`type`, `quest_title`, `quest_sort`, `quest_correct_answer`, `id_test`) VALUES(''add_new_quest'', ''sdf'', ''sdf'', ''<p>sdf</p>'', ''2'')</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0011 s (3 Requests)</td></tr><tr><td>PHP : </td><td>0.0294 s</td></tr><tr><td>Total : </td><td>0.0305 s</td></tr><tr><td>Memory : </td><td>2.5758857727051 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->work_form()<br />assets/modules/test_folder/test_content.class.php on line 13</td><tr><td valign="top">6</td><td>DBAPI->insert()<br />assets/modules/test_folder/test_content.class.php on line 57</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 282</td></table>\n'),
(2, 0, 1452283567, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">SELECT * FROM `test_quest` WHERE `id_test`=</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0028 s (3 Requests)</td></tr><tr><td>PHP : </td><td>0.0296 s</td></tr><tr><td>Total : </td><td>0.0324 s</td></tr><tr><td>Memory : </td><td>2.5777816772461 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->make_quest_arr()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->query()<br />assets/modules/test_folder/test_content.class.php on line 38</td></table>\n'),
(3, 0, 1452526087, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - Unknown column ''title'' in ''field list'' &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">INSERT INTO test_answer (`title`, `flag`) VALUES(''sdf'', ''0'')</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0010 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0299 s</td></tr><tr><td>Total : </td><td>0.0309 s</td></tr><tr><td>Memory : </td><td>2.5735054016113 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->insert()<br />assets/modules/test_folder/test_content.class.php on line 113</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 282</td></table>\n'),
(4, 0, 1452526574, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''SET `status` = ''0'',`answer_title` = ''111111111'',`id_quest` = ''2'''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE id = SET `status` = ''0'',`answer_title` = ''111111111'',`id_quest` = ''2'' </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0014 s (5 Requests)</td></tr><tr><td>PHP : </td><td>0.0295 s</td></tr><tr><td>Total : </td><td>0.0309 s</td></tr><tr><td>Memory : </td><td>2.5734825134277 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 113</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(5, 0, 1452526611, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''SET `status` = ''0'',`answer_title` = ''111111111'',`id_quest` = ''2'''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE id = SET `status` = ''0'',`answer_title` = ''111111111'',`id_quest` = ''2'' </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0010 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0327 s</td></tr><tr><td>Total : </td><td>0.0338 s</td></tr><tr><td>Memory : </td><td>2.5734825134277 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 113</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(6, 0, 1452527620, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE id = SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'' </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0011 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0297 s</td></tr><tr><td>Total : </td><td>0.0307 s</td></tr><tr><td>Memory : </td><td>2.5694465637207 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 115</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(7, 0, 1452527680, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE id = SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'' </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0029 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0297 s</td></tr><tr><td>Total : </td><td>0.0326 s</td></tr><tr><td>Memory : </td><td>2.5694465637207 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 115</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(8, 0, 1452527705, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE id = SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'' </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0009 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0301 s</td></tr><tr><td>Total : </td><td>0.0310 s</td></tr><tr><td>Memory : </td><td>2.5694465637207 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 115</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(9, 0, 1452528002, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE id = SET `status` = ''0'',`answer_title` = ''sdf'',`id_quest` = ''2'' </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0143 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0373 s</td></tr><tr><td>Total : </td><td>0.0516 s</td></tr><tr><td>Memory : </td><td>2.5694465637207 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 115</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(10, 0, 1452528049, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''SET `status` = ''0'',`answer_title` = ''sdf'''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE id = SET `status` = ''0'',`answer_title` = ''sdf'' </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0014 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0306 s</td></tr><tr><td>Total : </td><td>0.0320 s</td></tr><tr><td>Memory : </td><td>2.5694465637207 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 114</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(11, 0, 1452528103, 3, 1, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '''' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">UPDATE test_answer SET `status` = ''0'',`answer_title` = ''sdf'' WHERE id = </span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>185.36.62.245</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0008 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0278 s</td></tr><tr><td>Total : </td><td>0.0287 s</td></tr><tr><td>Memory : </td><td>2.5694465637207 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->save_quest()<br />assets/modules/test_folder/test_content.class.php on line 18</td><tr><td valign="top">6</td><td>DBAPI->update()<br />assets/modules/test_folder/test_content.class.php on line 114</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 263</td></table>\n'),
(12, 0, 1452590274, 3, 2, 0, 'SQL Query - Execute module', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''Calibri'',sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family:'' at line 1 &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">INSERT INTO test_quest (`quest_title`, `quest_sort`, `quest_correct_answer`, `id_test`) VALUES(''Можно ли использовать специи при приготовлении пп-блюд?'', ''9'', ''<p><span style="font-size: 11.0pt; line-height: 107%; font-family: ''Calibri'',sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: ''Times New Roman''; mso-bidi-theme-font: minor-bidi; mso-ansi-language: RU; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;">Специи использовать можно, если они натуральные! Обычно специями мы называем то,&nbsp;чем можно ароматизировать пищу, улучшать ее вкусовые качества. Это&nbsp;&ndash; высушенная зелень, семена растений и&nbsp;трав, растертые в&nbsp;порошок листья и&nbsp;высушенные плоды&hellip; Многие специи обладают не только отличными вкусовыми, но и&nbsp;лечебными качествами. Использование зелени (не только свежей, но и&nbsp;сушеной) улучшает процесс пищеварения, обмен веществ.&nbsp;Однако, поскольку большинство специй содержат эфирные масла, ароматические кислоты, дубильные вещества, они могут вызвать раздражение желудочно-кишечного тракта, а&nbsp;это&nbsp;&ndash; риск развития воспалительных процессов. Поэтому тем, у&nbsp;кого есть проблемы с&nbsp;желудочно-кишечным трактом,&nbsp;использовать специи&nbsp;нужно очень осторожно. Кроме того, надо иметь в&nbsp;виду, что многие специи аллергенны.</span></p>'', ''2'')</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;id=2</td></tr><tr><td valign="top">Manager action : </td><td>112 - Execute module</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/manager/index.php?a=112&amp;amp;id=2</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>91.79.135.74</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0047 s (3 Requests)</td></tr><tr><td>PHP : </td><td>0.0301 s</td></tr><tr><td>Total : </td><td>0.0348 s</td></tr><tr><td>Memory : </td><td>2.5677032470703 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>include_once()<br />manager/index.php on line 561</td><tr><td valign="top">2</td><td>evalModule()<br />manager/processors/execute_module.processor.php on line 69</td><tr><td valign="top">3</td><td>eval()<br />manager/processors/execute_module.processor.php on line 82</td><tr><td valign="top">4</td><td>test_content->creat_view()<br />manager/processors/execute_module.processor.php(82) : eval()''d code on line 5</td><tr><td valign="top">5</td><td>test_content->add_new_quest()<br />assets/modules/test_folder/test_content.class.php on line 15</td><tr><td valign="top">6</td><td>DBAPI->insert()<br />assets/modules/test_folder/test_content.class.php on line 96</td><tr><td valign="top">7</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 282</td></table>\n'),
(13, 0, 1452611641, 3, 0, 0, 'Snippet - find_worker', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed -  &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">Resource id #26</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/test_1</td></tr><tr><td valign="top">Resource : </td><td>[2]<a href="http://test.beauty-matrix.ru/test_1" target="_blank">test_1</a></td></tr><tr><td>Current Snippet : </td><td>find_worker</td></tr><tr><td>Referer : </td><td></td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0016 s (17 Requests)</td></tr><tr><td>PHP : </td><td>0.0191 s</td></tr><tr><td>Total : </td><td>0.0206 s</td></tr><tr><td>Memory : </td><td>1.4224891662598 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>DocumentParser->executeParser()<br />index.php on line 144</td><tr><td valign="top">2</td><td>DocumentParser->prepareResponse()<br />manager/includes/document.parser.class.inc.php on line 1576</td><tr><td valign="top">3</td><td>DocumentParser->outputContent()<br />manager/includes/document.parser.class.inc.php on line 1678</td><tr><td valign="top">4</td><td>DocumentParser->parseDocumentSource()<br />manager/includes/document.parser.class.inc.php on line 583</td><tr><td valign="top">5</td><td>DocumentParser->evalSnippets()<br />manager/includes/document.parser.class.inc.php on line 1458</td><tr><td valign="top">6</td><td>DocumentParser->_get_snip_result()<br />manager/includes/document.parser.class.inc.php on line 1047</td><tr><td valign="top">7</td><td>DocumentParser->evalSnippet()<br />manager/includes/document.parser.class.inc.php on line 1134</td><tr><td valign="top">8</td><td>eval()<br />manager/includes/document.parser.class.inc.php on line 996</td><tr><td valign="top">9</td><td>DBAPI->query()<br />manager/includes/document.parser.class.inc.php(996) : eval()''d code on line 3</td></table>\n'),
(14, 0, 1452611672, 3, 0, 0, 'Snippet - find_worker', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed -  &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">Resource id #26</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/test_1</td></tr><tr><td valign="top">Resource : </td><td>[2]<a href="http://test.beauty-matrix.ru/test_1" target="_blank">test_1</a></td></tr><tr><td>Current Snippet : </td><td>find_worker</td></tr><tr><td>Referer : </td><td></td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0015 s (17 Requests)</td></tr><tr><td>PHP : </td><td>0.0187 s</td></tr><tr><td>Total : </td><td>0.0201 s</td></tr><tr><td>Memory : </td><td>1.4224891662598 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>DocumentParser->executeParser()<br />index.php on line 144</td><tr><td valign="top">2</td><td>DocumentParser->prepareResponse()<br />manager/includes/document.parser.class.inc.php on line 1576</td><tr><td valign="top">3</td><td>DocumentParser->outputContent()<br />manager/includes/document.parser.class.inc.php on line 1678</td><tr><td valign="top">4</td><td>DocumentParser->parseDocumentSource()<br />manager/includes/document.parser.class.inc.php on line 583</td><tr><td valign="top">5</td><td>DocumentParser->evalSnippets()<br />manager/includes/document.parser.class.inc.php on line 1458</td><tr><td valign="top">6</td><td>DocumentParser->_get_snip_result()<br />manager/includes/document.parser.class.inc.php on line 1047</td><tr><td valign="top">7</td><td>DocumentParser->evalSnippet()<br />manager/includes/document.parser.class.inc.php on line 1134</td><tr><td valign="top">8</td><td>eval()<br />manager/includes/document.parser.class.inc.php on line 996</td><tr><td valign="top">9</td><td>DBAPI->query()<br />manager/includes/document.parser.class.inc.php(996) : eval()''d code on line 3</td></table>\n'),
(15, 0, 1452614964, 3, 0, 0, 'Snippet - chek_form', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; `` is not numeric and may not be passed to makeUrl() &raquo;</b></td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/chek_form</td></tr><tr><td valign="top">Resource : </td><td>[5]<a href="http://test.beauty-matrix.ru/chek_form" target="_blank">chek_form</a></td></tr><tr><td>Current Snippet : </td><td>chek_form</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/test_1</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0005 s (2 Requests)</td></tr><tr><td>PHP : </td><td>0.0170 s</td></tr><tr><td>Total : </td><td>0.0175 s</td></tr><tr><td>Memory : </td><td>1.6672973632812 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>DocumentParser->executeParser()<br />index.php on line 144</td><tr><td valign="top">2</td><td>DocumentParser->prepareResponse()<br />manager/includes/document.parser.class.inc.php on line 1576</td><tr><td valign="top">3</td><td>DocumentParser->outputContent()<br />manager/includes/document.parser.class.inc.php on line 1678</td><tr><td valign="top">4</td><td>DocumentParser->parseDocumentSource()<br />manager/includes/document.parser.class.inc.php on line 583</td><tr><td valign="top">5</td><td>DocumentParser->evalSnippets()<br />manager/includes/document.parser.class.inc.php on line 1458</td><tr><td valign="top">6</td><td>DocumentParser->_get_snip_result()<br />manager/includes/document.parser.class.inc.php on line 1047</td><tr><td valign="top">7</td><td>DocumentParser->evalSnippet()<br />manager/includes/document.parser.class.inc.php on line 1134</td><tr><td valign="top">8</td><td>eval()<br />manager/includes/document.parser.class.inc.php on line 996</td><tr><td valign="top">9</td><td>test_content->chek_form()<br />manager/includes/document.parser.class.inc.php(996) : eval()''d code on line 6</td><tr><td valign="top">10</td><td>DocumentParser->makeUrl()<br />assets/modules/test_folder/test_content.class.php on line 167</td></table>\n'),
(16, 0, 1452614982, 3, 0, 0, 'Snippet - chek_form', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - Unknown column ''name'' in ''field list'' &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">INSERT INTO test_user_answer (`name`, `mail`, `test_id`, `hash_user`, `page`) VALUES(''asd'', ''asd@asdma.rt'', ''2'', ''de339bca970fbbf3678a18db6f6d6d8a'', ''/0'')</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/chek_form</td></tr><tr><td valign="top">Resource : </td><td>[5]<a href="http://test.beauty-matrix.ru/chek_form" target="_blank">chek_form</a></td></tr><tr><td>Current Snippet : </td><td>chek_form</td></tr><tr><td>Referer : </td><td>http://test.beauty-matrix.ru/test_1</td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0009 s (3 Requests)</td></tr><tr><td>PHP : </td><td>0.0172 s</td></tr><tr><td>Total : </td><td>0.0181 s</td></tr><tr><td>Memory : </td><td>1.6672973632812 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>DocumentParser->executeParser()<br />index.php on line 144</td><tr><td valign="top">2</td><td>DocumentParser->prepareResponse()<br />manager/includes/document.parser.class.inc.php on line 1576</td><tr><td valign="top">3</td><td>DocumentParser->outputContent()<br />manager/includes/document.parser.class.inc.php on line 1678</td><tr><td valign="top">4</td><td>DocumentParser->parseDocumentSource()<br />manager/includes/document.parser.class.inc.php on line 583</td><tr><td valign="top">5</td><td>DocumentParser->evalSnippets()<br />manager/includes/document.parser.class.inc.php on line 1458</td><tr><td valign="top">6</td><td>DocumentParser->_get_snip_result()<br />manager/includes/document.parser.class.inc.php on line 1047</td><tr><td valign="top">7</td><td>DocumentParser->evalSnippet()<br />manager/includes/document.parser.class.inc.php on line 1134</td><tr><td valign="top">8</td><td>eval()<br />manager/includes/document.parser.class.inc.php on line 996</td><tr><td valign="top">9</td><td>test_content->chek_form()<br />manager/includes/document.parser.class.inc.php(996) : eval()''d code on line 6</td><tr><td valign="top">10</td><td>DBAPI->insert()<br />assets/modules/test_folder/test_content.class.php on line 177</td><tr><td valign="top">11</td><td>DBAPI->query()<br />manager/includes/extenders/dbapi.mysql.class.inc.php on line 282</td></table>\n'),
(17, 0, 1452691608, 3, 0, 0, 'Snippet - load_user_answer', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - Unknown column ''hash'' in ''where clause'' &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">SELECT id FROM `test_user` WHERE `hash` = ''Array''</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/stranica-otvetov?uid=cec163c71af35c8cf8fee97b69bb510b</td></tr><tr><td valign="top">Resource : </td><td>[3]<a href="http://test.beauty-matrix.ru/stranica-otvetov" target="_blank">страница ответов</a></td></tr><tr><td>Current Snippet : </td><td>load_user_answer</td></tr><tr><td>Referer : </td><td></td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0072 s (2 Requests)</td></tr><tr><td>PHP : </td><td>0.0210 s</td></tr><tr><td>Total : </td><td>0.0282 s</td></tr><tr><td>Memory : </td><td>1.6720581054688 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>DocumentParser->executeParser()<br />index.php on line 144</td><tr><td valign="top">2</td><td>DocumentParser->prepareResponse()<br />manager/includes/document.parser.class.inc.php on line 1576</td><tr><td valign="top">3</td><td>DocumentParser->outputContent()<br />manager/includes/document.parser.class.inc.php on line 1678</td><tr><td valign="top">4</td><td>DocumentParser->parseDocumentSource()<br />manager/includes/document.parser.class.inc.php on line 583</td><tr><td valign="top">5</td><td>DocumentParser->evalSnippets()<br />manager/includes/document.parser.class.inc.php on line 1458</td><tr><td valign="top">6</td><td>DocumentParser->_get_snip_result()<br />manager/includes/document.parser.class.inc.php on line 1047</td><tr><td valign="top">7</td><td>DocumentParser->evalSnippet()<br />manager/includes/document.parser.class.inc.php on line 1134</td><tr><td valign="top">8</td><td>eval()<br />manager/includes/document.parser.class.inc.php on line 996</td><tr><td valign="top">9</td><td>test_content->load_user_answer()<br />manager/includes/document.parser.class.inc.php(996) : eval()''d code on line 6</td><tr><td valign="top">10</td><td>DBAPI->query()<br />assets/modules/test_folder/test_content.class.php on line 222</td></table>\n');
INSERT INTO `modx_event_log` (`id`, `eventid`, `createdon`, `type`, `user`, `usertype`, `source`, `description`) VALUES
(18, 0, 1452691629, 3, 0, 0, 'Snippet - load_user_answer', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; Execution of a query to the database failed - Unknown column ''hash'' in ''where clause'' &raquo;</b></td></tr><tr><td colspan="2"><div style="font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;">SQL &gt; <span id="sqlHolder">SELECT id FROM `test_user` WHERE `hash` = ''cec163c71af35c8cf8fee97b69bb510b''</span></div>\n	                </td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/stranica-otvetov?uid=cec163c71af35c8cf8fee97b69bb510b</td></tr><tr><td valign="top">Resource : </td><td>[3]<a href="http://test.beauty-matrix.ru/stranica-otvetov" target="_blank">страница ответов</a></td></tr><tr><td>Current Snippet : </td><td>load_user_answer</td></tr><tr><td>Referer : </td><td></td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0005 s (2 Requests)</td></tr><tr><td>PHP : </td><td>0.0174 s</td></tr><tr><td>Total : </td><td>0.0180 s</td></tr><tr><td>Memory : </td><td>1.6720581054688 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>DocumentParser->executeParser()<br />index.php on line 144</td><tr><td valign="top">2</td><td>DocumentParser->prepareResponse()<br />manager/includes/document.parser.class.inc.php on line 1576</td><tr><td valign="top">3</td><td>DocumentParser->outputContent()<br />manager/includes/document.parser.class.inc.php on line 1678</td><tr><td valign="top">4</td><td>DocumentParser->parseDocumentSource()<br />manager/includes/document.parser.class.inc.php on line 583</td><tr><td valign="top">5</td><td>DocumentParser->evalSnippets()<br />manager/includes/document.parser.class.inc.php on line 1458</td><tr><td valign="top">6</td><td>DocumentParser->_get_snip_result()<br />manager/includes/document.parser.class.inc.php on line 1047</td><tr><td valign="top">7</td><td>DocumentParser->evalSnippet()<br />manager/includes/document.parser.class.inc.php on line 1134</td><tr><td valign="top">8</td><td>eval()<br />manager/includes/document.parser.class.inc.php on line 996</td><tr><td valign="top">9</td><td>test_content->load_user_answer()<br />manager/includes/document.parser.class.inc.php(996) : eval()''d code on line 6</td><tr><td valign="top">10</td><td>DBAPI->query()<br />assets/modules/test_folder/test_content.class.php on line 222</td></table>\n'),
(19, 0, 1452692074, 3, 0, 0, 'Snippet - load_user_answer', '<h3 style="color:red">&laquo; MODX Parse Error &raquo;</h3>\n	                <table border="0" cellpadding="1" cellspacing="0">\n	                <tr><td colspan="2">MODX encountered the following error while attempting to parse the requested resource:</td></tr>\n	                <tr><td colspan="2"><b style="color:red;">&laquo; `` is not numeric and may not be passed to makeUrl() &raquo;</b></td></tr><tr><td colspan="2"><b>Basic info</b></td></tr><tr><td valign="top" style="white-space:nowrap;">REQUEST_URI : </td><td>http://test.beauty-matrix.ru/stranica-otvetov?uid=cec163c71af35c8cf8fee97b69bb510</td></tr><tr><td valign="top">Resource : </td><td>[3]<a href="http://test.beauty-matrix.ru/stranica-otvetov" target="_blank">страница ответов</a></td></tr><tr><td>Current Snippet : </td><td>load_user_answer</td></tr><tr><td>Referer : </td><td></td></tr><tr><td>User Agent : </td><td>Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36</td></tr><tr><td>IP : </td><td>188.126.39.105</td></tr><tr><td colspan="2"><b>Benchmarks</b></td></tr><tr><td>MySQL : </td><td>0.0009 s (4 Requests)</td></tr><tr><td>PHP : </td><td>0.0167 s</td></tr><tr><td>Total : </td><td>0.0176 s</td></tr><tr><td>Memory : </td><td>1.672176361084 mb</td></tr></table>\n<br /><p><b>Backtrace</b></p>\n<table><tr><td valign="top">1</td><td>DocumentParser->executeParser()<br />index.php on line 144</td><tr><td valign="top">2</td><td>DocumentParser->prepareResponse()<br />manager/includes/document.parser.class.inc.php on line 1576</td><tr><td valign="top">3</td><td>DocumentParser->outputContent()<br />manager/includes/document.parser.class.inc.php on line 1678</td><tr><td valign="top">4</td><td>DocumentParser->parseDocumentSource()<br />manager/includes/document.parser.class.inc.php on line 583</td><tr><td valign="top">5</td><td>DocumentParser->evalSnippets()<br />manager/includes/document.parser.class.inc.php on line 1458</td><tr><td valign="top">6</td><td>DocumentParser->_get_snip_result()<br />manager/includes/document.parser.class.inc.php on line 1047</td><tr><td valign="top">7</td><td>DocumentParser->evalSnippet()<br />manager/includes/document.parser.class.inc.php on line 1134</td><tr><td valign="top">8</td><td>eval()<br />manager/includes/document.parser.class.inc.php on line 996</td><tr><td valign="top">9</td><td>test_content->load_user_answer()<br />manager/includes/document.parser.class.inc.php(996) : eval()''d code on line 6</td><tr><td valign="top">10</td><td>DocumentParser->makeUrl()<br />assets/modules/test_folder/test_content.class.php on line 227</td></table>\n'),
(20, 0, 1452953118, 3, 0, 0, 'phpmailer', 'Could not instantiate mail function.'),
(21, 0, 1452953237, 3, 0, 0, 'phpmailer', 'Could not instantiate mail function.');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_keyword_xref`
--

CREATE TABLE IF NOT EXISTS `modx_keyword_xref` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `keyword_id` int(11) NOT NULL DEFAULT '0',
  KEY `content_id` (`content_id`),
  KEY `keyword_id` (`keyword_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Cross reference bewteen keywords and content';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_manager_log`
--

CREATE TABLE IF NOT EXISTS `modx_manager_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `timestamp` int(20) NOT NULL DEFAULT '0',
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` int(10) NOT NULL DEFAULT '0',
  `itemid` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `itemname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains a record of user interaction.' AUTO_INCREMENT=1033 ;

--
-- Дамп данных таблицы `modx_manager_log`
--

INSERT INTO `modx_manager_log` (`id`, `timestamp`, `internalKey`, `username`, `action`, `itemid`, `itemname`, `message`) VALUES
(1, 1452271018, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(2, 1452271019, 1, 'anser', 17, '-', '-', 'Editing settings'),
(3, 1452271137, 1, 'anser', 30, '-', '-', 'Saving settings'),
(4, 1452271139, 1, 'anser', 76, '-', '-', 'Element management'),
(5, 1452271171, 1, 'anser', 102, '7', 'TransAlias', 'Edit plugin'),
(6, 1452271178, 1, 'anser', 103, '7', 'TransAlias', 'Saving plugin'),
(7, 1452271178, 1, 'anser', 76, '-', '-', 'Element management'),
(8, 1452271180, 1, 'anser', 27, '1', 'MODX CMS Install Success', 'Editing resource'),
(9, 1452271191, 1, 'anser', 5, '1', 'главная', 'Saving resource'),
(10, 1452271192, 1, 'anser', 3, '1', 'главная', 'Viewing data for resource'),
(11, 1452271204, 1, 'anser', 76, '-', '-', 'Element management'),
(12, 1452271205, 1, 'anser', 19, '-', 'Новый шаблон', 'Creating a new template'),
(13, 1452271242, 1, 'anser', 20, '-', 'test_1_form', 'Saving template'),
(14, 1452271242, 1, 'anser', 76, '-', '-', 'Element management'),
(15, 1452271246, 1, 'anser', 19, '-', 'Новый шаблон', 'Creating a new template'),
(16, 1452271276, 1, 'anser', 20, '-', 'test_1_answer', 'Saving template'),
(17, 1452271276, 1, 'anser', 76, '-', '-', 'Element management'),
(18, 1452271278, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(19, 1452271284, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(20, 1452271286, 1, 'anser', 5, '-', 'test_1', 'Saving resource'),
(21, 1452271287, 1, 'anser', 3, '2', 'test_1', 'Viewing data for resource'),
(22, 1452271290, 1, 'anser', 61, '2', 'test_1', 'Publishing a resource'),
(23, 1452271292, 1, 'anser', 3, '2', 'test_1', 'Viewing data for resource'),
(24, 1452271294, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(25, 1452271311, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(26, 1452271313, 1, 'anser', 5, '-', 'страница ответов', 'Saving resource'),
(27, 1452271314, 1, 'anser', 3, '3', 'страница ответов', 'Viewing data for resource'),
(28, 1452271319, 1, 'anser', 61, '3', 'страница ответов', 'Publishing a resource'),
(29, 1452271320, 1, 'anser', 3, '2', 'test_1', 'Viewing data for resource'),
(30, 1452271970, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(31, 1452271975, 1, 'anser', 5, '-', 'test_folder', 'Saving resource'),
(32, 1452271976, 1, 'anser', 3, '4', 'test_folder', 'Viewing data for resource'),
(33, 1452271980, 1, 'anser', 61, '4', 'test_folder', 'Publishing a resource'),
(34, 1452271981, 1, 'anser', 3, '4', 'test_folder', 'Viewing data for resource'),
(35, 1452271983, 1, 'anser', 51, '2', 'test_1', 'Moving resource'),
(36, 1452271985, 1, 'anser', 52, '2', '-', 'Moved resource'),
(37, 1452272408, 1, 'anser', 106, '-', '-', 'Viewing Modules'),
(38, 1452272410, 1, 'anser', 107, '-', 'Новый модуль', 'Create new module'),
(39, 1452272520, 1, 'anser', 109, '-', 'test_content', 'Saving module'),
(40, 1452272520, 1, 'anser', 108, '2', 'test_content', 'Edit module'),
(41, 1452272647, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(42, 1452274622, 1, 'anser', 76, '-', '-', 'Element management'),
(43, 1452274625, 1, 'anser', 19, '-', 'Новый шаблон', 'Creating a new template'),
(44, 1452274647, 1, 'anser', 76, '-', '-', 'Element management'),
(45, 1452274650, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(46, 1452274860, 1, 'anser', 79, '-', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(47, 1452274860, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(48, 1452274880, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(49, 1452274880, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(50, 1452274967, 1, 'anser', 106, '-', 'test_content', 'Viewing Modules'),
(51, 1452274969, 1, 'anser', 108, '2', 'test_content', 'Edit module'),
(52, 1452275064, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(53, 1452275073, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(54, 1452275083, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(55, 1452275097, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(56, 1452275114, 1, 'anser', 76, '-', '-', 'Element management'),
(57, 1452275116, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(58, 1452275145, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(59, 1452275145, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(60, 1452275147, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(61, 1452277434, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(62, 1452277434, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(63, 1452277436, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(64, 1452277463, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(65, 1452277476, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(66, 1452277669, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(67, 1452277669, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(68, 1452277676, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(69, 1452277678, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(70, 1452277685, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(71, 1452277725, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(72, 1452277725, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(73, 1452277727, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(74, 1452277728, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(75, 1452277730, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(76, 1452278352, 1, 'anser', 97, '3', 'Duplicate of tpl_main', 'Duplicate Chunk (HTML Snippet)'),
(77, 1452278352, 1, 'anser', 78, '4', 'Duplicate of tpl_main', 'Editing Chunk (HTML Snippet)'),
(78, 1452278946, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(79, 1452278948, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(80, 1452278971, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(81, 1452278973, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(82, 1452279006, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(83, 1452279006, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(84, 1452279008, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(85, 1452279009, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(86, 1452279027, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(87, 1452279028, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(88, 1452279030, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(89, 1452279031, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(90, 1452279073, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(91, 1452279074, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(92, 1452279095, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(93, 1452279096, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(94, 1452279500, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(95, 1452279529, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(96, 1452279535, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(97, 1452279542, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(98, 1452279545, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(99, 1452279609, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(100, 1452279656, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(101, 1452279656, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(102, 1452279658, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(103, 1452279677, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(104, 1452279714, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(105, 1452279714, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(106, 1452279765, 1, 'anser', 76, '-', '-', 'Element management'),
(107, 1452279767, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(108, 1452280148, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(109, 1452280148, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(110, 1452280314, 1, 'anser', 79, '-', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(111, 1452280314, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(112, 1452280348, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(113, 1452280348, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(114, 1452280443, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(115, 1452280512, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(116, 1452280512, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(117, 1452280514, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(118, 1452280525, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(119, 1452280618, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(120, 1452280625, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(121, 1452280643, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(122, 1452280651, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(123, 1452280725, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(124, 1452280726, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(125, 1452280727, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(126, 1452280825, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(127, 1452280825, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(128, 1452280833, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(129, 1452281892, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(130, 1452281892, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(131, 1452281893, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(132, 1452281917, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(133, 1452281917, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(134, 1452281919, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(135, 1452282151, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(136, 1452282177, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(137, 1452282184, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(138, 1452282228, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(139, 1452282228, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(140, 1452282245, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(141, 1452282245, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(142, 1452282275, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(143, 1452282286, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(144, 1452282362, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(145, 1452282363, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(146, 1452282397, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(147, 1452282416, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(148, 1452282517, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(149, 1452282517, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(150, 1452282787, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(151, 1452282787, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(152, 1452282791, 1, 'anser', 76, '-', '-', 'Element management'),
(153, 1452282797, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(154, 1452282803, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(155, 1452282803, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(156, 1452282819, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(157, 1452282819, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(158, 1452282825, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(159, 1452282825, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(160, 1452282887, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(161, 1452282887, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(162, 1452282894, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(163, 1452282917, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(164, 1452282924, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(165, 1452282941, 1, 'anser', 79, '3', 'tpl_main', 'Saving Chunk (HTML Snippet)'),
(166, 1452282941, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(167, 1452282946, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(168, 1452282946, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(169, 1452282965, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(170, 1452282969, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(171, 1452283028, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(172, 1452283029, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(173, 1452283054, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(174, 1452283058, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(175, 1452283060, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(176, 1452283082, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(177, 1452283132, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(178, 1452283273, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(179, 1452283331, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(180, 1452283407, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(181, 1452283407, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(182, 1452283408, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(183, 1452283429, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(184, 1452283429, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(185, 1452283433, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(186, 1452283437, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(187, 1452283458, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(188, 1452283458, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(189, 1452283463, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(190, 1452283464, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(191, 1452283465, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(192, 1452283466, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(193, 1452283468, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(194, 1452283546, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(195, 1452283546, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(196, 1452283547, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(197, 1452283549, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(198, 1452283564, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(199, 1452283565, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(200, 1452283566, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(201, 1452283602, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(202, 1452283603, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(203, 1452283604, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(204, 1452283606, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(205, 1452283607, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(206, 1452283608, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(207, 1452283611, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(208, 1452283622, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(209, 1452509070, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(210, 1452509074, 1, 'anser', 76, '-', '-', 'Element management'),
(211, 1452509078, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(212, 1452509080, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(213, 1452509137, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(214, 1452509157, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(215, 1452509297, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(216, 1452509298, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(217, 1452509300, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(218, 1452509392, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(219, 1452509417, 1, 'anser', 76, '-', '-', 'Element management'),
(220, 1452509420, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(221, 1452509524, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(222, 1452509530, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(223, 1452509548, 1, 'anser', 79, '-', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(224, 1452509548, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(225, 1452509588, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(226, 1452509626, 1, 'anser', 76, '-', '-', 'Element management'),
(227, 1452509631, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(228, 1452509636, 1, 'anser', 76, '-', '-', 'Element management'),
(229, 1452509637, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(230, 1452509640, 1, 'anser', 76, '-', '-', 'Element management'),
(231, 1452509647, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(232, 1452509653, 1, 'anser', 76, '-', '-', 'Element management'),
(233, 1452509655, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(234, 1452509667, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(235, 1452509667, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(236, 1452509668, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(237, 1452509725, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(238, 1452509830, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(239, 1452509830, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(240, 1452510034, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(241, 1452510034, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(242, 1452510166, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(243, 1452510166, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(244, 1452510530, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(245, 1452510585, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(246, 1452510585, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(247, 1452510588, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(248, 1452510887, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(249, 1452510887, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(250, 1452510902, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(251, 1452511003, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(252, 1452511021, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(253, 1452511028, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(254, 1452511056, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(255, 1452511079, 1, 'anser', 76, '-', '-', 'Element management'),
(256, 1452511080, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(257, 1452511087, 1, 'anser', 79, '5', 'add_quest_html', 'Saving Chunk (HTML Snippet)'),
(258, 1452511087, 1, 'anser', 78, '5', 'add_quest_html', 'Editing Chunk (HTML Snippet)'),
(259, 1452511089, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(260, 1452511123, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(261, 1452511197, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(262, 1452511197, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(263, 1452511198, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(264, 1452511250, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(265, 1452511250, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(266, 1452511289, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(267, 1452512451, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(268, 1452512451, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(269, 1452512884, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(270, 1452512901, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(271, 1452512919, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(272, 1452512947, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(273, 1452512947, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(274, 1452512971, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(275, 1452513002, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(276, 1452513020, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(277, 1452513064, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(278, 1452513092, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(279, 1452513092, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(280, 1452513095, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(281, 1452513107, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(282, 1452513119, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(283, 1452513121, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(284, 1452513140, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(285, 1452513362, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(286, 1452513381, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(287, 1452513385, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(288, 1452513641, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(289, 1452513641, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(290, 1452513752, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(291, 1452513752, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(292, 1452513796, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(293, 1452513802, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(294, 1452513842, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(295, 1452513847, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(296, 1452513860, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(297, 1452513863, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(298, 1452513892, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(299, 1452513895, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(300, 1452513913, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(301, 1452513953, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(302, 1452513957, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(303, 1452513961, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(304, 1452513974, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(305, 1452514064, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(306, 1452514070, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(307, 1452514114, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(308, 1452514140, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(309, 1452514154, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(310, 1452514157, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(311, 1452514161, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(312, 1452514163, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(313, 1452514703, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(314, 1452520429, 1, 'anser', 76, '-', '-', 'Element management'),
(315, 1452520439, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(316, 1452521363, 1, 'anser', 79, '-', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(317, 1452521363, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(318, 1452521386, 1, 'anser', 76, '-', '-', 'Element management'),
(319, 1452521390, 1, 'anser', 78, '3', 'tpl_main', 'Editing Chunk (HTML Snippet)'),
(320, 1452521392, 1, 'anser', 76, '-', '-', 'Element management'),
(321, 1452521402, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(322, 1452521422, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(323, 1452521422, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(324, 1452521514, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(325, 1452521618, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(326, 1452521618, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(327, 1452521619, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(328, 1452521679, 1, 'anser', 79, '4', 'quest_view', 'Saving Chunk (HTML Snippet)'),
(329, 1452521679, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(330, 1452521820, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(331, 1452522137, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(332, 1452522157, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(333, 1452522199, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(334, 1452522220, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(335, 1452522240, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(336, 1452522243, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(337, 1452522391, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(338, 1452522393, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(339, 1452522395, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(340, 1452522420, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(341, 1452522445, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(342, 1452522506, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(343, 1452522545, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(344, 1452522554, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(345, 1452522556, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(346, 1452522626, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(347, 1452522675, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(348, 1452522704, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(349, 1452522876, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(350, 1452522880, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(351, 1452522943, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(352, 1452522943, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(353, 1452523112, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(354, 1452523178, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(355, 1452523353, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(356, 1452523353, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(357, 1452523374, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(358, 1452523382, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(359, 1452523384, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(360, 1452523452, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(361, 1452523457, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(362, 1452523491, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(363, 1452523514, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(364, 1452523813, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(365, 1452523854, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(366, 1452523885, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(367, 1452523893, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(368, 1452524540, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(369, 1452524569, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(370, 1452524569, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(371, 1452524571, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(372, 1452524636, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(373, 1452524636, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(374, 1452524683, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(375, 1452524683, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(376, 1452524698, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(377, 1452524700, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(378, 1452524729, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(379, 1452524729, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(380, 1452524734, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(381, 1452524829, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(382, 1452524829, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(383, 1452524910, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(384, 1452524911, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(385, 1452524992, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(386, 1452525009, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(387, 1452525089, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(388, 1452525089, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(389, 1452525092, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(390, 1452525167, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(391, 1452525179, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(392, 1452525312, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(393, 1452525314, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(394, 1452525329, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(395, 1452525331, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(396, 1452525387, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(397, 1452525393, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(398, 1452525402, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(399, 1452525753, 1, 'anser', 31, '-', '-', 'Using file manager'),
(400, 1452525755, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(401, 1452525756, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(402, 1452525757, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(403, 1452525759, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(404, 1452525785, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(405, 1452525832, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(406, 1452525850, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(407, 1452525924, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(408, 1452525942, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(409, 1452525954, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(410, 1452526000, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(411, 1452526005, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(412, 1452526015, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(413, 1452526049, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(414, 1452526050, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(415, 1452526057, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(416, 1452526102, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(417, 1452526103, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(418, 1452526111, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(419, 1452526139, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(420, 1452526158, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(421, 1452526161, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(422, 1452526166, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(423, 1452526180, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(424, 1452526213, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(425, 1452526249, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(426, 1452526327, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(427, 1452526371, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(428, 1452526371, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(429, 1452526373, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(430, 1452526384, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(431, 1452526498, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(432, 1452526566, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(433, 1452526727, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(434, 1452526731, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(435, 1452526732, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(436, 1452526738, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(437, 1452526773, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(438, 1452526778, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(439, 1452526794, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(440, 1452526798, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(441, 1452526805, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(442, 1452526809, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(443, 1452526917, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(444, 1452526940, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(445, 1452526962, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(446, 1452526984, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(447, 1452527032, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(448, 1452527047, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(449, 1452527221, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(450, 1452527226, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(451, 1452527233, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(452, 1452527472, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(453, 1452527513, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(454, 1452527614, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(455, 1452527689, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(456, 1452527697, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(457, 1452527698, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(458, 1452527995, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(459, 1452527997, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(460, 1452528142, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(461, 1452528149, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(462, 1452528222, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(463, 1452528228, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(464, 1452528248, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(465, 1452528285, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(466, 1452528331, 1, 'anser', 79, '7', 'add_answer_line', 'Saving Chunk (HTML Snippet)'),
(467, 1452528331, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(468, 1452528332, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(469, 1452528360, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(470, 1452528378, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(471, 1452528383, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(472, 1452528395, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(473, 1452528495, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(474, 1452528709, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(475, 1452529469, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(476, 1452529470, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(477, 1452529514, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(478, 1452529515, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(479, 1452529557, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(480, 1452529591, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(481, 1452529599, 1, 'anser', 112, '1', 'test_content', 'Execute module'),
(482, 1452529609, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(483, 1452529678, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(484, 1452529787, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(485, 1452529798, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(486, 1452529818, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(487, 1452529892, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(488, 1452529904, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(489, 1452529916, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(490, 1452530016, 1, 'anser', 97, '7', 'Duplicate of add_answer_line', 'Duplicate Chunk (HTML Snippet)'),
(491, 1452530016, 1, 'anser', 78, '8', 'Duplicate of add_answer_line', 'Editing Chunk (HTML Snippet)'),
(492, 1452530053, 1, 'anser', 79, '8', 'add_answer_line_act', 'Saving Chunk (HTML Snippet)'),
(493, 1452530053, 1, 'anser', 76, '-', '-', 'Element management'),
(494, 1452530055, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(495, 1452530060, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(496, 1452530068, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(497, 1452530072, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(498, 1452530836, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(499, 1452530845, 1, 'anser', 75, '-', '-', 'User/ role management'),
(500, 1452530846, 1, 'anser', 11, '-', 'Новый пользователь', 'Creating a user'),
(501, 1452530862, 1, 'anser', 32, '-', 'den', 'Saving user'),
(502, 1452530900, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(503, 1452530916, 1, 'anser', 27, '2', 'test_1', 'Editing resource'),
(504, 1452530921, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(505, 1452530924, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(506, 1452581818, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(507, 1452581820, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(508, 1452581821, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(509, 1452581846, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(510, 1452581883, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(511, 1452581884, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(512, 1452588938, 2, 'den', 58, '-', 'MODX', 'Logged in'),
(513, 1452588945, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(514, 1452588946, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(515, 1452589828, 2, 'den', 112, '5', 'test_content', 'Execute module'),
(516, 1452589894, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(517, 1452589946, 2, 'den', 112, '5', 'test_content', 'Execute module'),
(518, 1452590010, 2, 'den', 112, '1', 'test_content', 'Execute module'),
(519, 1452590051, 2, 'den', 112, '3', 'test_content', 'Execute module'),
(520, 1452590094, 2, 'den', 112, '4', 'test_content', 'Execute module'),
(521, 1452590123, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(522, 1452590159, 2, 'den', 112, '6', 'test_content', 'Execute module'),
(523, 1452590185, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(524, 1452590208, 2, 'den', 112, '7', 'test_content', 'Execute module'),
(525, 1452590226, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(526, 1452590251, 2, 'den', 112, '8', 'test_content', 'Execute module'),
(527, 1452590291, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(528, 1452590292, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(529, 1452590317, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(530, 1452590345, 2, 'den', 112, '9', 'test_content', 'Execute module'),
(531, 1452590369, 2, 'den', 112, '9', 'test_content', 'Execute module'),
(532, 1452590392, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(533, 1452590422, 2, 'den', 112, '10', 'test_content', 'Execute module'),
(534, 1452590438, 2, 'den', 27, '2', 'test_1', 'Editing resource'),
(535, 1452590444, 2, 'den', 27, '3', 'страница ответов', 'Editing resource'),
(536, 1452590446, 2, 'den', 3, '3', 'страница ответов', 'Viewing data for resource'),
(537, 1452590539, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(538, 1452590542, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(539, 1452590543, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(540, 1452590615, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(541, 1452590621, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(542, 1452590771, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(543, 1452590798, 1, 'anser', 76, '-', '-', 'Element management'),
(544, 1452590805, 1, 'anser', 78, '7', 'add_answer_line', 'Editing Chunk (HTML Snippet)'),
(545, 1452590809, 1, 'anser', 76, '-', '-', 'Element management'),
(546, 1452590811, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(547, 1452590820, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(548, 1452590821, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(549, 1452590822, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(550, 1452591375, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(551, 1452591635, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(552, 1452591635, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(553, 1452591666, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(554, 1452591666, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(555, 1452591737, 1, 'anser', 79, '6', 'test_quest_line', 'Saving Chunk (HTML Snippet)'),
(556, 1452591737, 1, 'anser', 78, '6', 'test_quest_line', 'Editing Chunk (HTML Snippet)'),
(557, 1452591751, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(558, 1452591766, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(559, 1452591802, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(560, 1452591807, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(561, 1452591863, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(562, 1452591873, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(563, 1452591913, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(564, 1452591915, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(565, 1452593134, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(566, 1452593138, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(567, 1452593140, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(568, 1452599151, 2, 'den', 31, '-', '-', 'Using file manager'),
(569, 1452599152, 2, 'den', 31, '-', '-', 'Using file manager'),
(570, 1452599155, 2, 'den', 31, '-', '-', 'Using file manager'),
(571, 1452599161, 2, 'den', 31, '-', '-', 'Using file manager'),
(572, 1452599162, 2, 'den', 31, '-', '-', 'Using file manager'),
(573, 1452599166, 2, 'den', 31, '-', '-', 'Using file manager'),
(574, 1452599168, 2, 'den', 31, '-', '-', 'Using file manager'),
(575, 1452599170, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img1.png', 'Uploaded File'),
(576, 1452599172, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img2.jpg', 'Uploaded File'),
(577, 1452599174, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img3.jpg', 'Uploaded File'),
(578, 1452599176, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img4.jpg', 'Uploaded File'),
(579, 1452599177, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img5.jpg', 'Uploaded File'),
(580, 1452599179, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img6.jpg', 'Uploaded File'),
(581, 1452599181, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img7.jpg', 'Uploaded File'),
(582, 1452599182, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img8.jpg', 'Uploaded File'),
(583, 1452599184, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img9.jpg', 'Uploaded File'),
(584, 1452599186, 2, 'den', 31, '-', '/var/www/test_beauty/www/img/test1/img10.jpg', 'Uploaded File'),
(585, 1452599190, 2, 'den', 31, '-', '-', 'Using file manager'),
(586, 1452599193, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(587, 1452599194, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(588, 1452599221, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(589, 1452599327, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(590, 1452599336, 2, 'den', 112, '5', 'test_content', 'Execute module'),
(591, 1452599342, 2, 'den', 112, '1', 'test_content', 'Execute module'),
(592, 1452599350, 2, 'den', 112, '3', 'test_content', 'Execute module'),
(593, 1452599357, 2, 'den', 112, '4', 'test_content', 'Execute module'),
(594, 1452599362, 2, 'den', 112, '6', 'test_content', 'Execute module'),
(595, 1452599368, 2, 'den', 112, '7', 'test_content', 'Execute module'),
(596, 1452599375, 2, 'den', 112, '8', 'test_content', 'Execute module'),
(597, 1452599380, 2, 'den', 112, '9', 'test_content', 'Execute module'),
(598, 1452599387, 2, 'den', 112, '10', 'test_content', 'Execute module'),
(599, 1452599393, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(600, 1452599394, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(601, 1452599446, 2, 'den', 27, '2', 'test_1', 'Editing resource'),
(602, 1452601954, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(603, 1452601968, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(604, 1452601977, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(605, 1452601981, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(606, 1452601982, 1, 'anser', 112, '2', 'test_content', 'Execute module'),
(607, 1452602834, 2, 'den', 27, '2', 'test_1', 'Editing resource'),
(608, 1452604523, 1, 'anser', 76, '-', '-', 'Element management'),
(609, 1452604526, 1, 'anser', 19, '-', 'Новый шаблон', 'Creating a new template'),
(610, 1452604528, 1, 'anser', 76, '-', '-', 'Element management'),
(611, 1452604530, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(612, 1452604553, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(613, 1452604553, 1, 'anser', 76, '-', '-', 'Element management'),
(614, 1452604562, 1, 'anser', 17, '-', '-', 'Editing settings'),
(615, 1452604592, 1, 'anser', 30, '-', '-', 'Saving settings'),
(616, 1452604610, 1, 'anser', 31, '-', '-', 'Using file manager'),
(617, 1452604612, 1, 'anser', 31, '-', '-', 'Using file manager'),
(618, 1452604614, 1, 'anser', 31, '-', '-', 'Using file manager'),
(619, 1452604618, 1, 'anser', 31, '-', '-', 'Using file manager'),
(620, 1452604620, 1, 'anser', 31, '-', '-', 'Using file manager'),
(621, 1452604621, 1, 'anser', 31, '-', '-', 'Using file manager'),
(622, 1452604626, 1, 'anser', 31, '-', '/var/www/test_beauty/www/css/styles.css', 'Uploaded File'),
(623, 1452604628, 1, 'anser', 31, '-', '-', 'Using file manager'),
(624, 1452604630, 1, 'anser', 31, '-', '-', 'Using file manager'),
(625, 1452604638, 1, 'anser', 31, '-', '/var/www/test_beauty/www/js/all.js', 'Uploaded File'),
(626, 1452604641, 1, 'anser', 31, '-', '-', 'Using file manager'),
(627, 1452604656, 1, 'anser', 31, '-', '-', 'Using file manager'),
(628, 1452604658, 1, 'anser', 31, '-', '-', 'Using file manager'),
(629, 1452604665, 1, 'anser', 31, '-', '-', 'Using file manager'),
(630, 1452604670, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProBlack.ttf', 'Uploaded File'),
(631, 1452604674, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProBlack.woff', 'Uploaded File'),
(632, 1452604678, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProBold.ttf', 'Uploaded File'),
(633, 1452604683, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProBold.woff', 'Uploaded File'),
(634, 1452604687, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProRegular.ttf', 'Uploaded File'),
(635, 1452604692, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProRegular.woff', 'Uploaded File'),
(636, 1452604695, 1, 'anser', 31, '-', '-', 'Using file manager'),
(637, 1452604698, 1, 'anser', 17, '-', '-', 'Editing settings'),
(638, 1452604719, 1, 'anser', 30, '-', '-', 'Saving settings'),
(639, 1452604721, 1, 'anser', 31, '-', '-', 'Using file manager'),
(640, 1452604723, 1, 'anser', 31, '-', '-', 'Using file manager'),
(641, 1452604726, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProBlack.eot', 'Uploaded File'),
(642, 1452604736, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProBold.eot', 'Uploaded File'),
(643, 1452604743, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothamProRegular.eot', 'Uploaded File'),
(644, 1452604746, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothaProBla.otf', 'Uploaded File'),
(645, 1452604749, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothaProBol.otf', 'Uploaded File'),
(646, 1452604753, 1, 'anser', 31, '-', '/var/www/test_beauty/www/fonts/GothaProReg.otf', 'Uploaded File'),
(647, 1452604755, 1, 'anser', 31, '-', '-', 'Using file manager'),
(648, 1452604759, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(649, 1452604764, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(650, 1452604765, 1, 'anser', 5, '4', 'test_folder', 'Saving resource'),
(651, 1452604767, 1, 'anser', 3, '4', 'test_folder', 'Viewing data for resource');
INSERT INTO `modx_manager_log` (`id`, `timestamp`, `internalKey`, `username`, `action`, `itemid`, `itemname`, `message`) VALUES
(652, 1452604987, 1, 'anser', 31, '-', '-', 'Using file manager'),
(653, 1452604988, 1, 'anser', 31, '-', '-', 'Using file manager'),
(654, 1452605004, 1, 'anser', 31, '-', '-', 'Using file manager'),
(655, 1452605008, 1, 'anser', 31, '-', '-', 'Using file manager'),
(656, 1452605010, 1, 'anser', 31, '-', '-', 'Using file manager'),
(657, 1452605017, 1, 'anser', 31, '-', '/var/www/test_beauty/www/images/sprite.png', 'Uploaded File'),
(658, 1452605025, 1, 'anser', 31, '-', '/var/www/test_beauty/www/images/bg.jpg', 'Uploaded File'),
(659, 1452605028, 1, 'anser', 31, '-', '/var/www/test_beauty/www/images/img-1.png', 'Uploaded File'),
(660, 1452605079, 1, 'anser', 76, '-', '-', 'Element management'),
(661, 1452605081, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(662, 1452605094, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(663, 1452605094, 1, 'anser', 76, '-', '-', 'Element management'),
(664, 1452605105, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(665, 1452605113, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(666, 1452605113, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(667, 1452605137, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(668, 1452605137, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(669, 1452605207, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(670, 1452605207, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(671, 1452605221, 1, 'anser', 31, '-', '-', 'Using file manager'),
(672, 1452605223, 1, 'anser', 31, '-', '-', 'Using file manager'),
(673, 1452605285, 1, 'anser', 76, '-', '-', 'Element management'),
(674, 1452605286, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(675, 1452605338, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(676, 1452605338, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(677, 1452606265, 1, 'anser', 76, '-', '-', 'Element management'),
(678, 1452606267, 1, 'anser', 23, '-', 'Новый сниппет', 'Creating a new Snippet'),
(679, 1452606279, 1, 'anser', 106, '-', '-', 'Viewing Modules'),
(680, 1452606280, 1, 'anser', 108, '2', 'test_content', 'Edit module'),
(681, 1452606295, 1, 'anser', 24, '-', 'load_quest', 'Saving Snippet'),
(682, 1452606295, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(683, 1452606307, 1, 'anser', 24, '17', 'load_quest', 'Saving Snippet'),
(684, 1452606307, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(685, 1452606324, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(686, 1452606324, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(687, 1452606327, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(688, 1452606330, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(689, 1452606332, 1, 'anser', 5, '4', 'test_folder', 'Saving resource'),
(690, 1452606333, 1, 'anser', 3, '4', 'test_folder', 'Viewing data for resource'),
(691, 1452606336, 1, 'anser', 3, '4', 'test_folder', 'Viewing data for resource'),
(692, 1452606339, 1, 'anser', 3, '4', 'test_folder', 'Viewing data for resource'),
(693, 1452606356, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(694, 1452606360, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(695, 1452606364, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(696, 1452606367, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(697, 1452606369, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(698, 1452606371, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(699, 1452606372, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(700, 1452606374, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(701, 1452606399, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(702, 1452606400, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(703, 1452606407, 1, 'anser', 13, '-', '-', 'Viewing logging'),
(704, 1452606409, 1, 'anser', 53, '-', '-', 'Viewing system info'),
(705, 1452606450, 1, 'anser', 53, '-', '-', 'Viewing system info'),
(706, 1452606455, 1, 'anser', 8, '-', '-', 'Logged out'),
(707, 1452606479, 2, 'den', 58, '-', 'MODX', 'Logged in'),
(708, 1452606484, 2, 'den', 8, '-', '-', 'Logged out'),
(709, 1452606485, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(710, 1452606488, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(711, 1452606494, 1, 'anser', 27, '2', 'test_1', 'Editing resource'),
(712, 1452606498, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(713, 1452606499, 1, 'anser', 27, '2', 'test_1', 'Editing resource'),
(714, 1452606506, 1, 'anser', 76, '-', '-', 'Element management'),
(715, 1452606508, 1, 'anser', 17, '-', '-', 'Editing settings'),
(716, 1452606518, 1, 'anser', 30, '-', '-', 'Saving settings'),
(717, 1452606525, 1, 'anser', 27, '2', 'test_1', 'Editing resource'),
(718, 1452606527, 1, 'anser', 27, '2', 'test_1', 'Editing resource'),
(719, 1452606535, 1, 'anser', 5, '2', 'test_1', 'Saving resource'),
(720, 1452606536, 1, 'anser', 3, '2', 'test_1', 'Viewing data for resource'),
(721, 1452606537, 1, 'anser', 27, '4', 'test_folder', 'Editing resource'),
(722, 1452606540, 1, 'anser', 27, '2', 'test_1', 'Editing resource'),
(723, 1452606861, 1, 'anser', 8, '-', '-', 'Logged out'),
(724, 1452607376, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(725, 1452607379, 1, 'anser', 76, '-', '-', 'Element management'),
(726, 1452607382, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(727, 1452607388, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(728, 1452607391, 1, 'anser', 76, '-', '-', 'Element management'),
(729, 1452607397, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(730, 1452607449, 2, 'den', 27, '4', 'test_folder', 'Editing resource'),
(731, 1452607470, 2, 'den', 3, '4', 'test_folder', 'Viewing data for resource'),
(732, 1452607594, 1, 'anser', 79, '-', 'quest_tab', 'Saving Chunk (HTML Snippet)'),
(733, 1452607594, 1, 'anser', 78, '9', 'quest_tab', 'Editing Chunk (HTML Snippet)'),
(734, 1452607697, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(735, 1452607699, 1, 'anser', 76, '-', '-', 'Element management'),
(736, 1452607701, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(737, 1452607710, 1, 'anser', 24, '17', 'load_quest', 'Saving Snippet'),
(738, 1452607710, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(739, 1452607773, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(740, 1452607773, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(741, 1452607837, 1, 'anser', 79, '9', 'quest_tab', 'Saving Chunk (HTML Snippet)'),
(742, 1452607837, 1, 'anser', 78, '9', 'quest_tab', 'Editing Chunk (HTML Snippet)'),
(743, 1452607887, 1, 'anser', 24, '17', 'load_quest', 'Saving Snippet'),
(744, 1452607887, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(745, 1452608147, 1, 'anser', 76, '-', '-', 'Element management'),
(746, 1452608149, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(747, 1452608223, 1, 'anser', 79, '-', 'answer_list', 'Saving Chunk (HTML Snippet)'),
(748, 1452608223, 1, 'anser', 78, '10', 'answer_list', 'Editing Chunk (HTML Snippet)'),
(749, 1452608535, 1, 'anser', 79, '9', 'quest_tab', 'Saving Chunk (HTML Snippet)'),
(750, 1452608535, 1, 'anser', 78, '9', 'quest_tab', 'Editing Chunk (HTML Snippet)'),
(751, 1452611023, 1, 'anser', 76, '-', '-', 'Element management'),
(752, 1452611025, 1, 'anser', 19, '-', 'Новый шаблон', 'Creating a new template'),
(753, 1452611059, 1, 'anser', 20, '-', 'test_form_work', 'Saving template'),
(754, 1452611059, 1, 'anser', 16, '7', 'test_form_work', 'Editing template'),
(755, 1452611063, 1, 'anser', 76, '-', '-', 'Element management'),
(756, 1452611066, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(757, 1452611073, 1, 'anser', 76, '-', '-', 'Element management'),
(758, 1452611076, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(759, 1452611081, 1, 'anser', 76, '-', '-', 'Element management'),
(760, 1452611085, 1, 'anser', 23, '-', 'Новый сниппет', 'Creating a new Snippet'),
(761, 1452611088, 1, 'anser', 76, '-', '-', 'Element management'),
(762, 1452611090, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(763, 1452611106, 1, 'anser', 24, '-', 'chek_form', 'Saving Snippet'),
(764, 1452611106, 1, 'anser', 22, '18', 'chek_form', 'Editing Snippet'),
(765, 1452611143, 1, 'anser', 24, '18', 'chek_form', 'Saving Snippet'),
(766, 1452611143, 1, 'anser', 22, '18', 'chek_form', 'Editing Snippet'),
(767, 1452611152, 1, 'anser', 76, '-', '-', 'Element management'),
(768, 1452611155, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(769, 1452611189, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(770, 1452611232, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(771, 1452611239, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(772, 1452611249, 1, 'anser', 4, '-', 'Новый ресурс', 'Creating a resource'),
(773, 1452611255, 1, 'anser', 5, '-', 'chek_form', 'Saving resource'),
(774, 1452611256, 1, 'anser', 3, '5', 'chek_form', 'Viewing data for resource'),
(775, 1452611260, 1, 'anser', 61, '5', 'chek_form', 'Publishing a resource'),
(776, 1452611261, 1, 'anser', 3, '2', 'test_1', 'Viewing data for resource'),
(777, 1452611262, 1, 'anser', 76, '-', '-', 'Element management'),
(778, 1452611267, 1, 'anser', 22, '18', 'chek_form', 'Editing Snippet'),
(779, 1452611270, 1, 'anser', 24, '18', 'chek_form', 'Saving Snippet'),
(780, 1452611270, 1, 'anser', 76, '-', '-', 'Element management'),
(781, 1452611277, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(782, 1452611280, 1, 'anser', 76, '-', '-', 'Element management'),
(783, 1452611281, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(784, 1452611313, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(785, 1452611313, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(786, 1452611339, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(787, 1452611339, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(788, 1452611388, 1, 'anser', 76, '-', '-', 'Element management'),
(789, 1452611391, 1, 'anser', 78, '4', 'quest_view', 'Editing Chunk (HTML Snippet)'),
(790, 1452611395, 1, 'anser', 76, '-', '-', 'Element management'),
(791, 1452611398, 1, 'anser', 78, '9', 'quest_tab', 'Editing Chunk (HTML Snippet)'),
(792, 1452611410, 1, 'anser', 79, '9', 'quest_tab', 'Saving Chunk (HTML Snippet)'),
(793, 1452611410, 1, 'anser', 78, '9', 'quest_tab', 'Editing Chunk (HTML Snippet)'),
(794, 1452611458, 1, 'anser', 79, '9', 'quest_tab', 'Saving Chunk (HTML Snippet)'),
(795, 1452611458, 1, 'anser', 78, '9', 'quest_tab', 'Editing Chunk (HTML Snippet)'),
(796, 1452611466, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(797, 1452611466, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(798, 1452611471, 1, 'anser', 76, '-', '-', 'Element management'),
(799, 1452611474, 1, 'anser', 22, '17', 'load_quest', 'Editing Snippet'),
(800, 1452611476, 1, 'anser', 98, '17', 'Duplicate of load_quest', 'Duplicate Snippet'),
(801, 1452611476, 1, 'anser', 22, '19', 'Duplicate of load_quest', 'Editing Snippet'),
(802, 1452611579, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(803, 1452611579, 1, 'anser', 76, '-', '-', 'Element management'),
(804, 1452611640, 1, 'anser', 24, '19', 'find_worker', 'Saving Snippet'),
(805, 1452611640, 1, 'anser', 22, '19', 'find_worker', 'Editing Snippet'),
(806, 1452611671, 1, 'anser', 24, '19', 'find_worker', 'Saving Snippet'),
(807, 1452611671, 1, 'anser', 22, '19', 'find_worker', 'Editing Snippet'),
(808, 1452611699, 1, 'anser', 24, '19', 'find_worker', 'Saving Snippet'),
(809, 1452611699, 1, 'anser', 22, '19', 'find_worker', 'Editing Snippet'),
(810, 1452611731, 1, 'anser', 24, '19', 'find_worker', 'Saving Snippet'),
(811, 1452611731, 1, 'anser', 22, '19', 'find_worker', 'Editing Snippet'),
(812, 1452611736, 1, 'anser', 24, '19', 'find_worker', 'Saving Snippet'),
(813, 1452611736, 1, 'anser', 22, '19', 'find_worker', 'Editing Snippet'),
(814, 1452611837, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(815, 1452611846, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(816, 1452611847, 1, 'anser', 76, '-', '-', 'Element management'),
(817, 1452611887, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(818, 1452611888, 1, 'anser', 76, '-', '-', 'Element management'),
(819, 1452611890, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(820, 1452613015, 1, 'anser', 53, '-', '-', 'Viewing system info'),
(821, 1452613016, 1, 'anser', 200, '-', '-', 'Viewing phpInfo()'),
(822, 1452614577, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(823, 1452614577, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(824, 1452614579, 1, 'anser', 76, '-', '-', 'Element management'),
(825, 1452614584, 1, 'anser', 22, '19', 'find_worker', 'Editing Snippet'),
(826, 1452614588, 1, 'anser', 98, '19', 'Duplicate of find_worker', 'Duplicate Snippet'),
(827, 1452614588, 1, 'anser', 22, '20', 'Duplicate of find_worker', 'Editing Snippet'),
(828, 1452614606, 1, 'anser', 76, '-', '-', 'Element management'),
(829, 1452614617, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(830, 1452614626, 1, 'anser', 24, '20', 'find_result_page', 'Saving Snippet'),
(831, 1452614626, 1, 'anser', 76, '-', '-', 'Element management'),
(832, 1452618927, 1, 'anser', 17, '-', '-', 'Editing settings'),
(833, 1452618932, 1, 'anser', 30, '-', '-', 'Saving settings'),
(834, 1452618934, 1, 'anser', 31, '-', '-', 'Using file manager'),
(835, 1452618951, 1, 'anser', 31, '-', '-', 'Using file manager'),
(836, 1452618953, 1, 'anser', 31, '-', '-', 'Using file manager'),
(837, 1452618959, 1, 'anser', 31, '-', '-', 'Using file manager'),
(838, 1452618961, 1, 'anser', 31, '-', '-', 'Using file manager'),
(839, 1452618964, 1, 'anser', 31, '-', '-', 'Using file manager'),
(840, 1452618980, 1, 'anser', 31, '-', '/var/www/test_beauty/www/mail/inc/store-address.php', 'Viewing File'),
(841, 1452618985, 1, 'anser', 31, '-', '/var/www/test_beauty/www/mail/inc/store-address.php', 'Modified File'),
(842, 1452618985, 1, 'anser', 31, '-', '/var/www/test_beauty/www/mail/inc/store-address.php', 'Viewing File'),
(843, 1452618998, 1, 'anser', 31, '-', '-', 'Using file manager'),
(844, 1452619001, 1, 'anser', 31, '-', '/var/www/test_beauty/www/mail/inc/MCAPI.class.php', 'Viewing File'),
(845, 1452619020, 1, 'anser', 31, '-', '/var/www/test_beauty/www/mail/inc/MCAPI.class.php', 'Modified File'),
(846, 1452619020, 1, 'anser', 31, '-', '/var/www/test_beauty/www/mail/inc/MCAPI.class.php', 'Viewing File'),
(847, 1452690460, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(848, 1452691075, 1, 'anser', 76, '-', '-', 'Element management'),
(849, 1452691077, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(850, 1452691116, 1, 'anser', 76, '-', '-', 'Element management'),
(851, 1452691119, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(852, 1452691155, 1, 'anser', 20, '6', 'test_1_answer', 'Saving template'),
(853, 1452691155, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(854, 1452691246, 1, 'anser', 20, '6', 'test_1_answer', 'Saving template'),
(855, 1452691246, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(856, 1452691248, 1, 'anser', 76, '-', '-', 'Element management'),
(857, 1452691250, 1, 'anser', 23, '-', 'Новый сниппет', 'Creating a new Snippet'),
(858, 1452691253, 1, 'anser', 76, '-', '-', 'Element management'),
(859, 1452691255, 1, 'anser', 22, '18', 'chek_form', 'Editing Snippet'),
(860, 1452691258, 1, 'anser', 98, '18', 'Duplicate of chek_form', 'Duplicate Snippet'),
(861, 1452691258, 1, 'anser', 22, '21', 'Duplicate of chek_form', 'Editing Snippet'),
(862, 1452691276, 1, 'anser', 24, '21', 'load_user_answer', 'Saving Snippet'),
(863, 1452691276, 1, 'anser', 22, '21', 'load_user_answer', 'Editing Snippet'),
(864, 1452692810, 1, 'anser', 76, '-', '-', 'Element management'),
(865, 1452692812, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(866, 1452692895, 1, 'anser', 79, '-', 'quest_full', 'Saving Chunk (HTML Snippet)'),
(867, 1452692895, 1, 'anser', 78, '11', 'quest_full', 'Editing Chunk (HTML Snippet)'),
(868, 1452692991, 1, 'anser', 20, '6', 'test_1_answer', 'Saving template'),
(869, 1452692991, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(870, 1452693045, 1, 'anser', 79, '11', 'quest_full', 'Saving Chunk (HTML Snippet)'),
(871, 1452693046, 1, 'anser', 78, '11', 'quest_full', 'Editing Chunk (HTML Snippet)'),
(872, 1452693157, 1, 'anser', 79, '11', 'quest_full', 'Saving Chunk (HTML Snippet)'),
(873, 1452693157, 1, 'anser', 78, '11', 'quest_full', 'Editing Chunk (HTML Snippet)'),
(874, 1452693447, 1, 'anser', 79, '11', 'quest_full', 'Saving Chunk (HTML Snippet)'),
(875, 1452693447, 1, 'anser', 78, '11', 'quest_full', 'Editing Chunk (HTML Snippet)'),
(876, 1452693488, 1, 'anser', 79, '11', 'quest_full', 'Saving Chunk (HTML Snippet)'),
(877, 1452693488, 1, 'anser', 78, '11', 'quest_full', 'Editing Chunk (HTML Snippet)'),
(878, 1452694337, 1, 'anser', 76, '-', '-', 'Element management'),
(879, 1452694341, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(880, 1452694434, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(881, 1452694438, 1, 'anser', 77, '-', 'Новый чанк', 'Creating a new Chunk (HTML Snippet)'),
(882, 1452694488, 1, 'anser', 79, '-', 'answer_list_user', 'Saving Chunk (HTML Snippet)'),
(883, 1452694488, 1, 'anser', 78, '12', 'answer_list_user', 'Editing Chunk (HTML Snippet)'),
(884, 1452694497, 1, 'anser', 79, '11', 'quest_full', 'Saving Chunk (HTML Snippet)'),
(885, 1452694497, 1, 'anser', 78, '11', 'quest_full', 'Editing Chunk (HTML Snippet)'),
(886, 1452697111, 1, 'anser', 79, '12', 'answer_list_user', 'Saving Chunk (HTML Snippet)'),
(887, 1452697112, 1, 'anser', 78, '12', 'answer_list_user', 'Editing Chunk (HTML Snippet)'),
(888, 1452697423, 1, 'anser', 79, '12', 'answer_list_user', 'Saving Chunk (HTML Snippet)'),
(889, 1452697423, 1, 'anser', 78, '12', 'answer_list_user', 'Editing Chunk (HTML Snippet)'),
(890, 1452697507, 1, 'anser', 79, '12', 'answer_list_user', 'Saving Chunk (HTML Snippet)'),
(891, 1452697507, 1, 'anser', 78, '12', 'answer_list_user', 'Editing Chunk (HTML Snippet)'),
(892, 1452697725, 1, 'anser', 79, '12', 'answer_list_user', 'Saving Chunk (HTML Snippet)'),
(893, 1452697725, 1, 'anser', 78, '12', 'answer_list_user', 'Editing Chunk (HTML Snippet)'),
(894, 1452697741, 1, 'anser', 79, '12', 'answer_list_user', 'Saving Chunk (HTML Snippet)'),
(895, 1452697741, 1, 'anser', 78, '12', 'answer_list_user', 'Editing Chunk (HTML Snippet)'),
(896, 1452698813, 1, 'anser', 31, '-', '-', 'Using file manager'),
(897, 1452698854, 1, 'anser', 99, '-', '-', 'Manage Web Users'),
(898, 1452698856, 1, 'anser', 75, '-', '-', 'User/ role management'),
(899, 1452698859, 1, 'anser', 11, '-', 'Новый пользователь', 'Creating a user'),
(900, 1452698871, 1, 'anser', 32, '-', 'manager', 'Saving user'),
(901, 1452699648, 1, 'anser', 75, '-', '-', 'User/ role management'),
(902, 1452700001, 1, 'anser', 76, '-', '-', 'Element management'),
(903, 1452700009, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(904, 1452700022, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(905, 1452700022, 1, 'anser', 76, '-', '-', 'Element management'),
(906, 1452700023, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(907, 1452700032, 1, 'anser', 20, '6', 'test_1_answer', 'Saving template'),
(908, 1452700032, 1, 'anser', 76, '-', '-', 'Element management'),
(909, 1452858886, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(910, 1452858891, 1, 'anser', 76, '-', '-', 'Element management'),
(911, 1452858908, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(912, 1452858918, 1, 'anser', 20, '6', 'test_1_answer', 'Saving template'),
(913, 1452858918, 1, 'anser', 76, '-', '-', 'Element management'),
(914, 1452858920, 1, 'anser', 16, '7', 'test_form_work', 'Editing template'),
(915, 1452858921, 1, 'anser', 76, '-', '-', 'Element management'),
(916, 1452858923, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(917, 1452858925, 1, 'anser', 76, '-', '-', 'Element management'),
(918, 1452858927, 1, 'anser', 16, '5', 'test_1_form', 'Editing template'),
(919, 1452858932, 1, 'anser', 20, '5', 'test_1_form', 'Saving template'),
(920, 1452858932, 1, 'anser', 76, '-', '-', 'Element management'),
(921, 1452861090, 1, 'anser', 13, '-', '-', 'Viewing logging'),
(922, 1452864825, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(923, 1452864831, 1, 'anser', 13, '-', '-', 'Viewing logging'),
(924, 1452864840, 1, 'anser', 75, '-', '-', 'User/ role management'),
(925, 1452872038, 1, 'anser', 99, '-', '-', 'Manage Web Users'),
(926, 1452872040, 1, 'anser', 99, '-', '-', 'Manage Web Users'),
(927, 1452872046, 1, 'anser', 99, '-', '-', 'Manage Web Users'),
(928, 1452872047, 1, 'anser', 75, '-', '-', 'User/ role management'),
(929, 1452884986, 2, 'den', 58, '-', 'MODX', 'Logged in'),
(930, 1452884991, 2, 'den', 27, '2', 'test_1', 'Editing resource'),
(931, 1452884996, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(932, 1452885013, 2, 'den', 94, '5', 'Duplicate of chek_form', 'Duplicate resource'),
(933, 1452885013, 2, 'den', 3, '6', 'Duplicate of chek_form', 'Viewing data for resource'),
(934, 1452885016, 2, 'den', 27, '6', 'Duplicate of chek_form', 'Editing resource'),
(935, 1452885034, 2, 'den', 5, '6', 'result_form', 'Saving resource'),
(936, 1452885036, 2, 'den', 3, '6', 'result_form', 'Viewing data for resource'),
(937, 1452885042, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(938, 1452885043, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(939, 1452885048, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(940, 1452885051, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(941, 1452885054, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(942, 1452885057, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(943, 1452885060, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(944, 1452885067, 2, 'den', 76, '-', '-', 'Element management'),
(945, 1452885075, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(946, 1452885085, 2, 'den', 76, '-', '-', 'Element management'),
(947, 1452885086, 2, 'den', 16, '7', 'test_form_work', 'Editing template'),
(948, 1452885097, 2, 'den', 96, '7', 'Duplicate of test_form_work', 'Duplicate Template'),
(949, 1452885097, 2, 'den', 16, '8', 'Duplicate of test_form_work', 'Editing template'),
(950, 1452885112, 2, 'den', 20, '8', 'test_form_result', 'Saving template'),
(951, 1452885112, 2, 'den', 76, '-', '-', 'Element management'),
(952, 1452885114, 2, 'den', 16, '8', 'test_form_result', 'Editing template'),
(953, 1452885125, 2, 'den', 20, '8', 'test_form_result', 'Saving template'),
(954, 1452885125, 2, 'den', 76, '-', '-', 'Element management'),
(955, 1452885127, 2, 'den', 16, '8', 'test_form_result', 'Editing template'),
(956, 1452885132, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(957, 1452885135, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(958, 1452885139, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(959, 1452885141, 2, 'den', 5, '6', 'result_form', 'Saving resource'),
(960, 1452885142, 2, 'den', 3, '6', 'result_form', 'Viewing data for resource'),
(961, 1452885144, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(962, 1452885148, 2, 'den', 76, '-', '-', 'Element management'),
(963, 1452885160, 2, 'den', 22, '18', 'chek_form', 'Editing Snippet'),
(964, 1452885170, 2, 'den', 98, '18', 'Duplicate of chek_form', 'Duplicate Snippet'),
(965, 1452885170, 2, 'den', 22, '22', 'Duplicate of chek_form', 'Editing Snippet'),
(966, 1452885205, 2, 'den', 24, '22', 'result_form', 'Saving Snippet'),
(967, 1452885205, 2, 'den', 76, '-', '-', 'Element management'),
(968, 1452885375, 2, 'den', 22, '20', 'find_result_page', 'Editing Snippet'),
(969, 1452885399, 2, 'den', 27, '2', 'test_1', 'Editing resource'),
(970, 1452885404, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(971, 1452885409, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(972, 1452885426, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(973, 1452885437, 2, 'den', 27, '1', 'главная', 'Editing resource'),
(974, 1452885524, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(975, 1452885526, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(976, 1452885544, 2, 'den', 76, '-', '-', 'Element management'),
(977, 1452885562, 2, 'den', 22, '22', 'result_form', 'Editing Snippet'),
(978, 1452885573, 2, 'den', 24, '22', 'result_form', 'Saving Snippet'),
(979, 1452885573, 2, 'den', 76, '-', '-', 'Element management'),
(980, 1452885579, 2, 'den', 22, '22', 'result_form', 'Editing Snippet'),
(981, 1452885606, 2, 'den', 24, '22', 'result_form', 'Saving Snippet'),
(982, 1452885606, 2, 'den', 76, '-', '-', 'Element management'),
(983, 1452885676, 2, 'den', 22, '22', 'result_form', 'Editing Snippet'),
(984, 1452944484, 2, 'den', 58, '-', 'MODX', 'Logged in'),
(985, 1452944493, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(986, 1452944501, 2, 'den', 76, '-', '-', 'Element management'),
(987, 1452944506, 2, 'den', 16, '8', 'test_form_result', 'Editing template'),
(988, 1452944515, 2, 'den', 20, '8', 'test_form_result', 'Saving template'),
(989, 1452944515, 2, 'den', 76, '-', '-', 'Element management'),
(990, 1452944538, 2, 'den', 16, '8', 'test_form_result', 'Editing template'),
(991, 1452944550, 2, 'den', 76, '-', '-', 'Element management'),
(992, 1452944555, 2, 'den', 16, '5', 'test_1_form', 'Editing template'),
(993, 1452944621, 2, 'den', 76, '-', '-', 'Element management'),
(994, 1452944627, 2, 'den', 16, '8', 'test_form_result', 'Editing template'),
(995, 1452944669, 2, 'den', 20, '8', 'test_form_result', 'Saving template'),
(996, 1452944669, 2, 'den', 76, '-', '-', 'Element management'),
(997, 1452947486, 2, 'den', 106, '-', '-', 'Viewing Modules'),
(998, 1452947487, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(999, 1452947488, 2, 'den', 112, '2', 'test_content', 'Execute module'),
(1000, 1452947499, 2, 'den', 93, '-', '-', 'Backup Manager'),
(1001, 1452947513, 2, 'den', 95, '-', '-', 'Importing resources from HTML'),
(1002, 1452947514, 2, 'den', 83, '-', '-', 'Exporting a resource to HTML'),
(1003, 1452950138, 2, 'den', 17, '-', '-', 'Editing settings'),
(1004, 1452951422, 2, 'den', 76, '-', '-', 'Element management'),
(1005, 1452951425, 2, 'den', 16, '8', 'test_form_result', 'Editing template'),
(1006, 1452951453, 2, 'den', 20, '8', 'test_form_result', 'Saving template'),
(1007, 1452951453, 2, 'den', 76, '-', '-', 'Element management'),
(1008, 1452951562, 2, 'den', 16, '8', 'test_form_result', 'Editing template'),
(1009, 1452951589, 2, 'den', 20, '8', 'test_form_result', 'Saving template'),
(1010, 1452951589, 2, 'den', 76, '-', '-', 'Element management'),
(1011, 1452952941, 2, 'den', 8, '-', '-', 'Logged out'),
(1012, 1452953013, 2, 'den', 58, '-', 'MODX', 'Logged in'),
(1013, 1452953015, 2, 'den', 27, '4', 'test_folder', 'Editing resource'),
(1014, 1452953019, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(1015, 1452953044, 2, 'den', 27, '5', 'chek_form', 'Editing resource'),
(1016, 1452953046, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(1017, 1452953049, 2, 'den', 5, '6', 'result_form', 'Saving resource'),
(1018, 1452953050, 2, 'den', 3, '6', 'result_form', 'Viewing data for resource'),
(1019, 1452953050, 2, 'den', 27, '6', 'result_form', 'Editing resource'),
(1020, 1452953053, 2, 'den', 8, '-', '-', 'Logged out'),
(1021, 1453104417, 1, 'anser', 58, '-', 'MODX', 'Logged in'),
(1022, 1453104419, 1, 'anser', 76, '-', '-', 'Element management'),
(1023, 1453104421, 1, 'anser', 16, '8', 'test_form_result', 'Editing template'),
(1024, 1453104436, 1, 'anser', 76, '-', '-', 'Element management'),
(1025, 1453104442, 1, 'anser', 27, '6', 'result_form', 'Editing resource'),
(1026, 1453104445, 1, 'anser', 76, '-', '-', 'Element management'),
(1027, 1453104454, 1, 'anser', 27, '3', 'страница ответов', 'Editing resource'),
(1028, 1453104460, 1, 'anser', 76, '-', '-', 'Element management'),
(1029, 1453104461, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(1030, 1453104548, 1, 'anser', 20, '6', 'test_1_answer', 'Saving template'),
(1031, 1453104548, 1, 'anser', 16, '6', 'test_1_answer', 'Editing template'),
(1032, 1453104794, 1, 'anser', 31, '-', '-', 'Using file manager');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_manager_users`
--

CREATE TABLE IF NOT EXISTS `modx_manager_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains login information for backend users.' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `modx_manager_users`
--

INSERT INTO `modx_manager_users` (`id`, `username`, `password`) VALUES
(1, 'anser', 'uncrypt>ac5726923cc0f6088855fbf30f601175aa394465'),
(2, 'den', 'uncrypt>fc81bf8e0c36d8b7e16f0cf446385cd9de508c45'),
(3, 'manager', 'uncrypt>78bae5b0b1809844833f9ca4ae6e7ad01adfdacc');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_membergroup_access`
--

CREATE TABLE IF NOT EXISTS `modx_membergroup_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `membergroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_membergroup_names`
--

CREATE TABLE IF NOT EXISTS `modx_membergroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_member_groups`
--

CREATE TABLE IF NOT EXISTS `modx_member_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_group` int(10) NOT NULL DEFAULT '0',
  `member` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_group_member` (`user_group`,`member`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_content`
--

CREATE TABLE IF NOT EXISTS `modx_site_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'document',
  `contentType` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `longtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `link_attributes` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `published` int(1) NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` int(1) NOT NULL DEFAULT '0',
  `introtext` text COLLATE utf8_unicode_ci COMMENT 'Used to provide quick summary of the document',
  `content` mediumtext COLLATE utf8_unicode_ci,
  `richtext` tinyint(1) NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` int(1) NOT NULL DEFAULT '1',
  `cacheable` int(1) NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0' COMMENT 'Date the document was published',
  `publishedby` int(10) NOT NULL DEFAULT '0' COMMENT 'ID of user who published the document',
  `menutitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Menu title',
  `donthit` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Disable page hit count',
  `haskeywords` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has links to keywords',
  `hasmetatags` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has links to meta tags',
  `privateweb` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private web document',
  `privatemgr` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private manager document',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-inline, 1-attachment',
  `hidemenu` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Hide document from menu',
  `alias_visible` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `parent` (`parent`),
  KEY `aliasidx` (`alias`),
  KEY `typeidx` (`type`),
  FULLTEXT KEY `content_ft_idx` (`pagetitle`,`description`,`content`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site document tree.' AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `modx_site_content`
--

INSERT INTO `modx_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `haskeywords`, `hasmetatags`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `alias_visible`) VALUES
(1, 'document', 'text/html', 'главная', '', '', 'minimal-base', '', 1, 0, 0, 0, 0, '', '', 1, 3, 0, 1, 1, 1, 1130304721, 1, 1452271191, 0, 0, 0, 1130304721, 1, 'Base Install', 0, 0, 0, 0, 0, 0, 0, 1),
(2, 'document', 'text/html', 'test_1', '', '', 'test_1', '', 1, 0, 0, 4, 1, '', '', 1, 5, 1, 1, 1, 1, 1452271286, 1, 1452606535, 0, 0, 0, 1452271290, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(3, 'document', 'text/html', 'страница ответов', '', '', 'stranica-otvetov', '', 1, 0, 0, 2, 0, '', '', 1, 6, 0, 1, 1, 1, 1452271313, 1, 1452271319, 0, 0, 0, 1452271319, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(4, 'document', 'text/html', 'test_folder', '', '', 'test_folder', '', 1, 0, 0, 0, 1, '', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>', 1, 0, 2, 1, 1, 1, 1452271975, 1, 1452606331, 0, 0, 0, 1452271980, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(5, 'document', 'text/html', 'chek_form', '', '', 'chek_form', '', 1, 0, 0, 2, 0, '', '', 1, 7, 1, 1, 1, 1, 1452611255, 1, 1452611260, 0, 0, 0, 1452611260, 1, '', 0, 0, 0, 0, 0, 0, 0, 1),
(6, 'document', 'text/html', 'result_form', '', '', 'result_form', '', 1, 0, 0, 2, 0, '', '', 1, 8, 1, 1, 1, 2, 1452885013, 2, 1452953049, 0, 0, 0, 1452953049, 2, '', 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_content_metatags`
--

CREATE TABLE IF NOT EXISTS `modx_site_content_metatags` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `metatag_id` int(11) NOT NULL DEFAULT '0',
  KEY `content_id` (`content_id`),
  KEY `metatag_id` (`metatag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reference table between meta tags and content';

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_htmlsnippets`
--

CREATE TABLE IF NOT EXISTS `modx_site_htmlsnippets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site chunks.' AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `modx_site_htmlsnippets`
--

INSERT INTO `modx_site_htmlsnippets` (`id`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`) VALUES
(1, 'mm_rules', 'Default ManagerManager rules.', 0, 2, 0, '// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php\n// example of how PHP is allowed - check that a TV named documentTags exists before creating rule\n\nif ($modx->db->getValue($modx->db->select(''count(id)'', $modx->getFullTableName(''site_tmplvars''), "name=''documentTags''"))) {\n	mm_widget_tags(''documentTags'', '' ''); // Give blog tag editing capabilities to the ''documentTags (3)'' TV\n}\nmm_widget_showimagetvs(); // Always give a preview of Image TVs\n', 0),
(2, 'WebLoginSideBar', 'WebLogin Tpl', 0, 3, 0, '<!-- #declare:separator <hr> -->\n<!-- login form section-->\n<form method="post" name="loginfrm" action="[+action+]">\n    <input type="hidden" value="[+rememberme+]" name="rememberme" />\n    <fieldset>\n        <h3>Your Login Details</h3>\n        <label for="username">User: <input type="text" name="username" id="username" tabindex="1" onkeypress="return webLoginEnter(document.loginfrm.password);" value="[+username+]" /></label>\n    	<label for="password">Password: <input type="password" name="password" id="password" tabindex="2" onkeypress="return webLoginEnter(document.loginfrm.cmdweblogin);" value="" /></label>\n    	<input type="checkbox" id="checkbox_1" name="checkbox_1" tabindex="3" size="1" value="" [+checkbox+] onclick="webLoginCheckRemember()" /><label for="checkbox_1" class="checkbox">Remember me</label>\n    	<input type="submit" value="[+logintext+]" name="cmdweblogin" class="button" />\n	<a href="#" onclick="webLoginShowForm(2);return false;" id="forgotpsswd">Forget Your Password?</a>\n	</fieldset>\n</form>\n<hr>\n<!-- log out hyperlink section -->\n<h4>You''re already logged in</h4>\nDo you wish to <a href="[+action+]" class="button">[+logouttext+]</a>?\n<hr>\n<!-- Password reminder form section -->\n<form name="loginreminder" method="post" action="[+action+]">\n    <fieldset>\n        <h3>It happens to everyone...</h3>\n        <input type="hidden" name="txtpwdrem" value="0" />\n        <label for="txtwebemail">Enter the email address of your account to reset your password: <input type="text" name="txtwebemail" id="txtwebemail" size="24" /></label>\n        <label>To return to the login form, press the cancel button.</label>\n    	<input type="submit" value="Submit" name="cmdweblogin" class="button" /> <input type="reset" value="Cancel" name="cmdcancel" onclick="webLoginShowForm(1);" class="button" style="clear:none;display:inline" />\n    </fieldset>\n</form>\n\n', 0),
(3, 'tpl_main', '', 0, 9, 0, '<!DOCTYPE html>\r\n<html lang="ru">\r\n<head>\r\n	<meta charset="UTF-8">\r\n	<link rel="stylesheet" href="../assets/modules/test_folder/admin_css.css">\r\n	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>\r\n	<script>\r\n		var j = jQuery.noConflict();\r\n		j(function(){\r\n			j(''.test_link'').click(function(e){\r\n				e.preventDefault();\r\n				document.test_id.id_test.value=j(this).attr(''href'');\r\n				document.test_id.submit();\r\n			});\r\n		});\r\n	</script>\r\n</head>\r\n<body>\r\n	<p>Выберите тест</p>\r\n	<p>[+test_link+]</p>\r\n	<p><i>Для создания нового теста нужно создать ресурс в test_folder и страницу ответов в новом тесте</i></p>\r\n	<form name="test_id" method="post">\r\n		<input type="hidden" name="id_test" value="[+id_test+]">\r\n	</form>\r\n</body>\r\n</html>', 0),
(4, 'quest_view', '', 0, 9, 0, '<!DOCTYPE html>\r\n<html lang="ru">\r\n<head>\r\n	<meta charset="UTF-8">\r\n	<link rel="stylesheet" href="../assets/modules/test_folder/admin_css.css">\r\n	<link rel="stylesheet" href="../assets/plugins/tinymce/tiny_mce/themes/advanced/skins/default/ui.css">\r\n	<link rel="stylesheet" href="../assets/plugins/tinymce/tiny_mce/plugins/inlinepopups/skins/clearlooks2/window.css">\r\n	\r\n	\r\n	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>\r\n	\r\n	<script language="javascript" type="text/javascript" src="../assets/plugins/tinymce/tiny_mce/tiny_mce.js"></script>\r\n	<script language="javascript" type="text/javascript" src="../assets/plugins/tinymce/js/xconfig.js"></script>\r\n	<script>\r\n		var j = jQuery.noConflict(),\r\n			add_html = ''[+add_quest_html+]'';\r\n		\r\n		j(function(){\r\n			j(''.test_link'').click(function(e){\r\n				e.preventDefault();\r\n				document.test_id.id_test.value=j(this).attr(''href'');\r\n				document.test_id.submit();\r\n			});\r\n		});\r\n	</script>\r\n	<script src="../assets/modules/test_folder/admin_js.js"></script>\r\n</head>\r\n<body>\r\n	<button id="back">Назад</button>\r\n	<button id="add_quest">Добавить вопрос</button>\r\n	<p>Список вопросов</p>\r\n	<div>[+quest_list+]</div>\r\n	\r\n	<form name="test_id" method="post">\r\n		<input type="hidden" name="id_test" class="id_test" value="[+id_test+]">\r\n	</form>\r\n</body>\r\n</html>', 0),
(5, 'add_quest_html', '', 0, 9, 0, '<form name="add_quest_" method="POST" id="add_quest_">\r\n	<input type="hidden" name="type" value="add_new_quest">\r\n	<input name="quest_title" type="text" required placeholder="Текст вопроса">\r\n	<input name="quest_sort" type="text" required placeholder="Индекс сортировки" >\r\n	\r\n	<textarea name="quest_correct_answer" class="new_textarea"></textarea>\r\n	<input name="quest_content_flag" type="text" required style="height:0;opacity:0;">\r\n	\r\n	<button id="save_add_quest">Сохранить</button>\r\n</form>', 0),
(6, 'test_quest_line', '', 0, 0, 0, '<div class="quest_line">\r\n	<form name="quest_form_[+id+]" method="POST">\r\n		<input type="hidden" name="id" value="[+id+]">\r\n		<input type="hidden" name="id_test" value="[+id_test+]">\r\n		<div class="left_zone">\r\n			<p>Вопрос:  <input type="text" name="quest_title"  value="[+quest_title+]" required></p>\r\n			<p>Индекс сортировки: <input type="text" name="quest_sort" value="[+quest_sort+]" required></p>\r\n			<div class="answer_list">\r\n				<ul>[+answer_list+]</ul>\r\n				<button class="add_answer">добавить ответ</button>\r\n			</div>\r\n		</div><div class="content_true_answer">\r\n			<p>Контент ответа правильного вопроса</p>\r\n			<textarea class="content_answer" id="content_answe_[+id+]">[+quest_correct_answer+]</textarea>\r\n			<input type="text" class="hid_input" name="quest_correct_answer" required value=''[+quest_correct_answer+]''>\r\n			<input type="text" name="img" value="[+img+]" required id="img_[+id+]">\r\n			<button class="add_img" data-id="[+id+]">Добавить картинку</button>\r\n		</div>\r\n		\r\n		\r\n		\r\n		<input type="hidden" name="type" value="">\r\n		<button class="save_quest">Сохранить</button>\r\n		<button class="del_quest">Удалить</button>\r\n	</form>\r\n</div>', 0),
(7, 'add_answer_line', '', 0, 0, 0, '<li class="answer">\r\n	<input type="text" name="answer[ [+id+] ][title]" value="[+answer_title+]">\r\n	<input type="hidden" class="status" name="answer[ [+id+] ][flag]" value="[+status+]">\r\n	<input type="radio" name="true_answer" required>\r\n	<button class="del_answer">Удалить ответ</button>\r\n</li>', 0),
(8, 'add_answer_line_act', '', 0, 0, 0, '<li class="answer">\r\n	<input type="text" name="answer[ [+id+] ][title]" value="[+answer_title+]">\r\n	<input type="hidden" class="status" name="answer[ [+id+] ][flag]" value="[+status+]">\r\n	<input type="radio" name="true_answer" required checked="checked">\r\n	<button class="del_answer">Удалить ответ</button>\r\n</li>', 0),
(9, 'quest_tab', '', 0, 10, 0, '<div class="quest_tab">\r\n	<section class="content-header">\r\n		<img src="[+img+]" alt="picture-top">\r\n	</section>\r\n	<section class="test">	\r\n		<h2>[+quest_title+]</h2>\r\n		<form id="test-answer-[+id+]" action="#" method="post" data-id="[+id+]">\r\n			[+answer_list+]\r\n		</form>\r\n	</section>\r\n</div>', 0),
(11, 'quest_full', '', 0, 0, 0, '<section class="test">\r\n            <h2>[+iter+]. [+quest_title+]</h2>\r\n	<form id="test-answer-1" action="#" method="post" style="pointer-events:none;">\r\n                [+answer_list+]\r\n            </form>\r\n        </section><!--test-->\r\n        <section class="quote">\r\n            <div class="picture-user">\r\n                <img src="images/img-1.png" alt="img-1">\r\n                <b>Анна Лысенко</b>\r\n                <p>Основатель программы Beauty Matrix</p>\r\n            </div>\r\n            <div class="balloon">\r\n                [+quest_correct_answer+]\r\n            </div>\r\n            <br>\r\n        </section>', 0),
(10, 'answer_list', '', 0, 10, 0, '<p><input id="an[+id_q+]-[+id+]" type="radio" name="answer" value="[+id+]"><label for="an[+id_q+]-[+id+]">\r\n<span class="radio-button"></span>[+answer_title+]</label></p>', 0),
(12, 'answer_list_user', '', 0, 10, 0, '<p><input id="an1-2" type="radio" name="answer" value="a2" [+cheked+]><label for="an1-2" class="[+class+]">\r\n                    <span class="radio-button"></span>[+answer_title+] [+true_answer+][+false_answer+]</label></p>', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_keywords`
--

CREATE TABLE IF NOT EXISTS `modx_site_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site keyword list' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_metatags`
--

CREATE TABLE IF NOT EXISTS `modx_site_metatags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'tag name',
  `tagvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `http_equiv` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 - use http_equiv tag style, 0 - use name',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site meta tags' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_modules`
--

CREATE TABLE IF NOT EXISTS `modx_site_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `wrap` tinyint(4) NOT NULL DEFAULT '0',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'url to module icon',
  `enable_resource` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'enables the resource file feature',
  `resourcefile` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'a physical link to a resource file',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `guid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'globally unique identifier',
  `enable_sharedparams` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COLLATE utf8_unicode_ci,
  `modulecode` mediumtext COLLATE utf8_unicode_ci COMMENT 'module boot up code',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Modules' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `modx_site_modules`
--

INSERT INTO `modx_site_modules` (`id`, `name`, `description`, `editor_type`, `disabled`, `category`, `wrap`, `locked`, `icon`, `enable_resource`, `resourcefile`, `createdon`, `editedon`, `guid`, `enable_sharedparams`, `properties`, `modulecode`) VALUES
(1, 'Doc Manager', '<strong>1.1</strong> Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions', 0, 0, 4, 0, 0, '', 0, '', 0, 0, 'docman435243542tf542t5t', 1, '', 'include_once(MODX_BASE_PATH.''assets/modules/docmanager/classes/docmanager.class.php'');\ninclude_once(MODX_BASE_PATH.''assets/modules/docmanager/classes/dm_frontend.class.php'');\ninclude_once(MODX_BASE_PATH.''assets/modules/docmanager/classes/dm_backend.class.php'');\n\n$dm = new DocManager($modx);\n$dmf = new DocManagerFrontend($dm, $modx);\n$dmb = new DocManagerBackend($dm, $modx);\n\n$dm->ph = $dm->getLang();\n$dm->ph[''theme''] = $dm->getTheme();\n$dm->ph[''ajax.endpoint''] = MODX_SITE_URL.''assets/modules/docmanager/tv.ajax.php'';\n$dm->ph[''datepicker.offset''] = $modx->config[''datepicker_offset''];\n$dm->ph[''datetime.format''] = $modx->config[''datetime_format''];\n\nif (isset($_POST[''tabAction''])) {\n    $dmb->handlePostback();\n} else {\n    $dmf->getViews();\n    echo $dm->parseTemplate(''main.tpl'', $dm->ph);\n}'),
(2, 'test_content', '', 0, 0, 0, 0, 0, '', 0, '', 0, 0, '', 0, NULL, 'include_once (MODX_BASE_PATH.''assets/modules/test_folder/test_content.class.php'');\r\n\r\n$work = new test_content($modx);\r\n\r\n$work->creat_view();');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_module_access`
--

CREATE TABLE IF NOT EXISTS `modx_site_module_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` int(11) NOT NULL DEFAULT '0',
  `usergroup` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Module users group access permission' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_module_depobj`
--

CREATE TABLE IF NOT EXISTS `modx_site_module_depobj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` int(11) NOT NULL DEFAULT '0',
  `resource` int(11) NOT NULL DEFAULT '0',
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '10-chunks, 20-docs, 30-plugins, 40-snips, 50-tpls, 60-tvs',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Module Dependencies' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_plugins`
--

CREATE TABLE IF NOT EXISTS `modx_site_plugins` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Plugin',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `plugincode` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COLLATE utf8_unicode_ci COMMENT 'Default Properties',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the plugin',
  `moduleguid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site plugins.' AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `modx_site_plugins`
--

INSERT INTO `modx_site_plugins` (`id`, `name`, `description`, `editor_type`, `category`, `cache_type`, `plugincode`, `locked`, `properties`, `disabled`, `moduleguid`) VALUES
(1, 'TinyMCE Rich Text Editor', '<strong>3.5.11</strong> Javascript WYSIWYG Editor', 0, 4, 0, 'require MODX_BASE_PATH.''assets/plugins/tinymce/plugin.tinymce.php'';\n', 0, '&customparams=Custom Parameters;textarea;valid_elements : "*[*]", &mce_formats=Block Formats;text;p,h1,h2,h3,h4,h5,h6,div,blockquote,code,pre &entity_encoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &mce_path_options=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &mce_resizing=Advanced Resizing;list;true,false;true &disabledButtons=Disabled Buttons;text; &link_list=Link List;list;enabled,disabled;enabled &webtheme=Web Theme;list;simple,editor,creative,custom;simple &webPlugins=Web Plugins;text;style,advimage,advlink,searchreplace,contextmenu,paste,fullscreen,xhtmlxtras,media &webButtons1=Web Buttons 1;text;undo,redo,selectall,|,pastetext,pasteword,|,search,replace,|,hr,charmap,|,image,link,unlink,anchor,media,|,cleanup,removeformat,|,fullscreen,code,help &webButtons2=Web Buttons 2;text;bold,italic,underline,strikethrough,sub,sup,|,|,blockquote,bullist,numlist,outdent,indent,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,|,styleprops &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;500', 0, ''),
(2, 'CodeMirror', '<strong>1.2b</strong> JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirrir 3.13', 0, 4, 0, '$_CM_BASE = ''assets/plugins/codemirror/'';\r\n\r\n$_CM_URL = $modx->config[''site_url''] . $_CM_BASE;\r\n\r\nrequire(MODX_BASE_PATH. $_CM_BASE .''codemirror.plugin.php'');\r\n\r\n', 0, '&theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light; &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;true', 0, ''),
(3, 'Quick Manager+', '<strong>1.5.6</strong> Enables QuickManager+ front end content editing support', 0, 4, 0, '// In manager\nif (isset($_SESSION[''mgrValidated''])) {\n\n    $show = TRUE;\n\n    if ($disabled  != '''') {\n        $arr = array_filter(array_map(''intval'', explode('','', $disabled)));\n        if (in_array($modx->documentIdentifier, $arr)) {\n            $show = FALSE;\n        }\n    }\n\n    if ($show) {\n        // Replace [*#tv*] with QM+ edit TV button placeholders\n        if ($tvbuttons == ''true'') {\n            $e = $modx->Event;\n            if ($e->name == ''OnParseDocument'') {\n                 $output = &$modx->documentOutput;\n                 $output = preg_replace(''~\\[\\*#(.*?)\\*\\]~'', ''<!-- ''.$tvbclass.'' $1 -->[*$1*]'', $output);\n                 $modx->documentOutput = $output;\n             }\n         }\n        // In manager\n        if (isset($_SESSION[''mgrValidated''])) {\n            include_once($modx->config[''base_path''].''assets/plugins/qm/qm.inc.php'');\n            $qm = new Qm($modx, $jqpath, $loadmanagerjq, $loadfrontendjq, $noconflictjq, $loadtb, $tbwidth, $tbheight, $hidefields, $hidetabs, $hidesections, $addbutton, $tpltype, $tplid, $custombutton, $managerbutton, $logout, $autohide, $editbuttons, $editbclass, $newbuttons, $newbclass, $tvbuttons, $tvbclass);\n        }\n    }\n}', 0, '&jqpath=Path to jQuery;text;assets/js/jquery.min.js &loadmanagerjq=Load jQuery in manager;list;true,false;false &loadfrontendjq=Load jQuery in front-end;list;true,false;true &noconflictjq=jQuery noConflict mode in front-end;list;true,false;true &loadtb=Load modal box in front-end;list;true,false;true &tbwidth=Modal box window width;text;80% &tbheight=Modal box window height;text;90% &hidefields=Hide document fields from front-end editors;text;parent &hidetabs=Hide document tabs from front-end editors;text; &hidesections=Hide document sections from front-end editors;text; &addbutton=Show add document here button;list;true,false;true &tpltype=New document template type;list;parent,id,selected;parent &tplid=New document template id;int;3 &custombutton=Custom buttons;textarea; &managerbutton=Show go to manager button;list;true,false;true &logout=Logout to;list;manager,front-end;manager &disabled=Plugin disabled on documents;text; &autohide=Autohide toolbar;list;true,false;true &editbuttons=Inline edit buttons;list;true,false;false &editbclass=Edit button CSS class;text;qm-edit &newbuttons=Inline new resource buttons;list;true,false;false &newbclass=New resource button CSS class;text;qm-new &tvbuttons=Inline template variable buttons;list;true,false;false &tvbclass=Template variable button CSS class;text;qm-tv', 1, ''),
(4, 'ManagerManager', '<strong>0.6.2</strong> Customize the MODX Manager to offer bespoke admin functions for end users.', 0, 4, 0, '// You can put your ManagerManager rules EITHER in a chunk OR in an external file - whichever suits your development style the best\n\n// To use an external file, put your rules in /assets/plugins/managermanager/mm_rules.inc.php \n// (you can rename default.mm_rules.inc.php and use it as an example)\n// The chunk SHOULD have php opening tags at the beginning and end\n\n// If you want to put your rules in a chunk (so you can edit them through the Manager),\n// create the chunk, and enter its name in the configuration tab.\n// The chunk should NOT have php tags at the beginning or end.\n\n// See also user-friendly module for editing ManagerManager configuration file ddMMEditor (http://code.divandesign.biz/modx/ddmmeditor).\n\n// ManagerManager requires jQuery 1.9.1, which is located in /assets/plugins/managermanager/js/ folder.\n\n// You don''t need to change anything else from here onwards\n//-------------------------------------------------------\n\n// Run the main code\ninclude($modx->config[''base_path''].''assets/plugins/managermanager/mm.inc.php'');', 0, '&remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules', 0, ''),
(5, 'Search Highlight', '<strong>1.5</strong> Used with AjaxSearch to show search terms highlighted on page linked from search results', 0, 5, 0, '/*\n  ------------------------------------------------------------------------\n  Plugin: Search_Highlight v1.5\n  ------------------------------------------------------------------------\n  Changes:\n  18/03/10 - Remove possibility of XSS attempts being passed in the URL\n           - look-behind assertion improved\n  29/03/09 - Removed urldecode calls;\n           - Added check for magic quotes - if set, remove slashes\n           - Highlights terms searched for when target is a HTML entity\n  18/07/08 - advSearch parameter and pcre modifier added\n  10/02/08 - Strip_tags added to avoid sql injection and XSS. Use of $_REQUEST\n  01/03/07 - Added fies/updates from forum from users mikkelwe/identity\n  (better highlight replacement, additional div around term/removal message)\n  ------------------------------------------------------------------------\n  Description: When a user clicks on the link from the AjaxSearch results\n    the target page will have the terms highlighted.\n  ------------------------------------------------------------------------\n  Created By:  Susan Ottwell (sottwell@sottwell.com)\n               Kyle Jaebker (kjaebker@muddydogpaws.com)\n\n  Refactored by Coroico (www.evo.wangba.fr) and TS\n  ------------------------------------------------------------------------\n  Based off the the code by Susan Ottwell (www.sottwell.com)\n    http://forums.modx.com/thread/47775/plugin-highlight-search-terms\n  ------------------------------------------------------------------------\n  CSS:\n    The classes used for the highlighting are the same as the AjaxSearch\n  ------------------------------------------------------------------------\n  Notes:\n    To add a link to remove the highlighting and to show the searchterms\n    put the following on your page where you would like this to appear:\n\n      <!--search_terms-->\n\n    Example output for this:\n\n      Search Terms: the, template\n      Remove Highlighting\n\n    Set the following variables to change the text:\n\n      $termText - the text before the search terms\n      $removeText - the text for the remove link\n  ------------------------------------------------------------------------\n*/\nglobal $database_connection_charset;\n// Conversion code name between html page character encoding and Mysql character encoding\n// Some others conversions should be added if needed. Otherwise Page charset = Database charset\n$pageCharset = array(\n  ''utf8'' => ''UTF-8'',\n  ''latin1'' => ''ISO-8859-1'',\n  ''latin2'' => ''ISO-8859-2''\n);\n\nif (isset($_REQUEST[''searched'']) && isset($_REQUEST[''highlight''])) {\n\n  // Set these to customize the text for the highlighting key\n  // --------------------------------------------------------\n     $termText = ''<div class="searchTerms">Search Terms: '';\n     $removeText = ''Remove Highlighting'';\n  // --------------------------------------------------------\n\n  $highlightText = $termText;\n  $advsearch = ''oneword'';\n\n  $dbCharset = $database_connection_charset;\n  $pgCharset = array_key_exists($dbCharset,$pageCharset) ? $pageCharset[$dbCharset] : $dbCharset;\n\n  // magic quotes check\n  if (get_magic_quotes_gpc()){\n    $searched = strip_tags(stripslashes($_REQUEST[''searched'']));\n    $highlight = strip_tags(stripslashes($_REQUEST[''highlight'']));\n    if (isset($_REQUEST[''advsearch''])) $advsearch = strip_tags(stripslashes($_REQUEST[''advsearch'']));\n  }\n  else {\n    $searched = strip_tags($_REQUEST[''searched'']);\n    $highlight = strip_tags($_REQUEST[''highlight'']);\n    if (isset($_REQUEST[''advsearch''])) $advsearch = strip_tags($_REQUEST[''advsearch'']);\n  }\n\n  if ($advsearch != ''nowords'') {\n\n    $searchArray = array();\n    if ($advsearch == ''exactphrase'') $searchArray[0] = $searched;\n    else $searchArray = explode('' '', $searched);\n\n    $searchArray = array_unique($searchArray);\n    $nbterms = count($searchArray);\n    $searchTerms = array();\n    for($i=0;$i<$nbterms;$i++){\n      // Consider all possible combinations\n      $word_ents = array();\n      $word_ents[] = $searchArray[$i];\n      $word_ents[] = htmlentities($searchArray[$i], ENT_NOQUOTES, $pgCharset);\n      $word_ents[] = htmlentities($searchArray[$i], ENT_COMPAT, $pgCharset);\n      $word_ents[] = htmlentities($searchArray[$i], ENT_QUOTES, $pgCharset);\n      // Avoid duplication\n      $word_ents = array_unique($word_ents);\n      foreach($word_ents as $word) $searchTerms[]= array(''term'' => $word, ''class'' => $i+1);\n    }\n\n    $output = $modx->documentOutput; // get the parsed document\n    $body = explode("<body", $output); // break out the head\n\n    $highlightClass = explode('' '',$highlight); // break out the highlight classes\n    /* remove possibility of XSS attempts being passed in URL */\n    foreach ($highlightClass as $key => $value) {\n       $highlightClass[$key] = preg_match(''/[^A-Za-z0-9_-]/ms'', $value) == 1 ? '''' : $value;\n    }\n\n    $pcreModifier = ($pgCharset == ''UTF-8'') ? ''iu'' : ''i'';\n    $lookBehind = ''/(?<!&|&[\\w#]|&[\\w#]\\w|&[\\w#]\\w\\w|&[\\w#]\\w\\w\\w|&[\\w#]\\w\\w\\w\\w|&[\\w#]\\w\\w\\w\\w\\w)'';  // avoid a match with a html entity\n    $lookAhead = ''(?=[^>]*<)/''; // avoid a match with a html tag\n\n    $nbterms = count($searchTerms);\n    for($i=0;$i<$nbterms;$i++){\n      $word = $searchTerms[$i][''term''];\n      $class = $highlightClass[0].'' ''.$highlightClass[$searchTerms[$i][''class'']];\n\n      $highlightText .= ($i > 0) ? '', '' : '''';\n      $highlightText .= ''<span class="''.$class.''">''.$word.''</span>'';\n\n      $pattern = $lookBehind . preg_quote($word, ''/'') . $lookAhead . $pcreModifier;\n      $replacement = ''<span class="'' . $class . ''">${0}</span>'';\n      $body[1] = preg_replace($pattern, $replacement, $body[1]);\n    }\n\n    $output = implode("<body", $body);\n\n    $removeUrl = $modx->makeUrl($modx->documentIdentifier);\n    $highlightText .= ''<br /><a href="''.$removeUrl.''" class="ajaxSearch_removeHighlight">''.$removeText.''</a></div>'';\n\n    $output = str_replace(''<!--search_terms-->'',$highlightText,$output);\n    $modx->documentOutput = $output;\n  }\n}', 0, '', 1, ''),
(6, 'FileSource', '<strong>0.1</strong> Save snippet and plugins to file', 0, 4, 0, 'require MODX_BASE_PATH.''assets/plugins/filesource/plugin.filesource.php'';', 0, '', 0, ''),
(7, 'TransAlias', '<strong>1.0.2</strong> Human readible URL translation supporting multiple languages and overrides', 0, 4, 0, 'require MODX_BASE_PATH.''assets/plugins/transalias/plugin.transalias.php'';', 0, '&table_name=Trans table;list;common,russian,dutch,german,czech,utf8,utf8lowercase;russian &char_restrict=Restrict alias to;list;lowercase alphanumeric,alphanumeric,legal characters;legal characters &remove_periods=Remove Periods;list;Yes,No;No &word_separator=Word Separator;list;dash,underscore,none;dash &override_tv=Override TV name;string;', 0, ' ');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_plugin_events`
--

CREATE TABLE IF NOT EXISTS `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL,
  `evtid` int(10) NOT NULL DEFAULT '0',
  `priority` int(10) NOT NULL DEFAULT '0' COMMENT 'determines plugin run order',
  PRIMARY KEY (`pluginid`,`evtid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Links to system events';

--
-- Дамп данных таблицы `modx_site_plugin_events`
--

INSERT INTO `modx_site_plugin_events` (`pluginid`, `evtid`, `priority`) VALUES
(1, 85, 0),
(1, 87, 0),
(1, 88, 0),
(2, 23, 0),
(2, 29, 0),
(2, 35, 0),
(2, 41, 0),
(2, 47, 0),
(2, 73, 0),
(3, 3, 0),
(3, 13, 0),
(3, 28, 0),
(3, 31, 0),
(3, 92, 0),
(4, 28, 0),
(4, 29, 0),
(4, 30, 0),
(4, 31, 0),
(4, 35, 0),
(4, 53, 0),
(4, 205, 0),
(5, 3, 0),
(6, 34, 0),
(6, 35, 0),
(6, 36, 0),
(6, 40, 0),
(6, 41, 0),
(6, 42, 0),
(7, 100, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_snippets`
--

CREATE TABLE IF NOT EXISTS `modx_site_snippets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Snippet',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COLLATE utf8_unicode_ci COMMENT 'Default Properties',
  `moduleguid` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site snippets.' AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `modx_site_snippets`
--

INSERT INTO `modx_site_snippets` (`id`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`) VALUES
(1, 'Personalize', '<strong>2.1</strong> Personalize snippet', 0, 3, 0, 'return require MODX_BASE_PATH.''assets/snippets/personalize/snippet.personalize.php'';', 0, '', ''),
(2, 'FirstChildRedirect', '<strong>2.0</strong> Automatically redirects to the first child of a Container Resource', 0, 6, 0, 'return require MODX_BASE_PATH.''assets/snippets/firstchildredirect/snippet.firstchildredirect.php'';', 0, '', ''),
(3, 'Reflect', '<strong>2.1.0</strong> Generates date-based archives using Ditto', 0, 7, 0, '/*\n * Author: \n *      Mark Kaplan for MODX CMF\n * \n * Note: \n *      If Reflect is not retrieving its own documents, make sure that the\n *          Ditto call feeding it has all of the fields in it that you plan on\n *       calling in your Reflect template. Furthermore, Reflect will ONLY\n *          show what is currently in the Ditto result set.\n *       Thus, if pagination is on it will ONLY show that page''s items.\n*/\n \n\n// ---------------------------------------------------\n//  Includes\n// ---------------------------------------------------\n\n$reflect_base = isset($reflect_base) ? $modx->config[''base_path''].$reflect_base : $modx->config[''base_path'']."assets/snippets/reflect/";\n/*\n    Param: ditto_base\n    \n    Purpose:\n    Location of Ditto files\n\n    Options:\n    Any valid folder location containing the Ditto source code with a trailing slash\n\n    Default:\n    [(base_path)]assets/snippets/ditto/\n*/\n\n$config = (isset($config)) ? $config : "default";\n/*\n    Param: config\n\n    Purpose:\n    Load a custom configuration\n\n    Options:\n    "default" - default blank config file\n    CONFIG_NAME - Other configs installed in the configs folder or in any folder within the MODX base path via @FILE\n\n    Default:\n    "default"\n    \n    Related:\n    - <extenders>\n*/\n\nrequire($reflect_base."configs/default.config.php");\nrequire($reflect_base."default.templates.php");\nif ($config != "default") {\n    require((substr($config, 0, 5) != "@FILE") ? $reflect_base."configs/$config.config.php" : $modx->config[''base_path''].trim(substr($config, 5)));\n}\n\n// ---------------------------------------------------\n//  Parameters\n// ---------------------------------------------------\n\n$id = isset($id) ? $id."_" : false;\n/*\n    Param: id\n\n    Purpose:\n    Unique ID for this Ditto instance for connection with other scripts (like Reflect) and unique URL parameters\n\n    Options:\n    Any valid folder location containing the Ditto source code with a trailing slash\n\n    Default:\n    "" - blank\n*/\n$getDocuments = isset($getDocuments) ? $getDocuments : 0;\n/*\n    Param: getDocuments\n\n    Purpose:\n    Force Reflect to get documents\n\n    Options:\n    0 - off\n    1 - on\n    \n    Default:\n    0 - off\n*/\n$showItems = isset($showItems) ? $showItems : 1;\n/*\n    Param: showItems\n\n    Purpose:\n    Show individual items in the archive\n\n    Options:\n    0 - off\n    1 - on\n    \n    Default:\n    1 - on\n*/\n$groupByYears = isset($groupByYears)? $groupByYears : 1;\n/*\n    Param: groupByYears\n\n    Purpose:\n    Group the archive by years\n\n    Options:\n    0 - off\n    1 - on\n    \n    Default:\n    1 - on\n*/\n$targetID = isset($targetID) ? $targetID : $modx->documentObject[''id''];\n/*\n    Param: targetID\n\n    Purpose:\n    ID for archive links to point to\n\n    Options:\n    Any MODX document with a Ditto call setup with extenders=`dateFilter`\n    \n    Default:\n    Current MODX Document\n*/\n$dateSource = isset($dateSource) ? $dateSource : "createdon";\n/*\n    Param: dateSource\n\n    Purpose:\n    Date source to display for archive items\n\n    Options:\n    # - Any UNIX timestamp from MODX fields or TVs such as createdon, pub_date, or editedon\n    \n    Default:\n    "createdon"\n    \n    Related:\n    - <dateFormat>\n*/\n$dateFormat = isset($dateFormat) ? $dateFormat : "%d-%b-%y %H:%M";  \n/*\n    Param: dateFormat\n\n    Purpose:\n    Format the [+date+] placeholder in human readable form\n\n    Options:\n    Any PHP valid strftime option\n\n    Default:\n    "%d-%b-%y %H:%M"\n    \n    Related:\n    - <dateSource>\n*/\n$yearSortDir = isset($yearSortDir) ? $yearSortDir : "DESC";\n/*\n    Param: yearSortDir\n\n    Purpose:\n    Direction to sort documents\n\n    Options:\n    ASC - ascending\n    DESC - descending\n\n    Default:\n    "DESC"\n    \n    Related:\n    - <monthSortDir>\n*/\n$monthSortDir = isset($monthSortDir) ? $monthSortDir : "ASC";\n/*\n    Param: monthSortDir\n\n    Purpose:\n    Direction to sort the months\n\n    Options:\n    ASC - ascending\n    DESC - descending\n\n    Default:\n    "ASC"\n    \n    Related:\n    - <yearSortDir>\n*/\n$start = isset($start)? intval($start) : 0;\n/*\n    Param: start\n\n    Purpose:\n    Number of documents to skip in the results\n    \n    Options:\n    Any number\n\n    Default:\n    0\n*/  \n$phx = (isset($phx))? $phx : 1;\n/*\n    Param: phx\n\n    Purpose:\n    Use PHx formatting\n\n    Options:\n    0 - off\n    1 - on\n    \n    Default:\n    1 - on\n*/\n\n// ---------------------------------------------------\n//  Initialize Ditto\n// ---------------------------------------------------\n$placeholder = ($id != false && $getDocuments == 0) ? true : false;\nif ($placeholder === false) {\n    $rID = "reflect_".rand(1,1000);\n    $itemTemplate = isset($tplItem) ? $tplItem: "@CODE:".$defaultTemplates[''item''];\n    $dParams = array(\n        "id" => "$rID",\n        "save" => "3",  \n        "summarize" => "all",\n        "tpl" => $itemTemplate,\n    );\n    \n    $source = $dittoSnippetName;\n    $params = $dittoSnippetParameters;\n        // TODO: Remove after 3.0\n        \n    if (isset($params)) {\n        $givenParams = explode("|",$params);\n        foreach ($givenParams as $parameter) {\n            $p = explode(":",$parameter);\n            $dParams[$p[0]] = $p[1];\n        }\n    }\n    /*\n        Param: params\n\n        Purpose:\n        Pass parameters to the Ditto instance used to retreive the documents\n\n        Options:\n        Any valid ditto parameters in the format name:value \n        with multiple parameters separated by a pipe (|)\n        \n        Note:\n        This parameter is only needed for config, start, and phx as you can\n        now simply use the parameter as if Reflect was Ditto\n\n        Default:\n        [NULL]\n    */\n    \n    $reflectParameters = array(''reflect_base'',''config'',''id'',''getDocuments'',''showItems'',''groupByYears'',''targetID'',''yearSortDir'',''monthSortDir'',''start'',''phx'',''tplContainer'',''tplYear'',''tplMonth'',''tplMonthInner'',''tplItem'',''save'');\n    $params =& $modx->event->params;\n    if(is_array($params)) {\n        foreach ($params as $param=>$value) {\n            if (!in_array($param,$reflectParameters) && substr($param,-3) != ''tpl'') {\n                $dParams[$param] = $value;\n            }\n        }\n    }\n\n    $source = isset($source) ? $source : "Ditto";\n    /*\n        Param: source\n\n        Purpose:\n        Name of the Ditto snippet to use\n\n        Options:\n        Any valid snippet name\n\n        Default:\n        "Ditto"\n    */\n    $snippetOutput = $modx->runSnippet($source,$dParams);\n    $ditto = $modx->getPlaceholder($rID."_ditto_object");\n    $resource = $modx->getPlaceholder($rID."_ditto_resource");\n} else {\n    $ditto = $modx->getPlaceholder($id."ditto_object");\n    $resource = $modx->getPlaceholder($id."ditto_resource");\n}\nif (!is_object($ditto) || !isset($ditto) || !isset($resource)) {\n    return !empty($snippetOutput) ? $snippetOutput : "The Ditto object is invalid. Please check it.";\n}\n\n// ---------------------------------------------------\n//  Templates\n// ---------------------------------------------------\n\n$templates[''tpl''] = isset($tplContainer) ? $ditto->template->fetch($tplContainer): $defaultTemplates[''tpl''];\n/*\n    Param: tplContainer\n\n    Purpose:\n    Container template for the archive\n\n    Options:\n    - Any valid chunk name\n    - Code via @CODE:\n    - File via @FILE:\n\n    Default:\n    See default.tempates.php\n*/\n$templates[''year''] = isset($tplYear) ? $ditto->template->fetch($tplYear): $defaultTemplates[''year''];\n/*\n    Param: tplYear\n\n    Purpose:\n    Template for the year item\n\n    Options:\n    - Any valid chunk name\n    - Code via @CODE:\n    - File via @FILE:\n\n    Default:\n    See default.tempates.php\n*/\n$templates[''year_inner''] = isset($tplYearInner) ? $ditto->template->fetch($tplYearInner): $defaultTemplates[''year_inner''];\n/*\n    Param: tplYearInner\n\n    Purpose:\n    Template for the year item (the ul to hold the year template)\n\n    Options:\n    - Any valid chunk name\n    - Code via @CODE:\n    - File via @FILE:\n\n    Default:\n    See default.tempates.php\n*/\n$templates[''month''] = isset($tplMonth) ? $ditto->template->fetch($tplMonth): $defaultTemplates[''month''];\n/*\n    Param: tplMonth\n\n    Purpose:\n    Template for the month item\n\n    Options:\n    - Any valid chunk name\n    - Code via @CODE:\n    - File via @FILE:\n\n    Default:\n    See default.tempates.php\n*/\n$templates[''month_inner''] = isset($tplMonthInner) ? $ditto->template->fetch($tplMonthInner): $defaultTemplates[''month_inner''];\n/*\n    Param: tplMonthInner\n\n    Purpose:\n    Template for the month item  (the ul to hold the month template)\n\n    Options:\n    - Any valid chunk name\n    - Code via @CODE:\n    - File via @FILE:\n\n    Default:\n    See default.tempates.php\n*/\n$templates[''item''] = isset($tplItem) ? $ditto->template->fetch($tplItem): $defaultTemplates[''item''];\n/*\n    Param: tplItem\n\n    Purpose:\n    Template for the individual item\n\n    Options:\n    - Any valid chunk name\n    - Code via @CODE:\n    - File via @FILE:\n\n    Default:\n    See default.tempates.php\n*/\n\n$ditto->addField("date","display","custom");\n    // force add the date field if receiving data from a Ditto instance\n\n// ---------------------------------------------------\n//  Reflect\n// ---------------------------------------------------\n\nif (function_exists("reflect") === FALSE) {\nfunction reflect($templatesDocumentID, $showItems, $groupByYears, $resource, $templatesDateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir) {\n    global $modx;\n    $cal = array();\n    $output = '''';\n    $ph = array(''year''=>'''',''month''=>'''',''item''=>'''',''out''=>'''');\n    $build = array();\n    $stop = count($resource);\n\n    // loop and fetch all the results\n    for ($i = $start; $i < $stop; $i++) {\n        $date = getdate($resource[$i][$templatesDateSource]);\n        $year = $date["year"];\n        $month = $date["mon"];\n        $cal[$year][$month][] = $resource[$i];\n    }\n    if ($yearSortDir == "DESC") {\n        krsort($cal);\n    } else {\n        ksort($cal);\n    }\n    foreach ($cal as $year=>$months) {\n        if ($monthSortDir == "ASC") {\n            ksort($months);\n        } else {\n            krsort($months);\n        }\n        $build[$year] = $months;\n    }\n    \n    foreach ($build as $year=>$months) {\n        $r_year = '''';\n        $r_month = '''';\n        $r_month_2 = '''';\n        $year_count = 0;\n        $items = array();\n        \n        foreach ($months as $mon=>$month) {\n            $month_text = strftime("%B", mktime(10, 10, 10, $mon, 10, $year));\n            $month_url = $ditto->buildURL("month=".$mon."&year=".$year."&day=false&start=0",$templatesDocumentID,$id);\n            $month_count = count($month);\n            $year_count += $month_count;\n            $r_month = $ditto->template->replace(array("year"=>$year,"month"=>$month_text,"url"=>$month_url,"count"=>$month_count),$templates[''month'']);\n            if ($showItems) {\n                foreach ($month as $item) {\n                    $items[$year][$mon][''items''][] = $ditto->render($item, $templates[''item''], false, $templatesDateSource, $dateFormat, array(),$phx);\n                }\n                $r_month_2 = $ditto->template->replace(array(''wrapper'' => implode('''',$items[$year][$mon][''items''])),$templates[''month_inner'']);\n                $items[$year][$mon] = $ditto->template->replace(array(''wrapper'' => $r_month_2),$r_month);\n            } else {\n                $items[$year][$mon] = $r_month;\n            }\n        }\n        if ($groupByYears) {\n            $year_url = $ditto->buildURL("year=".$year."&month=false&day=false&start=0",$templatesDocumentID,$id);\n            $r_year =  $ditto->template->replace(array("year"=>$year,"url"=>$year_url,"count"=>$year_count),$templates[''year'']);\n            $var = $ditto->template->replace(array(''wrapper''=>implode('''',$items[$year])),$templates[''year_inner'']);\n            $output .= $ditto->template->replace(array(''wrapper''=>$var),$r_year);\n        } else {\n            $output .= implode('''',$items[$year]);\n        }\n    }\n\n    $output = $ditto->template->replace(array(''wrapper''=>$output),$templates[''tpl'']);\n    $modx->setPlaceholder($id.''reset'',$ditto->buildURL(''year=false&month=false&day=false'',$templatesDocumentID,$id));\n\nreturn $output;\n    \n}\n}\n\nreturn reflect($targetID, $showItems, $groupByYears, $resource, $dateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir);', 0, '', ''),
(4, 'WebChangePwd', '<strong>1.0.1</strong> Allows Web User to change their password from the front-end of the website', 0, 3, 0, '# Created By Raymond Irving April, 2005\n#::::::::::::::::::::::::::::::::::::::::\n# Params:	\n#\n#	&tpl			- (Optional)\n#		Chunk name or document id to use as a template\n#				  \n#	Note: Templats design:\n#			section 1: change pwd template\n#			section 2: notification template \n#\n# Examples:\n#\n#	[[WebChangePwd? &tpl=`ChangePwd`]] \n\n# Set Snippet Paths \n$snipPath  = (($modx->isBackend())? "../":"");\n$snipPath .= "assets/snippets/";\n\n# check if inside manager\nif ($m = $modx->isBackend()) {\n	return ''''; # don''t go any further when inside manager\n}\n\n\n# Snippet customize settings\n$tpl		= isset($tpl)? $tpl:"";\n\n# System settings\n$isPostBack		= count($_POST) && isset($_POST[''cmdwebchngpwd'']);\n\n# Start processing\ninclude_once $snipPath."weblogin/weblogin.common.inc.php";\ninclude_once $snipPath."weblogin/webchangepwd.inc.php";\n\n# Return\nreturn $output;\n\n\n\n', 0, '', ''),
(5, 'WebSignup', '<strong>1.1.1</strong> Basic Web User account creation/signup system', 0, 3, 0, '# Created By Raymond Irving April, 2005\n#::::::::::::::::::::::::::::::::::::::::\n# Usage:     \n#    Allows a web user to signup for a new web account from the website\n#    This snippet provides a basic set of form fields for the signup form\n#    You can customize this snippet to create your own signup form\n#\n# Params:    \n#\n#    &tpl        - (Optional) Chunk name or document id to use as a template\n#	    		   If custom template AND captcha on AND using WebSignup and \n#                  WebLogin on the same page make sure you have a field named\n#                  cmdwebsignup. In the default template it is the submit button \n#                  One can use a hidden field.\n#    &groups     - Web users groups to be assigned to users\n#    &useCaptcha - (Optional) Determine to use (1) or not to use (0) captcha\n#                  on signup form - if not defined, will default to system\n#                  setting. GD is required for this feature. If GD is not \n#                  available, useCaptcha will automatically be set to false;\n#                  \n#    Note: Templats design:\n#        section 1: signup template\n#        section 2: notification template \n#\n# Examples:\n#\n#    [[WebSignup? &tpl=`SignupForm` &groups=`NewsReaders,WebUsers`]] \n\n# Set Snippet Paths \n$snipPath = $modx->config[''base_path''] . "assets/snippets/";\n\n# check if inside manager\nif ($m = $modx->isBackend()) {\n    return ''''; # don''t go any further when inside manager\n}\n\n\n# Snippet customize settings\n$tpl = isset($tpl)? $tpl:"";\n$useCaptcha = isset($useCaptcha)? $useCaptcha : $modx->config[''use_captcha''] ;\n// Override captcha if no GD\nif ($useCaptcha && !gd_info()) $useCaptcha = 0;\n\n# setup web groups\n$groups = isset($groups) ? array_filter(array_map(''trim'', explode('','', $groups))):array();\n\n# System settings\n$isPostBack        = count($_POST) && isset($_POST[''cmdwebsignup'']);\n\n$output = '''';\n\n# Start processing\ninclude_once $snipPath."weblogin/weblogin.common.inc.php";\ninclude_once $snipPath."weblogin/websignup.inc.php";\n\n# Return\nreturn $output;', 0, '&tpl=Template;string;', ''),
(6, 'MemberCheck', '<strong>1.1</strong> Show chunks based on a logged in Web User''s group membership', 0, 3, 0, 'return require MODX_BASE_PATH.''assets/snippets/membercheck/snippet.membercheck.php'';', 0, '', ''),
(7, 'if', '<strong>1.2</strong> A simple conditional snippet. Allows for eq/neq/lt/gt/etc logic within templates, resources, chunks, etc.', 0, 6, 0, 'return require MODX_BASE_PATH.''assets/snippets/if/snippet.if.php'';', 0, '', ''),
(8, 'ListIndexer', '<strong>1.0.1</strong> A flexible way to show the most recent Resources and other Resource lists', 0, 6, 0, 'return require MODX_BASE_PATH.''assets/snippets/listindexer/snippet.listindexer.php'';', 0, '', ''),
(9, 'phpthumb', '<strong>1.2</strong> PHPThumb creates thumbnails and altered images on the fly and caches them', 0, 7, 0, 'return require MODX_BASE_PATH.''assets/snippets/phpthumb/snippet.phpthumb.php'';\r\n', 0, '', ''),
(10, 'Breadcrumbs', '<strong>1.0.4</strong> Configurable breadcrumb page-trail navigation', 0, 6, 0, 'return require MODX_BASE_PATH.''assets/snippets/breadcrumbs/snippet.breadcrumbs.php'';', 0, '', ''),
(11, 'Ditto', '<strong>2.1.1</strong> Summarizes and lists pages to create blogs, catalogs, PR archives, bio listings and more', 0, 7, 0, 'return require MODX_BASE_PATH.''assets/snippets/ditto/snippet.ditto.php'';', 0, '', ''),
(12, 'Jot', '<strong>1.1.4</strong> User comments with moderation and email subscription', 0, 7, 0, '/*####\n#\n# Author: Armand "bS" Pondman (apondman@zerobarrier.nl)\n#\n# Latest Version: http://modx.com/extras/package/jot\n# Jot Demo Site: http://projects.zerobarrier.nl/modx/\n# Documentation: http://wiki.modxcms.com/index.php/Jot (wiki)\n#\n####*/\n\n$jotPath = $modx->config[''base_path''] . ''assets/snippets/jot/'';\ninclude_once($jotPath.''jot.class.inc.php'');\n\n$Jot = new CJot;\n$Jot->VersionCheck("1.1.4");\n$Jot->Set("path",$jotPath);\n$Jot->Set("action", $action);\n$Jot->Set("postdelay", $postdelay);\n$Jot->Set("docid", $docid);\n$Jot->Set("tagid", $tagid);\n$Jot->Set("subscribe", $subscribe);\n$Jot->Set("moderated", $moderated);\n$Jot->Set("captcha", $captcha);\n$Jot->Set("badwords", $badwords);\n$Jot->Set("bw", $bw);\n$Jot->Set("sortby", $sortby);\n$Jot->Set("numdir", $numdir);\n$Jot->Set("customfields", $customfields);\n$Jot->Set("guestname", $guestname);\n$Jot->Set("canpost", $canpost);\n$Jot->Set("canview", $canview);\n$Jot->Set("canedit", $canedit);\n$Jot->Set("canmoderate", $canmoderate);\n$Jot->Set("trusted", $trusted);\n$Jot->Set("pagination", $pagination);\n$Jot->Set("placeholders", $placeholders);\n$Jot->Set("subjectSubscribe", $subjectSubscribe);\n$Jot->Set("subjectModerate", $subjectModerate);\n$Jot->Set("subjectAuthor", $subjectAuthor);\n$Jot->Set("notify", $notify);\n$Jot->Set("notifyAuthor", $notifyAuthor);\n$Jot->Set("validate", $validate);\n$Jot->Set("title", $title);\n$Jot->Set("authorid", $authorid);\n$Jot->Set("css", $css);\n$Jot->Set("cssFile", $cssFile);\n$Jot->Set("cssRowAlt", $cssRowAlt);\n$Jot->Set("cssRowMe", $cssRowMe);\n$Jot->Set("cssRowAuthor", $cssRowAuthor);\n$Jot->Set("tplForm", $tplForm);\n$Jot->Set("tplComments", $tplComments);\n$Jot->Set("tplModerate", $tplModerate);\n$Jot->Set("tplNav", $tplNav);\n$Jot->Set("tplNotify", $tplNotify);\n$Jot->Set("tplNotifyModerator", $tplNotifyModerator);\n$Jot->Set("tplNotifyAuthor", $tplNotifyAuthor);\n$Jot->Set("tplSubscribe", $tplSubscribe);\n$Jot->Set("debug", $debug);\n$Jot->Set("output", $output);\nreturn $Jot->Run();', 0, '', ''),
(13, 'eForm', '<strong>1.4.6</strong> Robust form parser/processor with validation, multiple sending options, chunk/page support for forms and reports, and file uploads', 0, 8, 0, 'return require MODX_BASE_PATH.''assets/snippets/eform/snippet.eform.php'';', 0, '', ''),
(14, 'UltimateParent', '<strong>2.0</strong> Travels up the document tree from a specified document and returns its "ultimate" non-root parent', 0, 6, 0, 'return require MODX_BASE_PATH.''assets/snippets/ultimateparent/snippet.ultimateparent.php'';', 0, '', ''),
(15, 'WebLogin', '<strong>1.1.1</strong> Allows webusers to login to protected pages in the website, supporting multiple user groups', 0, 3, 0, '# Created By Raymond Irving 2004\n#::::::::::::::::::::::::::::::::::::::::\n# Params:	\n#\n#	&loginhomeid 	- (Optional)\n#		redirects the user to first authorized page in the list.\n#		If no id was specified then the login home page id or \n#		the current document id will be used\n#\n#	&logouthomeid 	- (Optional)\n#		document id to load when user logs out	\n#\n#	&pwdreqid 	- (Optional)\n#		document id to load after the user has submited\n#		a request for a new password\n#\n#	&pwdactid 	- (Optional)\n#		document id to load when the after the user has activated\n#		their new password\n#\n#	&logintext		- (Optional) \n#		Text to be displayed inside login button (for built-in form)\n#\n#	&logouttext 	- (Optional)\n#		Text to be displayed inside logout link (for built-in form)\n#	\n#	&tpl			- (Optional)\n#		Chunk name or document id to as a template\n#				  \n#	Note: Templats design:\n#			section 1: login template\n#			section 2: logout template \n#			section 3: password reminder template \n#\n#			See weblogin.tpl for more information\n#\n# Examples:\n#\n#	[[WebLogin? &loginhomeid=`8` &logouthomeid=`1`]] \n#\n#	[[WebLogin? &loginhomeid=`8,18,7,5` &tpl=`Login`]] \n\n# Set Snippet Paths \n$snipPath = $modx->config[''base_path''] . "assets/snippets/";\n\n# check if inside manager\nif ($m = $modx->isBackend()) {\n	return ''''; # don''t go any further when inside manager\n}\n\n# deprecated params - only for backward compatibility\nif(isset($loginid)) $loginhomeid=$loginid;\nif(isset($logoutid)) $logouthomeid = $logoutid;\nif(isset($template)) $tpl = $template;\n\n# Snippet customize settings\n$liHomeId	= isset($loginhomeid)? array_filter(array_map(''intval'', explode('','', $loginhomeid))):array($modx->config[''login_home''],$modx->documentIdentifier);\n$loHomeId	= isset($logouthomeid)? $logouthomeid:$modx->documentIdentifier;\n$pwdReqId	= isset($pwdreqid)? $pwdreqid:0;\n$pwdActId	= isset($pwdactid)? $pwdactid:0;\n$loginText	= isset($logintext)? $logintext:''Login'';\n$logoutText	= isset($logouttext)? $logouttext:''Logout'';\n$tpl		= isset($tpl)? $tpl:"";\n\n# System settings\n$webLoginMode = isset($_REQUEST[''webloginmode''])? $_REQUEST[''webloginmode'']: '''';\n$isLogOut		= $webLoginMode==''lo'' ? 1:0;\n$isPWDActivate	= $webLoginMode==''actp'' ? 1:0;\n$isPostBack		= count($_POST) && (isset($_POST[''cmdweblogin'']) || isset($_POST[''cmdweblogin_x'']));\n$txtPwdRem 		= isset($_REQUEST[''txtpwdrem''])? $_REQUEST[''txtpwdrem'']: 0;\n$isPWDReminder	= $isPostBack && $txtPwdRem==''1'' ? 1:0;\n\n$site_id = isset($site_id)? $site_id: '''';\n$cookieKey = substr(md5($site_id."Web-User"),0,15);\n\n# Start processing\ninclude_once $snipPath."weblogin/weblogin.common.inc.php";\ninclude_once ($modx->config[''site_manager_path''] . "includes/crypt.class.inc.php");\n\nif ($isPWDActivate || $isPWDReminder || $isLogOut || $isPostBack) {\n	# include the logger class\n	include_once $modx->config[''site_manager_path''] . "includes/log.class.inc.php";\n	include_once $snipPath."weblogin/weblogin.processor.inc.php";\n}\n\ninclude_once $snipPath."weblogin/weblogin.inc.php";\n\n# Return\nreturn $output;\n', 0, '&loginhomeid=Login Home Id;string; &logouthomeid=Logout Home Id;string; &logintext=Login Button Text;string; &logouttext=Logout Button Text;string; &tpl=Template;string;', ''),
(16, 'Wayfinder', '<strong>2.0.4</strong> Completely template-driven and highly flexible menu builder', 0, 6, 0, 'return require MODX_BASE_PATH.''assets/snippets/wayfinder/snippet.wayfinder.php'';\n', 0, '', ''),
(17, 'load_quest', '', 0, 10, 0, '\r\ninclude_once (MODX_BASE_PATH.''assets/modules/test_folder/test_content.class.php'');\r\n\r\n$work = new test_content($modx);\r\n\r\necho($work->make_quest(intval($id)));\r\n', 0, '', ' '),
(18, 'chek_form', '', 0, 10, 0, '\r\ninclude_once (MODX_BASE_PATH.''assets/modules/test_folder/test_content.class.php'');\r\n\r\n$work = new test_content($modx);\r\n\r\necho($work->chek_form());\r\n', 0, '', ' '),
(19, 'find_worker', '', 0, 10, 0, '\r\n$result = $modx->db->query(''SELECT `id` FROM `modx_site_content` WHERE `parent`=''.$id.'' AND `template` = 7'');\r\n$arResult = $modx->db->makeArray($result);\r\nreturn $arResult[0][''id''];\r\n', 0, '', ' '),
(20, 'find_result_page', '', 0, 10, 0, '\r\n$result = $modx->db->query(''SELECT `id` FROM `modx_site_content` WHERE `parent`=''.$id.'' AND `template` = 6'');\r\n$arResult = $modx->db->makeArray($result);\r\nreturn $arResult[0][''id''];\r\n', 0, '', ' '),
(21, 'load_user_answer', '', 0, 10, 0, '\r\ninclude_once (MODX_BASE_PATH.''assets/modules/test_folder/test_content.class.php'');\r\n\r\n$work = new test_content($modx);\r\n\r\necho($work->load_user_answer($_GET));\r\n', 0, '', ' '),
(22, 'result_form', '', 0, 10, 0, '\r\ninclude_once (MODX_BASE_PATH.''assets/modules/test_folder/test_content.class.php'');\r\n\r\n$work = new test_content($modx);\r\n\r\necho($work->result_form());\r\n', 0, '', ' ');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_templates`
--

CREATE TABLE IF NOT EXISTS `modx_site_templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `templatename` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'url to icon file',
  `template_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-page,1-content',
  `content` mediumtext COLLATE utf8_unicode_ci,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains the site templates.' AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `modx_site_templates`
--

INSERT INTO `modx_site_templates` (`id`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`) VALUES
(3, 'Minimal Template', 'Default minimal empty template (content returned only)', 0, 0, '', 0, '[*content*]', 0),
(4, 'MODxHost', '<strong>1.0</strong> Legacy MODX Host template including dropdown menu', 0, 1, '', 0, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n\n<head>\n  <title>[(site_name)] | [*pagetitle*]</title>\n  <meta http-equiv="Content-Type" content="text/html; charset=[(modx_charset)]" />\n  <base href="[(site_url)]"></base>\n  <link rel="stylesheet" href="assets/templates/modxhost/layout.css" type="text/css" media="screen" />\n  <link rel="stylesheet" href="assets/templates/modxhost/modxmenu.css" type="text/css" media="screen" />\n  <link rel="stylesheet" href="assets/templates/modxhost/form.css" type="text/css" media="screen" />\n  <link rel="stylesheet" href="assets/templates/modxhost/modx.css" type="text/css" media="screen" />\n  <link rel="stylesheet" href="assets/templates/modxhost/print.css" type="text/css" media="print" />\n  <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="[(site_url)][~11~]" />\n  <script src="[(site_manager_url)]media/script/mootools/mootools.js" type="text/javascript"></script>\n  <script src="assets/templates/modxhost/drop_down_menu.js" type="text/javascript"></script>\n</head>\n<body>\n<div id="wrapper">\n  <div id="minHeight"></div>\n  <div id="outer">\n    <div id="inner">\n      <div id="right">\n        <div id="right-inner">\n          <h1 style="text-indent: -5000px;padding: 0px; margin:0px; font-size: 1px;">[(site_name)]</h1>\n          <div id="sidebar">\n            <h2>News:</h2>\n            [[Ditto? &parents=`2` &display=`2` &total=`20` &removeChunk=`Comments` &tpl=`nl_sidebar`]]\n            <div id="recentdocsctnr">\n              <h2>Most Recent:</h2>\n				<a name="recentdocs"></a><ul class="LIn_shortMode">[[Ditto?parents=0&display=5&tpl=''@CODE:<li><a href="[+url+]" title="[+pagetitle+]">[+pagetitle+]</a> <span class="LIn_date">[+date+]</span> <span class="LIn_desc"></span></li>'']]</ul> </div>\n            <h2>Login:</h2>\n            <div id="sidebarlogin">[!WebLogin? &tpl=`WebLoginSideBar` &loginhomeid=`[(site_start)]`!]</div>\n            <h2>Meta:</h2>\n            <p><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></p>\n            <p><a href="http://jigsaw.w3.org/css-validator/check/referer" title="This page uses valid Cascading Stylesheets" rel="external">Valid <abbr title="W3C Cascading Stylesheets">css</abbr></a></p>\n            <p><a href="http://modx.com" title="Ajax CMS and PHP Application Framework">MODX</a></p>\n          </div>\n          <!-- close #sidebar -->\n        </div>\n        <!-- end right inner-->\n      </div>\n      <!-- end right -->\n      <div id="left">\n        <div id="left-inner">\n          [[Breadcrumbs?]]\n          <div id="content">\n            <div class="post">\n              <h2>[*longtitle*]</h2>\n              [*#content*] </div>\n            <!-- close .post (main column content) -->\n          </div>\n          <!-- close #content -->\n        </div>\n        <!-- end left-inner -->\n      </div>\n      <!-- end left -->\n    </div>\n    <!-- end inner -->\n    <div id="clearfooter"></div>\n    <div id="header">\n      <h1><a id="logo" href="[~[(site_start)]~]" title="[(site_name)]">[(site_name)]</a></h1>\n\n      <div id="search"><!--search_terms--><span id="search-txt">SEARCH</span><a name="search"></a>[!AjaxSearch? &ajaxSearch=`1` &landingPage=`8` &moreResultsPage=`8` &addJscript=`0` &showIntro=`0` &ajaxMax=`5` &extract=`1`!]</div>\n      <div id="ajaxmenu"> [[Wayfinder?startId=`0` &outerTpl=`mh.OuterTpl` &innerTpl=`mh.InnerTpl` &rowTpl=`mh.RowTpl` &innerRowTpl=`mh.InnerRowTpl` &firstClass=`first` &hereClass=``]] </div>\n      <!-- end topmenu -->\n    </div>\n    <!-- end header -->\n    <br style="clear:both;height:0;font-size: 1px" />\n    <div id="footer">\n      <p> <a href="http://modx.com" title="Ajax CMS and PHP Application Framework">Powered\n          by MODX</a> &nbsp;<a href="http://modx.com/" title="Template Designed by modXhost.com">Template &copy; 2006-2011\n          modXhost.com</a><br />\n        Memory: [^m^], MySQL: [^qt^], [^q^] request(s), PHP: [^p^], total: [^t^], document retrieved\n        from [^s^]. </p>\n    </div>\n    <!-- end footer -->\n  </div>\n  <!-- end outer div -->\n</div>\n<!-- end wrapper -->\n</body>\n</html>', 0),
(5, 'test_1_form', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html>\r\n<head lang="ru">\r\n    <meta charset="UTF-8">\r\n    <title>Вопрос 2</title>\r\n	<meta name="viewport" content="width=660">\r\n	<base href="[(site_url)]">\r\n    <link rel="stylesheet" href="../css/styles.css">\r\n    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>\r\n    <script src="../js/all.js"></script>\r\n</head>\r\n<body>\r\n<div class="wrap">\r\n    <header>\r\n        <!--<span class="logo">Beauty matrix</span>-->\r\n        <h1>Тест: правильно ли я питаюсь?</h1>\r\n    </header>\r\n    <div class="content">\r\n		[!load_quest? &id=`[*id*]`!]\r\n        \r\n        \r\n        <section class="content-footer">\r\n                <a class="next" href="#">Следующий вопрос<span class="arrow"></span></a>\r\n                <br>\r\n                <div class="progress">\r\n                    <p class="pr">2/10</p>\r\n                </div>\r\n                <br>\r\n            </section>\r\n\r\n        <section class="form">\r\n            <h2>Укажите e-mail, на который <br> высылать ваши результаты:</h2>\r\n            <form class="results" id="result-form" action="[~[!find_worker? &id=`[*id*]`!]~]" method="post">\r\n				<input type="hidden" name="test_id" value="[*id*]">\r\n				<input type="hidden" name="pid" value="[!find_result_page? &id=`[*id*]`!]">\r\n                <input name="name" type="text" maxlength="40" placeholder="Ваше имя" value="" required>\r\n                <input name="email" type="email" maxlength="50" placeholder="Введите e-mail" value="" required>\r\n                <input id="submit" type="submit" name="submit" value="ПОЛУЧИТЬ РЕЗУЛЬТАТЫ">\r\n            </form>\r\n        </section>\r\n    </div><!--content-->\r\n</div><!--wrap-->\r\n</body>\r\n</html>', 0),
(6, 'test_1_answer', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html>\r\n<head lang="ru">\r\n    <meta charset="UTF-8">\r\n    <title>Ответы</title>\r\n	<meta name="viewport" content="width=660">\r\n    <link rel="stylesheet" href="css/styles.css">\r\n</head>\r\n<body>\r\n<div class="wrap">\r\n    <header>\r\n        <!--<span class="logo">Beauty matrix</span>-->\r\n        <h1>Тест: правильно ли я питаюсь?</h1>\r\n    </header>\r\n    <div class="content answer">\r\n		\r\n        <section class="content-header-text">\r\n            <p>Все мы знаем, что для хорошего здоровья нам необходимо правильно питаться. Однако универсального\r\n                продукта, подходящего всем без исключения, не существует. Но существуют правила, соблюдая которые,\r\n                человек закладывает основу правильного питания без особых финансовых затрат. А вы уже на пути к\r\n                здоровому питанию или еще не контролируете свой рацион? Предлагаем ответить на простейшие вопросы в\r\n                тесте и определить правильно ли вы питаетесь</p>\r\n        </section>\r\n		[!load_user_answer!]\r\n		<a href="http://beauty-matrix.ru/" class="link_to_main">Узнать продробнее о программе</a>\r\n        <section class="content-footer"></section>\r\n    </div><!--content-->\r\n</div><!--wrap-->\r\n</body>\r\n</html>', 0),
(7, 'test_form_work', '', 0, 0, '', 0, '[!chek_form!]', 0),
(8, 'test_form_result', '', 0, 0, '', 0, '<!DOCTYPE html>\r\n<html>\r\n<head lang="ru">\r\n    <meta charset="UTF-8">\r\n    <title>Результат отправлен вам на email!</title>\r\n	<meta name="viewport" content="width=660">\r\n	<base href="[(site_url)]">\r\n    <link rel="stylesheet" href="../css/styles.css">\r\n    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>\r\n    <script src="../js/all.js"></script>\r\n</head>\r\n<body>\r\n<div class="wrap">\r\n    <header>\r\n        <!--<span class="logo">Beauty matrix</span>-->\r\n        <h1>Тест: правильно ли я питаюсь?</h1>\r\n    </header>\r\n    <div class="content">\r\n		<div style="padding-top: 60px; padding-left: 20px; height: 70px;">\r\n			[!result_form!]\r\n		</div>\r\n    </div><!--content-->\r\n</div><!--wrap-->\r\n</body>\r\n</html>', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvars`
--

CREATE TABLE IF NOT EXISTS `modx_site_tmplvars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `caption` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `elements` text COLLATE utf8_unicode_ci,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Display Control',
  `display_params` text COLLATE utf8_unicode_ci COMMENT 'Display Control Properties',
  `default_text` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `indx_rank` (`rank`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Template Variables' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `modx_site_tmplvars`
--

INSERT INTO `modx_site_tmplvars` (`id`, `type`, `name`, `caption`, `description`, `editor_type`, `category`, `locked`, `elements`, `rank`, `display`, `display_params`, `default_text`) VALUES
(1, 'text', 'documentTags', 'Tags', 'Space delimited tags for the current document', 0, 1, 0, '', 0, '', '', ''),
(2, 'richtext', 'blogContent', 'blogContent', 'RTE for the new blog entries', 0, 1, 0, '', 0, 'RichText', '&w=383px&h=450px&edt=TinyMCE', ''),
(3, 'text', 'loginName', 'loginName', 'Conditional name for the Login menu item', 0, 1, 0, '', 0, '', '', '@EVAL if ($modx->getLoginUserID()) return ''Logout''; else return ''Login'';');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_access`
--

CREATE TABLE IF NOT EXISTS `modx_site_tmplvar_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for template variable access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_contentvalues`
--

CREATE TABLE IF NOT EXISTS `modx_site_tmplvar_contentvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `contentid` int(10) NOT NULL DEFAULT '0' COMMENT 'Site Content Id',
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_tmplvarid` (`tmplvarid`),
  KEY `idx_id` (`contentid`),
  FULLTEXT KEY `value_ft_idx` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Template Variables Content Values Link Table' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_templates`
--

CREATE TABLE IF NOT EXISTS `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmplvarid`,`templateid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Site Template Variables Templates Link Table';

--
-- Дамп данных таблицы `modx_site_tmplvar_templates`
--

INSERT INTO `modx_site_tmplvar_templates` (`tmplvarid`, `templateid`, `rank`) VALUES
(1, 4, 0),
(2, 4, 0),
(3, 4, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_system_eventnames`
--

CREATE TABLE IF NOT EXISTS `modx_system_eventnames` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `service` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'System Service number',
  `groupname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='System Event Names.' AUTO_INCREMENT=1001 ;

--
-- Дамп данных таблицы `modx_system_eventnames`
--

INSERT INTO `modx_system_eventnames` (`id`, `name`, `service`, `groupname`) VALUES
(1, 'OnDocPublished', 5, ''),
(2, 'OnDocUnPublished', 5, ''),
(3, 'OnWebPagePrerender', 5, ''),
(4, 'OnWebLogin', 3, ''),
(5, 'OnBeforeWebLogout', 3, ''),
(6, 'OnWebLogout', 3, ''),
(7, 'OnWebSaveUser', 3, ''),
(8, 'OnWebDeleteUser', 3, ''),
(9, 'OnWebChangePassword', 3, ''),
(10, 'OnWebCreateGroup', 3, ''),
(11, 'OnManagerLogin', 2, ''),
(12, 'OnBeforeManagerLogout', 2, ''),
(13, 'OnManagerLogout', 2, ''),
(14, 'OnManagerSaveUser', 2, ''),
(15, 'OnManagerDeleteUser', 2, ''),
(16, 'OnManagerChangePassword', 2, ''),
(17, 'OnManagerCreateGroup', 2, ''),
(18, 'OnBeforeCacheUpdate', 4, ''),
(19, 'OnCacheUpdate', 4, ''),
(20, 'OnLoadWebPageCache', 4, ''),
(21, 'OnBeforeSaveWebPageCache', 4, ''),
(22, 'OnChunkFormPrerender', 1, 'Chunks'),
(23, 'OnChunkFormRender', 1, 'Chunks'),
(24, 'OnBeforeChunkFormSave', 1, 'Chunks'),
(25, 'OnChunkFormSave', 1, 'Chunks'),
(26, 'OnBeforeChunkFormDelete', 1, 'Chunks'),
(27, 'OnChunkFormDelete', 1, 'Chunks'),
(28, 'OnDocFormPrerender', 1, 'Documents'),
(29, 'OnDocFormRender', 1, 'Documents'),
(30, 'OnBeforeDocFormSave', 1, 'Documents'),
(31, 'OnDocFormSave', 1, 'Documents'),
(32, 'OnBeforeDocFormDelete', 1, 'Documents'),
(33, 'OnDocFormDelete', 1, 'Documents'),
(34, 'OnPluginFormPrerender', 1, 'Plugins'),
(35, 'OnPluginFormRender', 1, 'Plugins'),
(36, 'OnBeforePluginFormSave', 1, 'Plugins'),
(37, 'OnPluginFormSave', 1, 'Plugins'),
(38, 'OnBeforePluginFormDelete', 1, 'Plugins'),
(39, 'OnPluginFormDelete', 1, 'Plugins'),
(40, 'OnSnipFormPrerender', 1, 'Snippets'),
(41, 'OnSnipFormRender', 1, 'Snippets'),
(42, 'OnBeforeSnipFormSave', 1, 'Snippets'),
(43, 'OnSnipFormSave', 1, 'Snippets'),
(44, 'OnBeforeSnipFormDelete', 1, 'Snippets'),
(45, 'OnSnipFormDelete', 1, 'Snippets'),
(46, 'OnTempFormPrerender', 1, 'Templates'),
(47, 'OnTempFormRender', 1, 'Templates'),
(48, 'OnBeforeTempFormSave', 1, 'Templates'),
(49, 'OnTempFormSave', 1, 'Templates'),
(50, 'OnBeforeTempFormDelete', 1, 'Templates'),
(51, 'OnTempFormDelete', 1, 'Templates'),
(52, 'OnTVFormPrerender', 1, 'Template Variables'),
(53, 'OnTVFormRender', 1, 'Template Variables'),
(54, 'OnBeforeTVFormSave', 1, 'Template Variables'),
(55, 'OnTVFormSave', 1, 'Template Variables'),
(56, 'OnBeforeTVFormDelete', 1, 'Template Variables'),
(57, 'OnTVFormDelete', 1, 'Template Variables'),
(58, 'OnUserFormPrerender', 1, 'Users'),
(59, 'OnUserFormRender', 1, 'Users'),
(60, 'OnBeforeUserFormSave', 1, 'Users'),
(61, 'OnUserFormSave', 1, 'Users'),
(62, 'OnBeforeUserFormDelete', 1, 'Users'),
(63, 'OnUserFormDelete', 1, 'Users'),
(64, 'OnWUsrFormPrerender', 1, 'Web Users'),
(65, 'OnWUsrFormRender', 1, 'Web Users'),
(66, 'OnBeforeWUsrFormSave', 1, 'Web Users'),
(67, 'OnWUsrFormSave', 1, 'Web Users'),
(68, 'OnBeforeWUsrFormDelete', 1, 'Web Users'),
(69, 'OnWUsrFormDelete', 1, 'Web Users'),
(70, 'OnSiteRefresh', 1, ''),
(71, 'OnFileManagerUpload', 1, ''),
(72, 'OnModFormPrerender', 1, 'Modules'),
(73, 'OnModFormRender', 1, 'Modules'),
(74, 'OnBeforeModFormDelete', 1, 'Modules'),
(75, 'OnModFormDelete', 1, 'Modules'),
(76, 'OnBeforeModFormSave', 1, 'Modules'),
(77, 'OnModFormSave', 1, 'Modules'),
(78, 'OnBeforeWebLogin', 3, ''),
(79, 'OnWebAuthentication', 3, ''),
(80, 'OnBeforeManagerLogin', 2, ''),
(81, 'OnManagerAuthentication', 2, ''),
(82, 'OnSiteSettingsRender', 1, 'System Settings'),
(83, 'OnFriendlyURLSettingsRender', 1, 'System Settings'),
(84, 'OnUserSettingsRender', 1, 'System Settings'),
(85, 'OnInterfaceSettingsRender', 1, 'System Settings'),
(86, 'OnMiscSettingsRender', 1, 'System Settings'),
(87, 'OnRichTextEditorRegister', 1, 'RichText Editor'),
(88, 'OnRichTextEditorInit', 1, 'RichText Editor'),
(89, 'OnManagerPageInit', 2, ''),
(90, 'OnWebPageInit', 5, ''),
(101, 'OnLoadDocumentObject', 5, ''),
(91, 'OnLoadWebDocument', 5, ''),
(92, 'OnParseDocument', 5, ''),
(93, 'OnManagerLoginFormRender', 2, ''),
(94, 'OnWebPageComplete', 5, ''),
(95, 'OnLogPageHit', 5, ''),
(96, 'OnBeforeManagerPageInit', 2, ''),
(97, 'OnBeforeEmptyTrash', 1, 'Documents'),
(98, 'OnEmptyTrash', 1, 'Documents'),
(99, 'OnManagerLoginFormPrerender', 2, ''),
(100, 'OnStripAlias', 1, 'Documents'),
(200, 'OnCreateDocGroup', 1, 'Documents'),
(201, 'OnManagerWelcomePrerender', 2, ''),
(202, 'OnManagerWelcomeHome', 2, ''),
(203, 'OnManagerWelcomeRender', 2, ''),
(204, 'OnBeforeDocDuplicate', 1, 'Documents'),
(205, 'OnDocDuplicate', 1, 'Documents'),
(206, 'OnManagerMainFrameHeaderHTMLBlock', 2, ''),
(207, 'OnManagerPreFrameLoader', 2, ''),
(208, 'OnManagerFrameLoader', 2, ''),
(209, 'OnManagerTreeInit', 2, ''),
(210, 'OnManagerTreePrerender', 2, ''),
(211, 'OnManagerTreeRender', 2, ''),
(212, 'OnManagerNodePrerender', 2, ''),
(213, 'OnManagerNodeRender', 2, ''),
(999, 'OnPageUnauthorized', 1, ''),
(1000, 'OnPageNotFound', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_system_settings`
--

CREATE TABLE IF NOT EXISTS `modx_system_settings` (
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `setting_value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains Content Manager settings.';

--
-- Дамп данных таблицы `modx_system_settings`
--

INSERT INTO `modx_system_settings` (`setting_name`, `setting_value`) VALUES
('manager_theme', 'MODxRE'),
('settings_version', '1.0.15'),
('show_meta', '0'),
('server_offset_time', '0'),
('server_protocol', 'http'),
('manager_language', 'russian-UTF8'),
('modx_charset', 'UTF-8'),
('site_name', 'My MODX Site'),
('site_start', '1'),
('error_page', '1'),
('unauthorized_page', '1'),
('site_status', '1'),
('site_unavailable_message', 'The site is currently unavailable'),
('track_visitors', '0'),
('top_howmany', '10'),
('auto_template_logic', 'parent'),
('default_template', '3'),
('old_template', '3'),
('publish_default', '0'),
('cache_default', '1'),
('search_default', '1'),
('friendly_urls', '1'),
('friendly_url_prefix', ''),
('friendly_url_suffix', ''),
('friendly_alias_urls', '1'),
('use_alias_path', '0'),
('use_udperms', '1'),
('udperms_allowroot', '0'),
('failed_login_attempts', '3'),
('blocked_minutes', '60'),
('use_captcha', '0'),
('captcha_words', 'MODX,Access,Better,BitCode,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Tattoo,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote'),
('emailsender', 'anseroot@gmail.com'),
('email_method', 'mail'),
('smtp_auth', '0'),
('smtp_host', ''),
('smtp_port', '25'),
('smtp_username', ''),
('emailsubject', 'Your login details'),
('number_of_logs', '100'),
('number_of_messages', '30'),
('number_of_results', '20'),
('use_editor', '1'),
('use_browser', '1'),
('rb_base_dir', '/var/www/test_beauty/www/assets/'),
('rb_base_url', 'assets/'),
('which_editor', 'TinyMCE'),
('fe_editor_lang', 'russian-UTF8'),
('fck_editor_toolbar', 'standard'),
('fck_editor_autolang', '0'),
('editor_css_path', ''),
('editor_css_selectors', ''),
('strip_image_paths', '1'),
('upload_images', 'bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff'),
('upload_media', 'au,avi,mp3,mp4,mpeg,mpg,wav,wmv'),
('upload_flash', 'fla,flv,swf'),
('upload_files', 'php,otf,eot,ttf,woff,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip'),
('upload_maxsize', '1048576'),
('new_file_permissions', '0644'),
('new_folder_permissions', '0755'),
('filemanager_path', '/var/www/test_beauty/www/'),
('theme_refresher', ''),
('manager_layout', '4'),
('custom_contenttype', 'application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json'),
('auto_menuindex', '1'),
('session.cookie.lifetime', '604800'),
('mail_check_timeperiod', '60'),
('manager_direction', 'ltr'),
('tinymce_editor_theme', 'editor'),
('tinymce_custom_plugins', 'style,advimage,advlink,searchreplace,print,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras,visualchars,media'),
('tinymce_custom_buttons1', 'undo,redo,selectall,separator,pastetext,pasteword,separator,search,replace,separator,nonbreaking,hr,charmap,separator,image,link,unlink,anchor,media,separator,cleanup,removeformat,separator,fullscreen,print,code,help'),
('tinymce_custom_buttons2', 'bold,italic,underline,strikethrough,sub,sup,separator,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,separator,styleprops'),
('tree_show_protected', '0'),
('rss_url_news', 'http://feeds.feedburner.com/modx-announce'),
('rss_url_security', 'http://feeds.feedburner.com/modxsecurity'),
('validate_referer', '1'),
('datepicker_offset', '-10'),
('xhtml_urls', '1'),
('allow_duplicate_alias', '0'),
('automatic_alias', '1'),
('datetime_format', 'dd-mm-YYYY'),
('warning_visibility', '1'),
('remember_last_tab', '0'),
('enable_bindings', '1'),
('seostrict', '1'),
('cache_type', '1'),
('maxImageWidth', '1600'),
('maxImageHeight', '1200'),
('thumbWidth', '150'),
('thumbHeight', '150'),
('thumbsDir', '.thumbs'),
('jpegQuality', '90'),
('denyZipDownload', '0'),
('denyExtensionRename', '0'),
('showHiddenFiles', '0'),
('docid_incrmnt_method', '0'),
('make_folders', '0'),
('site_id', '568fe5a337b4a'),
('site_unavailable_page', ''),
('reload_site_unavailable', ''),
('siteunavailable_message_default', 'В настоящее время сайт недоступен.'),
('check_files_onlogin', 'index.php\r\n.htaccess\r\nmanager/index.php\r\nmanager/includes/config.inc.php'),
('error_reporting', '1'),
('send_errormail', '0'),
('pwd_hash_algo', 'UNCRYPT'),
('reload_captcha_words', ''),
('captcha_words_default', 'MODX,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote'),
('smtp_secure', 'none'),
('reload_emailsubject', ''),
('emailsubject_default', 'Данные для авторизации'),
('reload_signupemail_message', ''),
('signupemail_message', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('system_email_signup_default', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('reload_websignupemail_message', ''),
('websignupemail_message', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('system_email_websignup_default', 'Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация'),
('reload_system_email_webreminder_message', ''),
('webpwdreminder_message', 'Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация'),
('system_email_webreminder_default', 'Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация'),
('tree_page_click', '27'),
('resource_tree_node_name', 'pagetitle'),
('mce_editor_skin', 'default'),
('mce_template_docs', ''),
('mce_template_chunks', ''),
('mce_entermode', 'p'),
('mce_element_format', 'xhtml'),
('mce_schema', 'html5'),
('tinymce_custom_buttons3', ''),
('tinymce_custom_buttons4', ''),
('tinymce_css_selectors', 'left=justifyleft;right=justifyright'),
('rb_webuser', '0'),
('clean_uploaded_filename', '0'),
('sys_files_checksum', 'a:4:{s:34:"/var/www/test_beauty/www/index.php";s:32:"c6f73908b7b0a58acfe95b0f844d134e";s:34:"/var/www/test_beauty/www/.htaccess";s:32:"61a3be34923238e6cfe23b963fb82015";s:42:"/var/www/test_beauty/www/manager/index.php";s:32:"30df65e2d71987b65a4258e318c21aaf";s:56:"/var/www/test_beauty/www/manager/includes/config.inc.php";s:32:"090831e50bbfe2322dacba24e34e539a";}');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_attributes`
--

CREATE TABLE IF NOT EXISTS `modx_user_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobilephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains information about the backend users.' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `modx_user_attributes`
--

INSERT INTO `modx_user_attributes` (`id`, `internalKey`, `fullname`, `role`, `email`, `phone`, `mobilephone`, `blocked`, `blockeduntil`, `blockedafter`, `logincount`, `lastlogin`, `thislogin`, `failedlogincount`, `sessionid`, `dob`, `gender`, `country`, `street`, `city`, `state`, `zip`, `fax`, `photo`, `comment`) VALUES
(1, 1, 'Default admin account', 1, 'anseroot@gmail.com', '', '', 0, 0, 0, 15, 1452864825, 1453104417, 0, 'cv1v6059838vve08h5o864sn94', 0, 0, '', '', '', '', '', '', '', ''),
(2, 2, '', 1, 'd.smirnov@specialview.ru', '', '', 0, 0, 0, 5, 1452944484, 1452953013, 0, '4shnao3pbn9shp11iph9mipcs6', 0, 0, '', '', '', '', '', '', '', ''),
(3, 3, '', 1, 'ersul@list.ru', '', '', 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_messages`
--

CREATE TABLE IF NOT EXISTS `modx_user_messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `postdate` int(20) NOT NULL DEFAULT '0',
  `messageread` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains messages for the Content Manager messaging system.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_roles`
--

CREATE TABLE IF NOT EXISTS `modx_user_roles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `frames` int(1) NOT NULL DEFAULT '0',
  `home` int(1) NOT NULL DEFAULT '0',
  `view_document` int(1) NOT NULL DEFAULT '0',
  `new_document` int(1) NOT NULL DEFAULT '0',
  `save_document` int(1) NOT NULL DEFAULT '0',
  `publish_document` int(1) NOT NULL DEFAULT '0',
  `delete_document` int(1) NOT NULL DEFAULT '0',
  `empty_trash` int(1) NOT NULL DEFAULT '0',
  `action_ok` int(1) NOT NULL DEFAULT '0',
  `logout` int(1) NOT NULL DEFAULT '0',
  `help` int(1) NOT NULL DEFAULT '0',
  `messages` int(1) NOT NULL DEFAULT '0',
  `new_user` int(1) NOT NULL DEFAULT '0',
  `edit_user` int(1) NOT NULL DEFAULT '0',
  `logs` int(1) NOT NULL DEFAULT '0',
  `edit_parser` int(1) NOT NULL DEFAULT '0',
  `save_parser` int(1) NOT NULL DEFAULT '0',
  `edit_template` int(1) NOT NULL DEFAULT '0',
  `settings` int(1) NOT NULL DEFAULT '0',
  `credits` int(1) NOT NULL DEFAULT '0',
  `new_template` int(1) NOT NULL DEFAULT '0',
  `save_template` int(1) NOT NULL DEFAULT '0',
  `delete_template` int(1) NOT NULL DEFAULT '0',
  `edit_snippet` int(1) NOT NULL DEFAULT '0',
  `new_snippet` int(1) NOT NULL DEFAULT '0',
  `save_snippet` int(1) NOT NULL DEFAULT '0',
  `delete_snippet` int(1) NOT NULL DEFAULT '0',
  `edit_chunk` int(1) NOT NULL DEFAULT '0',
  `new_chunk` int(1) NOT NULL DEFAULT '0',
  `save_chunk` int(1) NOT NULL DEFAULT '0',
  `delete_chunk` int(1) NOT NULL DEFAULT '0',
  `empty_cache` int(1) NOT NULL DEFAULT '0',
  `edit_document` int(1) NOT NULL DEFAULT '0',
  `change_password` int(1) NOT NULL DEFAULT '0',
  `error_dialog` int(1) NOT NULL DEFAULT '0',
  `about` int(1) NOT NULL DEFAULT '0',
  `file_manager` int(1) NOT NULL DEFAULT '0',
  `save_user` int(1) NOT NULL DEFAULT '0',
  `delete_user` int(1) NOT NULL DEFAULT '0',
  `save_password` int(11) NOT NULL DEFAULT '0',
  `edit_role` int(1) NOT NULL DEFAULT '0',
  `save_role` int(1) NOT NULL DEFAULT '0',
  `delete_role` int(1) NOT NULL DEFAULT '0',
  `new_role` int(1) NOT NULL DEFAULT '0',
  `access_permissions` int(1) NOT NULL DEFAULT '0',
  `bk_manager` int(1) NOT NULL DEFAULT '0',
  `new_plugin` int(1) NOT NULL DEFAULT '0',
  `edit_plugin` int(1) NOT NULL DEFAULT '0',
  `save_plugin` int(1) NOT NULL DEFAULT '0',
  `delete_plugin` int(1) NOT NULL DEFAULT '0',
  `new_module` int(1) NOT NULL DEFAULT '0',
  `edit_module` int(1) NOT NULL DEFAULT '0',
  `save_module` int(1) NOT NULL DEFAULT '0',
  `delete_module` int(1) NOT NULL DEFAULT '0',
  `exec_module` int(1) NOT NULL DEFAULT '0',
  `view_eventlog` int(1) NOT NULL DEFAULT '0',
  `delete_eventlog` int(1) NOT NULL DEFAULT '0',
  `manage_metatags` int(1) NOT NULL DEFAULT '0' COMMENT 'manage site meta tags and keywords',
  `edit_doc_metatags` int(1) NOT NULL DEFAULT '0' COMMENT 'edit document meta tags and keywords',
  `new_web_user` int(1) NOT NULL DEFAULT '0',
  `edit_web_user` int(1) NOT NULL DEFAULT '0',
  `save_web_user` int(1) NOT NULL DEFAULT '0',
  `delete_web_user` int(1) NOT NULL DEFAULT '0',
  `web_access_permissions` int(1) NOT NULL DEFAULT '0',
  `view_unpublished` int(1) NOT NULL DEFAULT '0',
  `import_static` int(1) NOT NULL DEFAULT '0',
  `export_static` int(1) NOT NULL DEFAULT '0',
  `remove_locks` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains information describing the user roles.' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `modx_user_roles`
--

INSERT INTO `modx_user_roles` (`id`, `name`, `description`, `frames`, `home`, `view_document`, `new_document`, `save_document`, `publish_document`, `delete_document`, `empty_trash`, `action_ok`, `logout`, `help`, `messages`, `new_user`, `edit_user`, `logs`, `edit_parser`, `save_parser`, `edit_template`, `settings`, `credits`, `new_template`, `save_template`, `delete_template`, `edit_snippet`, `new_snippet`, `save_snippet`, `delete_snippet`, `edit_chunk`, `new_chunk`, `save_chunk`, `delete_chunk`, `empty_cache`, `edit_document`, `change_password`, `error_dialog`, `about`, `file_manager`, `save_user`, `delete_user`, `save_password`, `edit_role`, `save_role`, `delete_role`, `new_role`, `access_permissions`, `bk_manager`, `new_plugin`, `edit_plugin`, `save_plugin`, `delete_plugin`, `new_module`, `edit_module`, `save_module`, `delete_module`, `exec_module`, `view_eventlog`, `delete_eventlog`, `manage_metatags`, `edit_doc_metatags`, `new_web_user`, `edit_web_user`, `save_web_user`, `delete_web_user`, `web_access_permissions`, `view_unpublished`, `import_static`, `export_static`, `remove_locks`) VALUES
(2, 'Editor', 'Limited to managing content', 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1),
(3, 'Publisher', 'Editor with expanded permissions including manage users, update Elements and site settings', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1),
(1, 'Administrator', 'Site administrators have full access to all functions', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_settings`
--

CREATE TABLE IF NOT EXISTS `modx_user_settings` (
  `user` int(11) NOT NULL,
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `setting_value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`user`,`setting_name`),
  KEY `setting_name` (`setting_name`),
  KEY `user` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains backend user settings.';

--
-- Дамп данных таблицы `modx_user_settings`
--

INSERT INTO `modx_user_settings` (`user`, `setting_name`, `setting_value`) VALUES
(2, 'allow_manager_access', '1'),
(3, 'allow_manager_access', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_webgroup_access`
--

CREATE TABLE IF NOT EXISTS `modx_webgroup_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for web access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_webgroup_names`
--

CREATE TABLE IF NOT EXISTS `modx_webgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for web access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_groups`
--

CREATE TABLE IF NOT EXISTS `modx_web_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `webuser` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_group_user` (`webgroup`,`webuser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains data used for web access permissions.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_users`
--

CREATE TABLE IF NOT EXISTS `modx_web_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cachepwd` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Store new unconfirmed password',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_user_attributes`
--

CREATE TABLE IF NOT EXISTS `modx_web_user_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobilephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains information for web users.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_web_user_settings`
--

CREATE TABLE IF NOT EXISTS `modx_web_user_settings` (
  `webuser` int(11) NOT NULL,
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `setting_value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`webuser`,`setting_name`),
  KEY `setting_name` (`setting_name`),
  KEY `webuserid` (`webuser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Contains web user settings.';

-- --------------------------------------------------------

--
-- Структура таблицы `test_answer`
--

CREATE TABLE IF NOT EXISTS `test_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_quest` int(11) NOT NULL,
  `answer_title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` text COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=81 ;

--
-- Дамп данных таблицы `test_answer`
--

INSERT INTO `test_answer` (`id`, `id_quest`, `answer_title`, `status`) VALUES
(47, 2, '2-3', '0'),
(46, 2, '3-4', '0'),
(52, 5, 'чашка кофе, завтракать совсем не могу', '0'),
(49, 2, '5-6', '1'),
(42, 1, 'малая - двух чашек кофе за завтраком вполне хватает на весь день, может еще чашечка чая вечером', '0'),
(53, 5, 'бутерброд, яичница', '0'),
(54, 5, 'готовые сухие завтраки типа кукурузных хлопьев', '0'),
(55, 2, 'один, а лучше вообще не есть, если хотите похудеть', '0'),
(56, 5, 'каша', '1'),
(57, 1, 'полтора литра: потребность покрывается кофе, чаем, соками и другими напитками', '0'),
(58, 1, 'пью 1-1,5 литра в день', '0'),
(59, 1, 'два литра в день как норма, при этом пью преимущественно воду', '1'),
(60, 3, 'да, очень люблю все соленое', '0'),
(61, 3, 'совсем отказалась от соли', '0'),
(62, 3, 'солю, но умеренно', '1'),
(63, 4, 'да, так быстрее можно похудеть', '0'),
(64, 4, 'нет, если занимаюсь на пустой желудок, чувствую головокружение или утомление', '1'),
(65, 4, 'вообще не тренируюсь', '0'),
(66, 6, 'в 18.00, после шести есть нельзя', '0'),
(67, 6, 'за два часа до сна', '1'),
(68, 6, 'можно есть даже ночью!', '0'),
(69, 7, 'конечно, они не калорийные и хорошо насыщают', '0'),
(70, 7, 'нет, в них много сахара', '1'),
(71, 7, 'я вообще стараюсь не есть вечером', '0'),
(72, 8, 'да, потому что в них мало калорий', '0'),
(73, 8, 'нет, обезжиренные продукты не полезны', '1'),
(74, 8, 'можно, но не часто', '0'),
(75, 9, 'конечно, специи придают приятный аромат', '1'),
(76, 9, 'лучше исключить, ничего хорошего там нет', '0'),
(77, 9, 'стоит ограничить, они не совсем полезны', '0'),
(78, 10, 'обязательно, в нем много полезных веществ', '0'),
(79, 10, 'нет, это «пустые» калории', '1'),
(80, 10, 'я вообще молоко не пью', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `test_quest`
--

CREATE TABLE IF NOT EXISTS `test_quest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_test` int(11) NOT NULL,
  `quest_title` text COLLATE utf8_unicode_ci NOT NULL,
  `quest_correct_answer` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `quest_sort` int(11) NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `test_quest`
--

INSERT INTO `test_quest` (`id`, `id_test`, `quest_title`, `quest_correct_answer`, `quest_sort`, `img`) VALUES
(1, 2, 'Ваша потребность в жидкости?', '<p class="MsoNormal">Если говорить о большинстве людей, не имеющих каких-либо особенностей, связанных с ограничением употребляемой жидкости, то их среднедневная норма составляет минимум 2 литра воды. Заметьте, не жидкости, а воды. Если такой объем вам кажется не по силам, старайтесь пить меньше других напитков, пить маленькими глотками. Постепенно организм подстроится и будет просить сам (жажда). В норме - если вы ходите в туалет раз в 2 часа. Тогда у вас нет обезвоживания.&nbsp;</p>', 3, 'assets/images/img3.jpg'),
(2, 2, 'Сколько раз в день вы едите?', '<p class="MsoNormal">Некоторые ошибочно думают: если совсем перестать есть или ограничить приемы пищи до одного раза в день, похудеть можно в два раза быстрее. Но обычно такие эксперименты ни к чему, кроме стресса и проблем со здоровьем, не приводят. Не зависимо от того, худеете ли вы, или&nbsp;набираете массу, либо просто ведете здоровый образ жизни, кушать нужно 5-6 раз в день.</p><p class="MsoNormal">При таком частом питании ваш организм постоянно подпитывается энергетически. Итог этого &ndash; бодрость, активность и высокая работоспособность. Плюс не мучают приступы голода. Кроме того, улучшается обмен веществ. Это происходит, так как органы пищеварения работают постоянно. В результате этого калорий сжигается гораздо больше.</p>', 1, 'assets/images/img1.png'),
(3, 2, 'Солите ли вы пищу?', '<p class="MsoNormal">Не стоит совсем исключать соль из рациона. Натрий и хлор, из которых состоит соль, имеют огромное значение для работы организма. Первый участвует в поддержании водного и кислотно-щелочного баланса, в передаче нервных импульсов. При его недостатке возникают мышечная слабость, сонливость, ухудшается координация. Хлор нужен, в числе прочего, для производства желудочного сока. Но стоит также помнить, что пересаливать пищу &ndash; плохо. Соль задерживает воду в организме, увеличиваются нагрузка на сердце и почки, риск гипертонии, появляется головная боль. Так что главный совет &ndash; солить пищу, но не пересаливать.</p>', 4, 'assets/images/img4.jpg'),
(4, 2, 'Тренируетесь ли вы на голодный желудок?', '<p class="MsoNormal">Тренировки натощак - это катастрофа для организма! Да, упражнения на голодный желудок сжигают больше жира. Однако, если вы хотите похудеть, сохранив свои мышцы и здоровье, необходимо за 30-40 минут перед кардиотренировкой съесть омлет из 2х белков, приготовленный на обезжиренном молоке. Это не даст критично упасть уровню сахара в крови, но при этом не будет источником энергии.&nbsp;Перед силовой тренировкой обязательно нужно позавтракать. А лучше проводить силовую тренировку после 2-3 приемов пищи. Оптимальным временем для силовых занятий будет 2-3 часа после последнего плотного приема пищи.&nbsp;</p>', 5, 'assets/images/img5.jpg'),
(5, 2, 'Что для вас идеальный завтрак?', '<p class="MsoNormal">Правильный завтрак&nbsp;обязательно должен включать&nbsp;углеводы. Причем углеводы желательно не простые, а сложные (продукты, сделанные из зерна: хлеб, крупы, макароны) &mdash; именно они обеспечивают хорошее пищеварение и улучшают&nbsp;обмен веществ. Горячая каша не только помогает взбодриться, но и очищает кишечник от шлаков, а заодно и контролирует усвоение жира из продуктов, съеденных за день. Есть на завтрак бутерброд с копченой колбасой, захлебывая его чашкой кофе, &mdash; нельзя ни в коем случае.&nbsp;От такой пищи организм не получает ничего полезного, это мертвая пища. Вы только забиваете пищевым комком желудок.</p>', 2, 'assets/images/img2.jpg'),
(6, 2, 'Во сколько должен быть последний прием пищи?', '<p class="MsoNormal">Пресловутое правило &laquo;не есть после 6 вечера&raquo; уже давно потеряло свою силу. Во-первых, это стресс для вас лично. Поскольку ближе к полуночи вы проголодаетесь и уже не сможете равнодушно проходить мимо&nbsp;холодильника. Во-вторых, длительное голодание - это стрессовая ситуация для организма. Так как он хочет жить, то в любой возможной ситуации будет делать запасы в виде жира на случаи таких же голодовок. Это значит, что при голодании длительностью более 10 часов в организме включается защитный режим, интенсивно вырабатывается фермент липопротеинлипаза. Благодаря ему жировые кислоты превращаются в подкожную жировую клетчатку. После десятичасового голодания фермент будет делать запасы жира из всей той пищи, которую вы съедите на следующий день. Поэтому еда вечером должна быть нежирная, ее должно быть немного, и она должна быть легкой и низкокалорийной &mdash; таковы основные правила для пищи после 6 вечера.</p>', 6, 'assets/images/img6.jpg'),
(7, 2, 'Можно ли есть фрукты вечером?', '<p class="MsoNormal">Есть фрукты или ягоды вечером &ndash; одна из самых распространенных ошибок тех людей, которые пытаются похудеть и при этом удивляются, почему им это не удается. Даже тем, кто набирает массу или работает над качеством тела, мы не рекомендуем кушать их вечером. Фрукты и ягоды являются источником углеводов, поэтому, кушая их вечером, мы запускаем инсулиновый механизм. Из-за резкого снижения уровня сахара в крови у нас появляется чувство голода.</p>', 7, 'assets/images/img7.jpg'),
(8, 2, 'Стоит ли употреблять обезжиренные продукты?', '<p class="MsoNormal">Нет, не стоит.&nbsp;Природа не создает обезжиренных продуктов. Уже только один этот факт может навести на правильную мысль. Снижение калорийности обезжиренных продуктов не более чем миф. Как правило, разница в калорийности невелика, поскольку все добавки, призванные скомпенсировать вкусовые различия обычных и обезжиренных продуктов, имеют свои калории, так что в конечном счете худеющий потребитель ничего не выигрывает. Обезжиренный продукт бесполезен. Он пуст, в нем отсутствуют все полезные вещества, которые изначально содержались в продукте. Жиры, так же как белки и&nbsp;углеводы, очень важны для нашего организма. Это источник энергии и строительный материал для клеточных мембран. Полностью исключив из питания жиры, мы вряд ли увидим в зеркале сияющую кожу и красивые волосы. Безусловно, от некоторых жиров в питании отказаться все же стоит &ndash; это трасжиры, которые содержатся в спредах, маргарине, майонезе, магазинной выпечке и других продуктах, производители которых экономят на животных жирах (некоторые виды мороженого, вафли и пр.).</p>', 8, 'assets/images/img8.jpg'),
(9, 2, 'Можно ли использовать специи при приготовлении пп-блюд?', '<p>Специи использовать можно, если они натуральные! Обычно специями мы называем то, чем можно ароматизировать пищу, улучшать ее вкусовые качества. Это &ndash; высушенная зелень, семена растений и трав, растертые в порошок листья и высушенные плоды&hellip; Многие специи обладают не только отличными вкусовыми, но и лечебными качествами. Использование зелени (не только свежей, но и сушеной) улучшает процесс пищеварения, обмен веществ. Однако, поскольку большинство специй содержат эфирные масла, ароматические кислоты, дубильные вещества, они могут вызвать раздражение желудочно-кишечного тракта, а это &ndash; риск развития воспалительных процессов. Поэтому тем, у кого есть проблемы с желудочно-кишечным трактом, использовать специи нужно очень осторожно. Кроме того, надо иметь в виду, что многие специи аллергенны.</p>', 9, 'assets/images/img9.jpg'),
(10, 2, 'Стоит ли пить молоко?', '<p class="MsoNormal">Стоит сразу сказать, что стерилизованное молоко, переработанное при 130-150 градусах и хранящееся в холодильнике месяцами, уже не содержит ни полезных, ни вредных веществ. Это &laquo;пустые&raquo; калории!&nbsp; Хотя молоко и молочные продукты имеют низкий гликемический индекс, выброс инсулина после их употребления почти такой же, как после белого хлеба. Кроме того, с возрастом происходит снижение в организме фермента, который расщепляет молочный сахар. Начиная с 25 лет требования к потреблению молока и молочных продуктов ужесточаются!</p>', 10, 'assets/images/img10.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `test_user`
--

CREATE TABLE IF NOT EXISTS `test_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `test_id` int(11) NOT NULL,
  `hash_user` text COLLATE utf8_unicode_ci NOT NULL,
  `page` text COLLATE utf8_unicode_ci NOT NULL,
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

--
-- Дамп данных таблицы `test_user`
--

INSERT INTO `test_user` (`id`, `name`, `mail`, `test_id`, `hash_user`, `page`, `viewed`) VALUES
(1, 'asd', '0', 2, 'de339bca970fbbf3678a18db6f6d6d8a', '/0', 0),
(2, 'asd', '0', 2, '21a2044be56b30b98e498f14e5484a81', '/0', 0),
(3, 'qwe', '0', 2, 'c3fe255f169e88c7f5e8b761c6ec2224', '/stranica-otvetov', 0),
(4, 'qwe', '0', 2, 'd0790fbd2115958f375ac76e81853d05', '/stranica-otvetov', 0),
(5, 'qwe', '0', 2, 'bc270c6f155e4fb7d1a30d268abf679a', '/stranica-otvetov', 0),
(6, 'asd', '0', 2, '9b4753108e2ae45f7b1f7667ea940c92', '/stranica-otvetov', 0),
(7, 'asd', '0', 2, 'eef6b0d4f64f60438ef0bca01374d736', '/stranica-otvetov', 0),
(8, 'Роман', '0', 2, '9b4b1871f114df603a8b206bc823337f', '/stranica-otvetov', 0),
(9, 'asd', '0', 2, 'cec163c71af35c8cf8fee97b69bb510b', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(10, 'asd', '0', 2, 'c322b5578439f1d68fbffd1739807f36', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(11, 'asd', '0', 2, 'e26a5b6e99c08a404ca71f3488b3add2', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(12, 'asd', '0', 2, '36ed52fe64ff0fe342e43511d8ea6627', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(13, 'asd', '0', 2, '93acda4b492fd7308e093d91a6957754', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(14, 'asd', '0', 2, '114c8007041eed8630f183c9e3b24309', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(15, 'фыв', '23423', 2, '1422a570c3580a42a11d39f919343c3b', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(16, 'Денис', '0', 2, '2ef03543dc3e5e958cb862e4cb092054', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(17, 'asdasd', '0', 2, '341bfd4ce5d4edec3ef10b2b798e0f7c', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(18, 'Tatiana', '0', 2, 'cc7a943d68ce1b671d32df4070d3ff88', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(19, '123', '0', 2, '50d493fa43b9c9bbf617abaaefcd4e79', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(20, 'test2', '0', 2, 'e27febff4a32549c0fe22364cdb9251c', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(21, 'Денис', '0', 2, '4c05c4ac0e2f740f60eedd9e1d2edfb9', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(22, 'test2', '0', 2, '4764fe9ad239c4ee362a268b770c6685', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(23, 'test123456', '0', 2, '330c54a7e3bc85f78d9a586cd2478723', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(24, 'ggggggggggg', '0', 2, '2408a28bcec215c0fccbcb0b02ecc94d', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(25, 'qwerty', '0', 2, 'f4b1866f51013339fab6c51f3a17bb48', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(26, 'test12356777', '0', 2, 'e101f1f6ab9d3e8c556f0fa5f6c70930', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(27, '123456', 'sergey@rostunov.com', 2, '4b3a8dbc6df94b41a20b715cf3ed33ed', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(28, 'Тестовый аккаунт', 'rostunov.sergey@yandex.ru', 2, '8ce3ed5b42118e3ad7946c89c1f54419', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(29, 'Денис', 'd.smirnov@specialview.ru', 2, 'a818f6f9c8a6cc1087da9afe76dc033a', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(30, 'Ден', 'xenium.pro@mail.ru', 2, '7ab3f73f6b0688427e2c8770ab5ceef6', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(31, 'Денис', 'd.smirnov@specialview.ru', 2, '32def695659557940ad1ba5cc6d93b38', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(32, 'test1234', 'sergey7@e-kirov.ru', 2, '7afd192218882cee8e0ce409bc72133e', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(33, 'tesq123', 'sergey7@e-kirov.ru', 2, 'e256207077f844ab0e9159435371526b', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(34, 'sdfds', 'sergey@rostunov.com', 2, '566557dfc8d5bb4eae0454e850eeb276', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(35, 'Денис', 'd.smirnov@specialview.ru', 2, '26366793675bd32048c35862149c070c', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(36, 'Денис', 'd.smirnov@specialview.ru', 2, '033fb2c1e91c43324d8f6d2351c7128c', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(37, 'Ден', 'xenium.pro@mail.ru', 2, '456ac13cccbdd925d7064e2350693dbd', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(38, 'еуые123123', 'wupexu@olypmall.ru', 2, '7e3c2db7e6a6067d4e820ac68fc003e4', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(39, 'Ден', 'd.smirnov@specialview.ru', 2, '14b2198edacf738c5b548fdd65cddccf', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(40, 'Денис', 'xenium.pro@mail.ru', 2, '6430fed567ccc243a79edd6e278834a0', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(41, 'Ромч', 'poker717@gmail.com', 2, 'd9d09dbc68a19dc131ade2bdbcd81983', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(42, 'den', 'anseroot@gmail.com', 2, '6567c4cd44492c0e5b0a907e94fd088b', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(43, 'Денис', 'd.smirnov@specialview.ru', 2, 'd1f93f8f1d027176891fc191d27343e2', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(44, 'Денис', 'd.smirnov@specialview.ru', 2, 'ebb6ad5d99ea936c85efeca75ff21163', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(45, 'Денис', 'd.smirnov@specialview.ru', 2, '98bcbda212875f28fa29c8ea5405a1e4', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(46, 'Денис', 'd.smirnov@specialview.ru', 2, 'd3112f11122f38fa6d22cd0bb39ff31e', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(47, 'Денис', 'd.smirnov@specialview.ru', 2, '46a32d519cfdd97c50ed3510e1f07eed', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(48, 'Роман', 'roma@specialview.ru', 2, '7604b87654c27f4c608fa3ff5ebd6157', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(49, 'Роман', 'roma@specialview.ru', 2, 'cd1d929a072f7ed263f2e63cb771d1c6', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(50, 'Йоу', 'roma@g-sign.ru', 2, 'dfea580627dadf5bbe699090f5a540c8', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(51, 'Йоу', 'roma@g-sign.ru', 2, 'dfea580627dadf5bbe699090f5a540c8', 'http://test.beauty-matrix.ru/stranica-otvetov', 0),
(52, 'asd', 'anseroot@gmail.com', 2, '66a754be3c650c4daf1da40d53c9cd11', 'http://test.beauty-matrix.ru/stranica-otvetov', 1),
(53, 'asd', 'anseroot@gmail.com', 2, 'a3aa3c62ce00d61f4606d7463cec0987', 'http://test.beauty-matrix.ru/stranica-otvetov', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `test_user_answer`
--

CREATE TABLE IF NOT EXISTS `test_user_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_answer` int(11) NOT NULL,
  `answer_value` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=521 ;

--
-- Дамп данных таблицы `test_user_answer`
--

INSERT INTO `test_user_answer` (`id`, `id_answer`, `answer_value`, `user_id`) VALUES
(1, 2, 46, 0),
(2, 5, 54, 0),
(3, 1, 57, 0),
(4, 3, 62, 0),
(5, 4, 64, 0),
(6, 6, 68, 0),
(7, 7, 70, 0),
(8, 8, 73, 0),
(9, 9, 76, 0),
(10, 10, 80, 0),
(11, 2, 46, 0),
(12, 5, 54, 0),
(13, 1, 57, 0),
(14, 3, 62, 0),
(15, 4, 64, 0),
(16, 6, 68, 0),
(17, 7, 71, 0),
(18, 8, 74, 0),
(19, 9, 77, 0),
(20, 10, 79, 0),
(21, 2, 46, 0),
(22, 5, 54, 0),
(23, 1, 57, 0),
(24, 3, 62, 0),
(25, 4, 64, 0),
(26, 6, 68, 0),
(27, 7, 71, 0),
(28, 8, 74, 0),
(29, 9, 77, 0),
(30, 10, 79, 0),
(31, 2, 46, 0),
(32, 5, 54, 0),
(33, 1, 57, 0),
(34, 3, 62, 0),
(35, 4, 64, 0),
(36, 6, 68, 0),
(37, 7, 71, 0),
(38, 8, 74, 0),
(39, 9, 77, 0),
(40, 10, 79, 0),
(41, 2, 55, 0),
(42, 5, 54, 0),
(43, 1, 57, 0),
(44, 3, 62, 0),
(45, 4, 64, 0),
(46, 6, 68, 0),
(47, 7, 71, 0),
(48, 8, 73, 0),
(49, 9, 76, 0),
(50, 10, 80, 0),
(51, 2, 55, 0),
(52, 5, 54, 0),
(53, 1, 57, 0),
(54, 3, 62, 0),
(55, 4, 64, 0),
(56, 6, 68, 0),
(57, 7, 71, 0),
(58, 8, 73, 0),
(59, 9, 76, 0),
(60, 10, 80, 0),
(61, 2, 49, 0),
(62, 5, 53, 0),
(63, 1, 57, 0),
(64, 3, 61, 0),
(65, 4, 63, 0),
(66, 6, 66, 0),
(67, 7, 70, 0),
(68, 8, 73, 0),
(69, 9, 75, 0),
(70, 10, 80, 0),
(71, 2, 55, 0),
(72, 5, 54, 0),
(73, 1, 57, 0),
(74, 3, 62, 0),
(75, 4, 64, 0),
(76, 6, 68, 0),
(77, 7, 71, 0),
(78, 8, 73, 0),
(79, 9, 76, 0),
(80, 10, 80, 0),
(81, 2, 55, 0),
(82, 5, 54, 0),
(83, 1, 57, 0),
(84, 3, 62, 0),
(85, 4, 64, 0),
(86, 6, 68, 0),
(87, 7, 71, 0),
(88, 8, 73, 0),
(89, 9, 76, 0),
(90, 10, 80, 0),
(91, 2, 55, 0),
(92, 5, 54, 0),
(93, 1, 57, 0),
(94, 3, 62, 0),
(95, 4, 64, 0),
(96, 6, 68, 0),
(97, 7, 71, 0),
(98, 8, 73, 0),
(99, 9, 76, 0),
(100, 10, 80, 0),
(101, 2, 55, 0),
(102, 5, 54, 0),
(103, 1, 57, 0),
(104, 3, 62, 0),
(105, 4, 64, 0),
(106, 6, 68, 0),
(107, 7, 71, 0),
(108, 8, 73, 0),
(109, 9, 76, 0),
(110, 10, 80, 0),
(111, 2, 55, 13),
(112, 5, 54, 13),
(113, 1, 57, 13),
(114, 3, 62, 13),
(115, 4, 64, 13),
(116, 6, 68, 13),
(117, 7, 71, 13),
(118, 8, 73, 13),
(119, 9, 76, 13),
(120, 10, 80, 13),
(121, 2, 55, 14),
(122, 5, 54, 14),
(123, 1, 57, 14),
(124, 3, 62, 14),
(125, 4, 64, 14),
(126, 6, 68, 14),
(127, 7, 71, 14),
(128, 8, 73, 14),
(129, 9, 76, 14),
(130, 10, 80, 14),
(131, 2, 47, 15),
(132, 5, 53, 15),
(133, 1, 42, 15),
(134, 3, 61, 15),
(135, 4, 64, 15),
(136, 6, 67, 15),
(137, 7, 70, 15),
(138, 8, 73, 15),
(139, 9, 76, 15),
(140, 10, 79, 15),
(141, 2, 47, 16),
(142, 5, 53, 16),
(143, 1, 57, 16),
(144, 3, 61, 16),
(145, 4, 63, 16),
(146, 6, 67, 16),
(147, 7, 69, 16),
(148, 8, 73, 16),
(149, 9, 75, 16),
(150, 10, 80, 16),
(151, 2, 47, 17),
(152, 5, 54, 17),
(153, 1, 57, 17),
(154, 3, 61, 17),
(155, 4, 64, 17),
(156, 6, 68, 17),
(157, 7, 71, 17),
(158, 8, 74, 17),
(159, 9, 77, 17),
(160, 10, 80, 17),
(161, 2, 47, 18),
(162, 5, 53, 18),
(163, 1, 58, 18),
(164, 3, 62, 18),
(165, 4, 65, 18),
(166, 6, 66, 18),
(167, 7, 70, 18),
(168, 8, 74, 18),
(169, 9, 75, 18),
(170, 10, 78, 18),
(171, 2, 55, 19),
(172, 5, 56, 19),
(173, 1, 59, 19),
(174, 3, 62, 19),
(175, 4, 65, 19),
(176, 6, 67, 19),
(177, 7, 70, 19),
(178, 8, 74, 19),
(179, 9, 77, 19),
(180, 10, 80, 19),
(181, 2, 55, 20),
(182, 5, 54, 20),
(183, 1, 59, 20),
(184, 3, 61, 20),
(185, 4, 64, 20),
(186, 6, 67, 20),
(187, 7, 70, 20),
(188, 8, 73, 20),
(189, 9, 75, 20),
(190, 10, 78, 20),
(191, 2, 47, 21),
(192, 5, 53, 21),
(193, 1, 57, 21),
(194, 3, 62, 21),
(195, 4, 64, 21),
(196, 6, 67, 21),
(197, 7, 70, 21),
(198, 8, 73, 21),
(199, 9, 77, 21),
(200, 10, 79, 21),
(201, 2, 47, 22),
(202, 5, 53, 22),
(203, 1, 57, 22),
(204, 3, 61, 22),
(205, 4, 64, 22),
(206, 6, 67, 22),
(207, 7, 70, 22),
(208, 8, 73, 22),
(209, 9, 76, 22),
(210, 10, 80, 22),
(211, 2, 47, 23),
(212, 5, 52, 23),
(213, 1, 42, 23),
(214, 3, 60, 23),
(215, 4, 63, 23),
(216, 6, 66, 23),
(217, 7, 69, 23),
(218, 8, 72, 23),
(219, 9, 75, 23),
(220, 10, 78, 23),
(221, 2, 47, 24),
(222, 5, 52, 24),
(223, 1, 42, 24),
(224, 3, 60, 24),
(225, 4, 63, 24),
(226, 6, 66, 24),
(227, 7, 69, 24),
(228, 8, 72, 24),
(229, 9, 75, 24),
(230, 10, 78, 24),
(231, 2, 47, 25),
(232, 5, 54, 25),
(233, 1, 57, 25),
(234, 3, 62, 25),
(235, 4, 64, 25),
(236, 6, 66, 25),
(237, 7, 69, 25),
(238, 8, 73, 25),
(239, 9, 76, 25),
(240, 10, 80, 25),
(241, 2, 47, 26),
(242, 5, 52, 26),
(243, 1, 42, 26),
(244, 3, 60, 26),
(245, 4, 63, 26),
(246, 6, 66, 26),
(247, 7, 69, 26),
(248, 8, 72, 26),
(249, 9, 75, 26),
(250, 10, 78, 26),
(251, 2, 47, 27),
(252, 5, 54, 27),
(253, 1, 58, 27),
(254, 3, 61, 27),
(255, 4, 64, 27),
(256, 6, 67, 27),
(257, 7, 70, 27),
(258, 8, 73, 27),
(259, 9, 76, 27),
(260, 10, 79, 27),
(261, 2, 47, 28),
(262, 5, 52, 28),
(263, 1, 42, 28),
(264, 3, 60, 28),
(265, 4, 63, 28),
(266, 6, 66, 28),
(267, 7, 69, 28),
(268, 8, 72, 28),
(269, 9, 75, 28),
(270, 10, 78, 28),
(271, 2, 47, 29),
(272, 5, 54, 29),
(273, 1, 57, 29),
(274, 3, 60, 29),
(275, 4, 64, 29),
(276, 6, 66, 29),
(277, 7, 71, 29),
(278, 8, 73, 29),
(279, 9, 76, 29),
(280, 10, 79, 29),
(281, 2, 47, 30),
(282, 5, 52, 30),
(283, 1, 42, 30),
(284, 3, 62, 30),
(285, 4, 63, 30),
(286, 6, 67, 30),
(287, 7, 69, 30),
(288, 8, 72, 30),
(289, 9, 76, 30),
(290, 10, 79, 30),
(291, 2, 47, 31),
(292, 5, 52, 31),
(293, 1, 57, 31),
(294, 3, 61, 31),
(295, 4, 64, 31),
(296, 6, 66, 31),
(297, 7, 70, 31),
(298, 8, 72, 31),
(299, 9, 76, 31),
(300, 10, 80, 31),
(301, 2, 47, 32),
(302, 5, 52, 32),
(303, 1, 42, 32),
(304, 3, 61, 32),
(305, 4, 64, 32),
(306, 6, 67, 32),
(307, 7, 70, 32),
(308, 8, 73, 32),
(309, 9, 77, 32),
(310, 10, 80, 32),
(311, 2, 47, 33),
(312, 5, 54, 33),
(313, 1, 57, 33),
(314, 3, 62, 33),
(315, 4, 64, 33),
(316, 6, 68, 33),
(317, 7, 71, 33),
(318, 8, 74, 33),
(319, 9, 77, 33),
(320, 10, 80, 33),
(321, 2, 55, 34),
(322, 5, 54, 34),
(323, 1, 57, 34),
(324, 3, 62, 34),
(325, 4, 64, 34),
(326, 6, 67, 34),
(327, 7, 70, 34),
(328, 8, 73, 34),
(329, 9, 76, 34),
(330, 10, 80, 34),
(331, 2, 47, 35),
(332, 5, 53, 35),
(333, 1, 57, 35),
(334, 3, 62, 35),
(335, 4, 63, 35),
(336, 6, 68, 35),
(337, 7, 70, 35),
(338, 8, 73, 35),
(339, 9, 76, 35),
(340, 10, 80, 35),
(341, 2, 47, 36),
(342, 5, 53, 36),
(343, 1, 57, 36),
(344, 3, 62, 36),
(345, 4, 64, 36),
(346, 6, 66, 36),
(347, 7, 70, 36),
(348, 8, 74, 36),
(349, 9, 76, 36),
(350, 10, 78, 36),
(351, 2, 47, 37),
(352, 5, 54, 37),
(353, 1, 42, 37),
(354, 3, 62, 37),
(355, 4, 63, 37),
(356, 6, 67, 37),
(357, 7, 70, 37),
(358, 8, 72, 37),
(359, 9, 77, 37),
(360, 10, 80, 37),
(361, 2, 47, 38),
(362, 5, 53, 38),
(363, 1, 57, 38),
(364, 3, 62, 38),
(365, 4, 65, 38),
(366, 6, 68, 38),
(367, 7, 71, 38),
(368, 8, 74, 38),
(369, 9, 77, 38),
(370, 10, 80, 38),
(371, 2, 55, 39),
(372, 5, 54, 39),
(373, 1, 42, 39),
(374, 3, 62, 39),
(375, 4, 63, 39),
(376, 6, 66, 39),
(377, 7, 70, 39),
(378, 8, 73, 39),
(379, 9, 76, 39),
(380, 10, 78, 39),
(381, 2, 47, 40),
(382, 5, 52, 40),
(383, 1, 42, 40),
(384, 3, 60, 40),
(385, 4, 63, 40),
(386, 6, 66, 40),
(387, 7, 69, 40),
(388, 8, 72, 40),
(389, 9, 75, 40),
(390, 10, 78, 40),
(391, 2, 47, 41),
(392, 5, 53, 41),
(393, 1, 57, 41),
(394, 3, 62, 41),
(395, 4, 64, 41),
(396, 6, 67, 41),
(397, 7, 71, 41),
(398, 8, 74, 41),
(399, 9, 77, 41),
(400, 10, 80, 41),
(401, 2, 46, 42),
(402, 5, 53, 42),
(403, 1, 42, 42),
(404, 3, 61, 42),
(405, 4, 63, 42),
(406, 6, 66, 42),
(407, 7, 71, 42),
(408, 8, 73, 42),
(409, 9, 76, 42),
(410, 10, 80, 42),
(411, 2, 47, 43),
(412, 5, 52, 43),
(413, 1, 42, 43),
(414, 3, 60, 43),
(415, 4, 63, 43),
(416, 6, 66, 43),
(417, 7, 69, 43),
(418, 8, 72, 43),
(419, 9, 75, 43),
(420, 10, 78, 43),
(421, 2, 47, 44),
(422, 5, 52, 44),
(423, 1, 42, 44),
(424, 3, 60, 44),
(425, 4, 63, 44),
(426, 6, 66, 44),
(427, 7, 69, 44),
(428, 8, 72, 44),
(429, 9, 75, 44),
(430, 10, 78, 44),
(431, 2, 47, 45),
(432, 5, 52, 45),
(433, 1, 42, 45),
(434, 3, 60, 45),
(435, 4, 63, 45),
(436, 6, 66, 45),
(437, 7, 69, 45),
(438, 8, 72, 45),
(439, 9, 75, 45),
(440, 10, 78, 45),
(441, 2, 47, 46),
(442, 5, 52, 46),
(443, 1, 42, 46),
(444, 3, 60, 46),
(445, 4, 63, 46),
(446, 6, 66, 46),
(447, 7, 69, 46),
(448, 8, 72, 46),
(449, 9, 75, 46),
(450, 10, 78, 46),
(451, 2, 47, 47),
(452, 5, 52, 47),
(453, 1, 42, 47),
(454, 3, 60, 47),
(455, 4, 63, 47),
(456, 6, 66, 47),
(457, 7, 69, 47),
(458, 8, 72, 47),
(459, 9, 75, 47),
(460, 10, 78, 47),
(461, 2, 47, 48),
(462, 5, 53, 48),
(463, 1, 42, 48),
(464, 3, 62, 48),
(465, 4, 65, 48),
(466, 6, 68, 48),
(467, 7, 70, 48),
(468, 8, 74, 48),
(469, 9, 77, 48),
(470, 10, 79, 48),
(471, 2, 47, 49),
(472, 5, 53, 49),
(473, 1, 42, 49),
(474, 3, 62, 49),
(475, 4, 65, 49),
(476, 6, 68, 49),
(477, 7, 70, 49),
(478, 8, 74, 49),
(479, 9, 77, 49),
(480, 10, 79, 49),
(481, 2, 46, 50),
(482, 5, 54, 50),
(483, 1, 57, 50),
(484, 3, 62, 50),
(485, 4, 65, 50),
(486, 6, 68, 50),
(487, 7, 71, 50),
(488, 8, 74, 50),
(489, 9, 76, 50),
(490, 10, 79, 50),
(491, 2, 46, 51),
(492, 5, 54, 51),
(493, 1, 57, 51),
(494, 3, 62, 51),
(495, 4, 65, 51),
(496, 6, 68, 51),
(497, 7, 71, 51),
(498, 8, 74, 51),
(499, 9, 76, 51),
(500, 10, 79, 51),
(501, 2, 46, 52),
(502, 5, 53, 52),
(503, 1, 57, 52),
(504, 3, 62, 52),
(505, 4, 64, 52),
(506, 6, 68, 52),
(507, 7, 70, 52),
(508, 8, 73, 52),
(509, 9, 76, 52),
(510, 10, 79, 52),
(511, 2, 46, 53),
(512, 5, 53, 53),
(513, 1, 57, 53),
(514, 3, 62, 53),
(515, 4, 64, 53),
(516, 6, 68, 53),
(517, 7, 70, 53),
(518, 8, 73, 53),
(519, 9, 76, 53),
(520, 10, 79, 53);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
